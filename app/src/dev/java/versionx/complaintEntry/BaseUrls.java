package versionx.complaintEntry;

public class BaseUrls {

    public static final String PRIMARY_DATABASE_URL = "https://dev-entry.firebaseio.com/";

    public static final String SECONDARY_DATABASE_URL = "https://dev-entry-2.firebaseio.com/";
}
