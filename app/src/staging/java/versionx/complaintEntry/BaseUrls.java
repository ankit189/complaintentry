package versionx.complaintEntry;

public class BaseUrls {

    public static final String PRIMARY_DATABASE_URL = "https://test-entry.firebaseio.com/";

    public static final String SECONDARY_DATABASE_URL = "https://testentry-custom.firebaseio.com/";
}
