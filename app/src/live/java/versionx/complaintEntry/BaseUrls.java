package versionx.complaintEntry;

public class BaseUrls {

    public static final String PRIMARY_DATABASE_URL = "https://whyentry.firebaseio.com/";

    public static final String SECONDARY_DATABASE_URL = "https://entry-custom.firebaseio.com/";
}
