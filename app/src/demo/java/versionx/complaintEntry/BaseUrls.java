package versionx.complaintEntry;

public class BaseUrls {

    public static final String PRIMARY_DATABASE_URL = "https://entry-demo.firebaseio.com/";

    public static final String SECONDARY_DATABASE_URL = "https://entry-demo-4f224.firebaseio.com/";
}
