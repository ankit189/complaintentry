package versionx.complaintEntry.OfflineDataBase;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;


import java.util.ArrayList;
import java.util.List;

import versionx.complaintEntry.GetterSetter.Audio;
import versionx.complaintEntry.GetterSetter.ImagePath;
import versionx.complaintEntry.GetterSetter.RefTable;


/**
 * Created by developer on 14/7/17.
 */

public class FirebaseImagePath {
    private static final String KEY_ROWID = "_id";
    private static final String KEY_SESSIONURI = "session_uri";
    private static final String KEY_STORAGE_PATH = "storage_path";
    private static final String KEY_FILEPATH = "file_path";
    private static final String KEY_HIS_KEY = "his_key";
    private static final String KEY_POSITION = "POS";
    private static final String KEY_IS_CUSTOM = "is_custom";
    private static final String KEY_TIME_STAMP = "time_stamp";


    private static final String KEY_ROWID_REF="id";
    private static final String KEY_DataBase_REF ="database_ref";
    private static final String KEY_MONTH_REF = "is_month_ref";

    private static final String DATABASE_NAME = "ImagePathStorage";

    private static final String DATABASE_TABLE = "ImagePath";
    private static final String DATABASE_TABLE_REF ="RefTable";

    private static final int DATABASE_VERSION = 2;
    private ArrayList<ImagePath> imgList;
    private DbHelper ourHelper;
    private Context ourContext;
    private SQLiteDatabase ourDatabase;


    //Audio Db
    public static final String TABLE_AUDIO = "audio";
    public static final String TABLE_AUDIO_PATH_REF = "audio_ref";




    public FirebaseImagePath(Context ourContext) {
        this.ourContext = ourContext;
    }

    public ArrayList<ImagePath> getAllImage(String hisKey) {

        ArrayList<ImagePath> keyValsList=new ArrayList<>();

        String[] columns=new String[]{KEY_ROWID, KEY_HIS_KEY,KEY_STORAGE_PATH, KEY_FILEPATH,KEY_SESSIONURI,KEY_POSITION,KEY_IS_CUSTOM};

        Cursor c=ourDatabase.query(DATABASE_TABLE,columns,KEY_IS_CUSTOM+" = ? ",new String[]{"0"},null,null,null);

        int iLocalPath = c.getColumnIndex(KEY_FILEPATH);
        int ikey = c.getColumnIndex(KEY_POSITION);
        int iHisKey=c.getColumnIndex(KEY_HIS_KEY);
        for (c.moveToFirst();!c.isAfterLast();c.moveToNext()){
            if (c.getString(iHisKey).equals(hisKey)){
                keyValsList.add(new ImagePath(c.getString(ikey),c.getString(iLocalPath)));
            }
        }

        c.close();

        return keyValsList;
    }




    private static class DbHelper extends SQLiteOpenHelper {


        public DbHelper(Context context) {
            super(context, DATABASE_NAME, null, DATABASE_VERSION);

        }

        @Override
        public void onCreate(SQLiteDatabase db) {
            db.execSQL("CREATE TABLE " + DATABASE_TABLE + " ( " +
                    KEY_ROWID        + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                    KEY_HIS_KEY      + " TEXT NOT NULL, " +
                    KEY_STORAGE_PATH + " TEXT NOT NULL, " +
                    KEY_FILEPATH     + " TEXT NOT NULL, " +
                    KEY_SESSIONURI   + " TEXT , " +
                    KEY_POSITION     + " TEXT NOT NULL, " +
                    KEY_TIME_STAMP   + " INTEGER ," +
                    KEY_IS_CUSTOM    + " INTEGER DEFAULT 0 );"
            );

            db.execSQL("CREATE TABLE " + DATABASE_TABLE_REF + " ( " +
                    KEY_ROWID_REF      + " INTEGER NOT NULL, " +
                    KEY_DataBase_REF   + " TEXT NOT NULL, " +
                    KEY_MONTH_REF + " INTEGER DEFAULT 0 );"
            );

            db.execSQL("CREATE TABLE "+ TABLE_AUDIO + " ( " +
                     KEY_ROWID+" INTEGER PRIMARY KEY AUTOINCREMENT, " +
                     KEY_HIS_KEY+ " TEXT NOT NULL, " +
                     KEY_STORAGE_PATH+ " TEXT NOT NULL, " +
                     KEY_FILEPATH+ " TEXT NOT NULL );"
            );

            db.execSQL("CREATE TABLE " + TABLE_AUDIO_PATH_REF + " ( " +
                    KEY_ROWID_REF      + " INTEGER NOT NULL, " +
                    KEY_DataBase_REF + " TEXT NOT NULL );"
            );



        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
           if(oldVersion==1 && newVersion==2){
               db.execSQL("ALTER TABLE " + DATABASE_TABLE_REF + " ADD COLUMN " + KEY_MONTH_REF + " INTEGER DEFAULT 0");
           }
        }
    }

    public FirebaseImagePath open() throws SQLException {

        ourHelper = new DbHelper(ourContext);

        ourDatabase=ourHelper.getWritableDatabase();

        return this;

    }
    public void close(){
        ourHelper.close();
    }

    public long createEntry(ImagePath imagePath){
        ContentValues cv=new ContentValues();
        cv.put(KEY_HIS_KEY, imagePath.getHisKey());
        cv.put(KEY_STORAGE_PATH, imagePath.getStoragePath());
        cv.put(KEY_FILEPATH, imagePath.getLocalPath());
        cv.put(KEY_POSITION, imagePath.getKey());

        if(!checkPathExists(imagePath.getLocalPath())) {
            return ourDatabase.insert(DATABASE_TABLE, null, cv);
        }else {
            return 0;
        }
    }

    public void createCustomImageEntry(ImagePath imagePath) {

        ContentValues cv = new ContentValues();
        cv.put(KEY_HIS_KEY, imagePath.getHisKey());
        cv.put(KEY_STORAGE_PATH, imagePath.getStoragePath());
        cv.put(KEY_FILEPATH, imagePath.getLocalPath());
        cv.put(KEY_POSITION, imagePath.getKey());
        cv.put(KEY_TIME_STAMP, imagePath.getMonth());
        cv.put(KEY_IS_CUSTOM,1);

        Cursor cursor = ourDatabase.rawQuery("SELECT * FROM "+DATABASE_TABLE+" WHERE "+
                KEY_HIS_KEY + " = '"+imagePath.getHisKey()+"' AND " + KEY_POSITION + " = '" + imagePath.getKey() +"'",null);

        int count  = cursor.getCount();

        long rowId = 0;

        /*So other threads doing database operations can access the database...*/
        ourDatabase.beginTransactionNonExclusive();

        try {
            if (count > 0 && cursor.moveToFirst()) {
                ourDatabase.update(DATABASE_TABLE, cv, KEY_HIS_KEY + " = ? AND " + KEY_POSITION + " = ?",
                        new String[]{imagePath.getHisKey(), imagePath.getKey()});
                //rowId = cursor.getInt(cursor.getColumnIndex(KEY_ROWID));
                //deleteDatabasePath(rowId);
                //CommonMethods.deleteImages(cursor.getString(cursor.getColumnIndex(KEY_FILEPATH)));
            } else {
                rowId = ourDatabase.insert(DATABASE_TABLE, null, cv);
            }

            cursor.close();

            if (rowId > 0) {
                imagePath.setRowId(String.valueOf(rowId));
                createEntryRef(imagePath.getDatabasePath(),rowId);
            }

            ourDatabase.setTransactionSuccessful();
        }catch (Exception e){
//            Crashlytics.log(e.getMessage());
        }finally {

            ourDatabase.endTransaction();
        }
    }

    public ArrayList<ImagePath> getRowDataByFilePath(String imgUrl) {
        ArrayList<ImagePath> keyValsList=new ArrayList<>();
        ImagePath imagePath =new ImagePath();
        Cursor c = ourDatabase.rawQuery("SELECT * FROM "+ DATABASE_TABLE +  " WHERE " + KEY_FILEPATH + " = '" + imgUrl+"'", null);
        int iRowId=c.getColumnIndex(KEY_ROWID);

        if (c.getCount()>0&&c.moveToFirst()){
            imagePath.setRowId(c.getString(iRowId));
            keyValsList.add(imagePath);
        }
        c.moveToNext();
        c.close();
        return keyValsList;
    }

    public ArrayList<ImagePath> getData() {

        imgList=new ArrayList<>();
        imgList.clear();
        ImagePath imagePath =new ImagePath();
        String[] columns=new String[]{KEY_ROWID, KEY_HIS_KEY,KEY_STORAGE_PATH, KEY_FILEPATH,KEY_SESSIONURI,KEY_POSITION,KEY_TIME_STAMP};
        Cursor c=ourDatabase.query(DATABASE_TABLE,columns,null,null,null,null,KEY_ROWID+" DESC" , "1");
        int iRow=c.getColumnIndex(KEY_ROWID);
        int iStorageKey=c.getColumnIndex(KEY_STORAGE_PATH);
        int iFilePath=c.getColumnIndex(KEY_FILEPATH);
        int iHisKey=c.getColumnIndex(KEY_HIS_KEY);
        int iSession=c.getColumnIndex(KEY_SESSIONURI);
        int iPos=c.getColumnIndex(KEY_POSITION);
        int iTsp=c.getColumnIndex(KEY_TIME_STAMP);

        if (c.getCount()>0 && c.moveToFirst())
        {

            imagePath.setRowId(c.getString(iRow));
            imagePath.setHisKey(c.getString(iHisKey));
            imagePath.setStoragePath(c.getString(iStorageKey));
            imagePath.setLocalPath(c.getString(iFilePath));
            imagePath.setKey(c.getString(iPos));
            imagePath.setSessionUri(c.getString(iSession));
            imagePath.setMonth(c.getString(iTsp));
            imgList.add(imagePath);

        }
        c.close();
        return imgList;
    }
    public void addSessionUri(String rowId, String uri) {
        ContentValues cvUpdates=new ContentValues();
        cvUpdates.put(KEY_SESSIONURI,uri);
        ourDatabase.update(DATABASE_TABLE,cvUpdates,KEY_ROWID + "=" + rowId,null);
    }
    public ArrayList<ImagePath> getRowData(String id) {
        ArrayList<ImagePath> keyValsList=new ArrayList<>();
        ImagePath imagePath =new ImagePath();
        Cursor c = ourDatabase.rawQuery("SELECT * FROM "+ DATABASE_TABLE +  " WHERE " + KEY_HIS_KEY + " = '" + id+"'", null);
        int iRowId=c.getColumnIndex(KEY_ROWID);
        int iLocalPath=c.getColumnIndex(KEY_FILEPATH);
        int ikey=c.getColumnIndex(KEY_POSITION);
        if (c.getCount()>0&&c.moveToFirst()){
            imagePath.setRowId(c.getString(iRowId));
            imagePath.setLocalPath(c.getString(iLocalPath));
            imagePath.setKey(c.getString(ikey));
            keyValsList.add(imagePath);
        }

        c.moveToNext();
        c.close();
        return keyValsList;
    }


    public long deleteStoragePath(long lRowDelete) throws SQLException{
        ourDatabase.delete(DATABASE_TABLE,KEY_ROWID + "=" + lRowDelete,null);
        return lRowDelete;
    }


    /**
        * ..........Reference Table Saving Database Reference..........*
                                                                         **/

    public void createEntryRef(List<RefTable> refTable,long rowId) throws SQLException {
        ourDatabase.beginTransactionNonExclusive();
        try {
            for (RefTable databaseRef : refTable) {
                ContentValues cv = new ContentValues();
                cv.put(KEY_ROWID_REF, rowId);
                cv.put(KEY_DataBase_REF, databaseRef.getDatabase_ref());
                cv.put(KEY_MONTH_REF,databaseRef.isMonth_ref());
                ourDatabase.insert(DATABASE_TABLE_REF, null, cv);
            }
            ourDatabase.setTransactionSuccessful();
        }finally {
            ourDatabase.endTransaction();
        }
    }

    public boolean checkPathExists(String path){

        Cursor cursor = ourDatabase.rawQuery("select * from " + DATABASE_TABLE+ " where " + KEY_FILEPATH + "='" + path + "'", null);

        int count = cursor.getCount();

        cursor.close();

        return count >= 1;
    }

    public List<RefTable> getDataRef(String rowId) {
        List<RefTable> databaseRefList = new ArrayList<>();
        Cursor c = ourDatabase.rawQuery("SELECT * FROM "+ DATABASE_TABLE_REF +  " WHERE " + KEY_ROWID_REF + " = " + rowId, null);
        int iDatabaseRef=c.getColumnIndex(KEY_DataBase_REF);
        int isMonthRef = c.getColumnIndex(KEY_MONTH_REF);
        if (c.getCount()>0&&c.moveToFirst()){
            do {
                databaseRefList.add(new RefTable(c.getString(iDatabaseRef), c.getInt(isMonthRef) == 1));
            }while (c.moveToNext());
        }
        c.close();
        return databaseRefList;
    }

   /* public ArrayList<ImagePath> getAllDataRef() {
        ArrayList<ImagePath> keyValsList=new ArrayList<>();
        Cursor c = ourDatabase.rawQuery("SELECT * FROM "+ DATABASE_TABLE_REF,null);

        int iRowId=c.getColumnIndex(KEY_ROWID_REF);
        int iDatabaseRef=c.getColumnIndex(KEY_DataBase_REF);
        if (c.getCount() > 0 && c.moveToFirst()){
            do {
                ImagePath imagePath =new ImagePath();

                imagePath.setRowId(c.getString(iRowId));
                imagePath.setDatabasePath(c.getString(iDatabaseRef));
                keyValsList.add(imagePath);
            }while (c.moveToNext());
        }

        c.close();

        return keyValsList;

    }*/

    public ArrayList<ImagePath> getAllData() {

        imgList=new ArrayList<>();
        imgList.clear();

        Cursor c=ourDatabase.rawQuery("SELECT * FROM "+DATABASE_TABLE,null);

        int iRow=c.getColumnIndex(KEY_ROWID);
        int iStorageKey=c.getColumnIndex(KEY_STORAGE_PATH);
        int iFilePath=c.getColumnIndex(KEY_FILEPATH);
        int iHisKey=c.getColumnIndex(KEY_HIS_KEY);
        int iSession=c.getColumnIndex(KEY_SESSIONURI);
        int iPos=c.getColumnIndex(KEY_POSITION);

        if (c.getCount() > 0 && c.moveToFirst())
        {
            do {
                ImagePath imagePath =new ImagePath();

                imagePath.setRowId(c.getString(iRow));
                imagePath.setHisKey(c.getString(iHisKey));
                imagePath.setStoragePath(c.getString(iStorageKey));
                imagePath.setLocalPath(c.getString(iFilePath));
                imagePath.setKey(c.getString(iPos));
                imagePath.setSessionUri(c.getString(iSession));
                imgList.add(imagePath);
            }while (c.moveToNext());
        }

        c.close();

        return imgList;
    }

    public long deleteDatabasePath(long lRowDelete) throws SQLException{
        ourDatabase.delete(DATABASE_TABLE_REF,KEY_ROWID_REF + "=" + lRowDelete,null);
        return lRowDelete;
    }

    public String getCustomImagePath(String customKey,String hisKey) {

        String imagePath = null;

        Cursor cursor = ourDatabase.rawQuery("SELECT " + KEY_FILEPATH + " FROM " + DATABASE_TABLE + " WHERE " +
                KEY_HIS_KEY + " = '"+hisKey+"' AND " + KEY_POSITION + " = '" + customKey +"'", null);

        if (cursor != null && cursor.moveToFirst()) {

            imagePath = cursor.getString(cursor.getColumnIndex(KEY_FILEPATH));

            cursor.close();
        }

        return imagePath;
    }


    public void insertAudio(Audio audioPath) {
        ourDatabase.beginTransactionNonExclusive();

        try {
        ContentValues cv=new ContentValues();
        cv.put(KEY_HIS_KEY, audioPath.getHis_key());
        cv.put(KEY_STORAGE_PATH, audioPath.getStorage_path());
        cv.put(KEY_FILEPATH, audioPath.getFile_path());




        Cursor cursor = ourDatabase.rawQuery("SELECT * FROM " + TABLE_AUDIO
                + " WHERE " + KEY_HIS_KEY + " = '" + audioPath.getHis_key()+"'", null);

        int count = cursor.getCount();

        if (count > 0 && cursor.moveToFirst()) {
            ourDatabase.update(TABLE_AUDIO, cv, KEY_HIS_KEY + " = ?",
                    new String[]{audioPath.getHis_key()});
        } else {

            long rowId = ourDatabase.insert(TABLE_AUDIO, null, cv);

            if (rowId > 0) {
                insertAudioRef(rowId, audioPath.getDatabase_ref());
            }
        }

        cursor.close();

        ourDatabase.setTransactionSuccessful();
    } catch (Exception e) {
//        Crashlytics.logException(e);
    } finally {
        ourDatabase.endTransaction();
    }

    }

    private void insertAudioRef(long rowId, List<String> databaseRefList) {
        for (String databaseRef : databaseRefList) {
            ContentValues contentValues = new ContentValues();
            contentValues.put(KEY_ROWID_REF, rowId);
            contentValues.put(KEY_DataBase_REF, databaseRef);
            long val=ourDatabase.insert(TABLE_AUDIO_PATH_REF, null, contentValues);
            System.out.print(val);
        }
    }


    public Audio getLastAudio() {

        String[] columns = new String[]{KEY_ROWID, KEY_HIS_KEY,
                KEY_STORAGE_PATH, KEY_FILEPATH};

        Cursor cursor = ourDatabase.query(TABLE_AUDIO, columns, null, null,
                null, null, KEY_ROWID + " DESC ", "1");

        Audio audio = new Audio();

        if (cursor.moveToFirst()) {
            audio.set_id(cursor.getInt(cursor.getColumnIndex(KEY_ROWID)));
            audio.setFile_path(cursor.getString(cursor.getColumnIndex(KEY_FILEPATH)));
            audio.setHis_key(cursor.getString(cursor.getColumnIndex(KEY_HIS_KEY)));
            audio.setStorage_path(cursor.getString(cursor.getColumnIndex(KEY_STORAGE_PATH)));
        }

        cursor.close();
        return audio;
    }




/*
    private static final String CREATE_TABLE_AUDIO = "CREATE TABLE IF NOT EXISTS "+TABLE_AUDIO+
            " ( "+KEY_ROWID+" INTEGER PRIMARY KEY AUTOINCREMENT, "
            +KEY_HIS_KEY+ " TEXT NOT NULL, "
            +KEY_STORAGE_PATH+ " TEXT NOT NULL, "
            +KEY_FILEPATH+ " TEXT NOT NULL  ) ";

    private static final String CREATE_TABLE_AUDIO_PATH_REF = "CREATE TABLE IF NOT EXISTS "+TABLE_AUDIO_PATH_REF
            +" ( "+KEY_ROWID+" INTEGER, "
            +KEY_DataBase_REF+" TEXT  ) ";
*/







    public Audio getAudioByHisKey(String hisKey) {
        String[] columns = new String[]{KEY_ROWID,
                KEY_HIS_KEY,
                KEY_STORAGE_PATH, KEY_FILEPATH};


        Cursor cursor = ourDatabase.query(TABLE_AUDIO, columns,
                KEY_HIS_KEY + " = ? ",
                new String[]{hisKey}, null, null, null, "1");


        Audio audio = new Audio();

        if (cursor.moveToFirst()) {
            audio.set_id(cursor.getInt(cursor.getColumnIndex(KEY_ROWID)));
            audio.setFile_path(cursor.getString(cursor.getColumnIndex(KEY_FILEPATH)));
            audio.setHis_key(cursor.getString(cursor.getColumnIndex(KEY_HIS_KEY)));
            audio.setStorage_path(cursor.getString(cursor.getColumnIndex(KEY_STORAGE_PATH)));
        }

        cursor.close();

        return audio;
    }


    public List<String> getAudioRefById(int columnId) {
        List<String> audioref = new ArrayList<>();
        String[] columns = new String[]{KEY_DataBase_REF};
        Cursor cursor = ourDatabase.query(TABLE_AUDIO_PATH_REF, columns,KEY_ROWID_REF + " = ? ",
                new String[]{String.valueOf(columnId)}, null, null, null);

        if (cursor.moveToFirst()) {
            do {
                audioref.add(cursor.getString(cursor.getColumnIndex(KEY_DataBase_REF)));
            } while (cursor.moveToNext());
        }
        cursor.close();
        return audioref;
    }

    public void deleteAudio(int columnId) {
        ourDatabase.beginTransaction();
        try {
            long l = ourDatabase.delete(TABLE_AUDIO,
                    KEY_ROWID + " = ?", new String[]{String.valueOf(columnId)});
            long l2 = ourDatabase.delete(TABLE_AUDIO_PATH_REF,
                    KEY_ROWID_REF + " = ? ", new String[]{String.valueOf(columnId)});
            ourDatabase.setTransactionSuccessful();
        } catch (Exception e) {
//            Crashlytics.logException(e);
        } finally {
            ourDatabase.endTransaction();
        }
    }








}
