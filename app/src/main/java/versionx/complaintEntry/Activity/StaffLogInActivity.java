package versionx.complaintEntry.Activity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.provider.Settings;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;

import java.util.HashMap;

import versionx.complaintEntry.Firebase.PersistentDatabase;
import versionx.complaintEntry.R;
import versionx.complaintEntry.Utils.Global;

public class StaffLogInActivity extends AppCompatActivity implements View.OnClickListener {
    private EditText et_staff_login_code;
    private SharedPreferences loginSP;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_staff_log_in);

        loginSP = getSharedPreferences(Global.LOGIN_SP, Context.MODE_PRIVATE);


        initGUI();
    }

    private void initGUI() {
        et_staff_login_code = (EditText) findViewById(R.id.et_dc_staff_login_code);
        Button btn_staff_login_btn = (Button) findViewById(R.id.btn_dc_login_btn);
        btn_staff_login_btn.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.btn_dc_login_btn) {
            String code = et_staff_login_code.getText().toString().trim();
            if (code.length() == 0) {
                et_staff_login_code.setError("Enter your phone number..");
                return;
            }
            checkStaffExists(code);
        }
    }

    private void checkStaffExists(final String code) {

        DatabaseReference accessRef = PersistentDatabase.getPrimaryDatabase().getReference("access")
                .child(loginSP.getString(Global.BIZ_ID, "")).child(loginSP.getString(Global.GROUP_ID, ""));

        accessRef.keepSynced(true);

        accessRef.orderByChild("f/mob/val").equalTo(code).limitToFirst(1)
                .addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        if (dataSnapshot.exists()) {
                            DataSnapshot staffSnap = dataSnapshot.getChildren().iterator().next();
                            loginSP.edit().putBoolean(Global.STAFF_LOGGED_IN, true).apply();
                            loginSP.edit().putString(Global.STAFF_KEY, staffSnap.getKey()).apply();
                            loginSP.edit().putString(Global.STAFF_NAME, staffSnap.child("f").child("nm").child("val").getValue(String.class)).apply();
                            loginSP.edit().putString(Global.STAFF_MOB, staffSnap.child("f").child("mob").child("val").getValue(String.class)).apply();


                            String cmpValues = "";
                            if (staffSnap.hasChild("cmp")) {
                                for (DataSnapshot snapshot : staffSnap.child("cmp").child("grp").getChildren()) {
                                    if (cmpValues.equals("")) {
                                        cmpValues = snapshot.getKey();
                                    } else {
                                        cmpValues = cmpValues + "," + snapshot.getKey();
                                    }
                                }
                            }

                            loginSP.edit().putString(Global.COMPLAINT_GROUP_TYPE, cmpValues).apply();

                            String uuid = Settings.Secure.getString(getContentResolver(),
                                    Settings.Secure.ANDROID_ID);
                            loginSP.edit().putString(Global.UUID, uuid).apply();

                            DatabaseReference deviceRef = PersistentDatabase.getPrimaryDatabase().getReference("device/" +
                                    loginSP.getString(Global.BIZ_ID, "") + "/complaintEntry/" + uuid);

                            HashMap<String, Object> oprMap = new HashMap<String, Object>();

                            HashMap<String, Object> map = new HashMap<String, Object>();
                            map.put("nm", staffSnap.child("f").child("nm").child("val").getValue(String.class));
                            map.put("mob", staffSnap.child("f").child("mob").child("val").getValue(String.class));
                            if (staffSnap.child("f").hasChild("em")) {
                                map.put("em", staffSnap.child("f").child("em").child("val").getValue(String.class));
                            }
                            map.put("id", staffSnap.getKey());
                            oprMap.put("opr", map);
                            deviceRef.updateChildren(oprMap);


                            Intent cmpIntent = new Intent(StaffLogInActivity.this, ComplaintsActivity.class);
                            startActivity(cmpIntent);
                            finish();

                        } else {
                            et_staff_login_code.setError("Invalid number..");
                        }
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });
    }

}
