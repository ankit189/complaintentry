package versionx.complaintEntry.Activity;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.media.MediaRecorder;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatEditText;
import androidx.appcompat.widget.Toolbar;
import androidx.cardview.widget.CardView;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.devlomi.record_view.OnRecordClickListener;
import com.devlomi.record_view.OnRecordListener;
import com.devlomi.record_view.RecordButton;
import com.devlomi.record_view.RecordView;
import com.example.cameraxexample.CameraXActivity;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import nl.changer.audiowife.AudioWife;
import studio.carbonylgroup.textfieldboxes.ExtendedEditText;
import studio.carbonylgroup.textfieldboxes.SimpleTextChangedWatcher;
import studio.carbonylgroup.textfieldboxes.TextFieldBoxes;
import versionx.complaintEntry.Adapter.AutoCompleteComplaintTextAdapter;
import versionx.complaintEntry.Adapter.CategoryAda;
import versionx.complaintEntry.Adapter.ComplaintLabelAdapter;
import versionx.complaintEntry.Adapter.ImageAdapter;
import versionx.complaintEntry.Firebase.PersistentDatabase;
import versionx.complaintEntry.GetterSetter.Audio;
import versionx.complaintEntry.GetterSetter.Base;
import versionx.complaintEntry.GetterSetter.Category;
import versionx.complaintEntry.GetterSetter.Complaint;
import versionx.complaintEntry.GetterSetter.ComplaintLabel;
import versionx.complaintEntry.GetterSetter.FieldInfo;
import versionx.complaintEntry.GetterSetter.ImagePath;
import versionx.complaintEntry.GetterSetter.RefTable;
import versionx.complaintEntry.GetterSetter.Topic;
import versionx.complaintEntry.OfflineDataBase.FirebaseImagePath;
import versionx.complaintEntry.R;
import versionx.complaintEntry.Utils.CommonMethods;
import versionx.complaintEntry.Utils.Global;


public class AddComplaintActivity extends AppCompatActivity implements View.OnClickListener, CompoundButton.OnCheckedChangeListener {

    int mYear = 0, mMonth = 0, mDay = 0, mHour = 0, mMinute = 0;
    MenuItem action_ack;

    ExtendedEditText et_Source, et_Remarks;
    TextFieldBoxes tb_description, tfb_Source;

    Button btn_Submit;
    ImageView btn_addPhoto;

    private String photoPath;
    boolean imageIsPresent = false;
    private Bitmap bmp = null;
    private ArrayList<ImagePath> imgList;
    RecyclerView rv_complaintImages;
    ImageAdapter imageAdapter;
    SharedPreferences loginSp;
    private String bizKey, grpKey;
    int typ = 2;

    AutoCompleteComplaintTextAdapter staffListAdapter;
    private List<Base> staffList;
    String staffId;


    CategoryAda categoryAdapter;
    ArrayList<Topic> topicArrayList;
    ArrayList<Topic> mainTopicArrayList;

    private ValueEventListener staffValueEventListener, roomValueEventListener, residentValueEventListener;
    private ChildEventListener stfChildEventListener, roomChildEventListener, residentChildEventListener;
    private File complaintImageFile;
    private String tempFile;
    private PhotoReceiver photoReceiver;


    private LinearLayout ll_mediaPlayer;
    private MediaRecorder mediaRecorder;
    private String mediaRecorderPath;
    private RelativeLayout rl_record_audio;
    private AppCompatEditText et_comments;
    private boolean mStartRecording = false;

    private Handler timerHandler;
    private Runnable timerRunnable;

    private ArrayList<Base> spinner_List;


    private ArrayList<Category> sp_List;
    private Complaint complaint;

    private ExtendedEditText et_Complaint_Topic;
    private TextFieldBoxes tf_Complaint_Topic;

    private Topic selectedTopics;
    private Base selectedSource;
    private ArrayList<String> qrCmtList;
    private ArrayList<String> qrStatusList;
    private ArrayList<Base> stList;
    private HashMap<String, Object> ntfySelect = new HashMap<>();

    //Lable
    ArrayList<FieldInfo> labelsArrayList;
    // ArrayList<FieldInfo> selectedlbl;
    LinearLayout ll1, ll_option;
    //CheckBox[] checkBox;
    private int pos = -1;


    private List<ComplaintLabel> complaintLabels;
    private ComplaintLabelAdapter complaintLabelAdapter;
    private RecyclerView rv_complaint_label_list;
    Toolbar toolbar;
    //  ImageView actionAssigned, actionStatus;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_complaint);
        loginSp = getSharedPreferences(Global.LOGIN_SP, Context.MODE_PRIVATE);
        bizKey = loginSp.getString(Global.BIZ_ID, "");
        grpKey = loginSp.getString(Global.GROUP_ID, "");
        //selectedlbl = new ArrayList<>();


        requestPermissions();

        initGUI();
        initRecorderView();
        getLabelSetting();
        getStaffList();
        getTopicList();
        getQuickComment();
        getStatusList();
    }

    private void initGUI() {
        imgList = new ArrayList<>();
        staffList = new ArrayList<>();
        spinner_List = new ArrayList<>();
        sp_List = new ArrayList<>();
        complaint = new Complaint();
        toolbar = findViewById(R.id.toolbar);
        toolbar.setTitle("Add Complaint");
        toolbar.setTitleTextColor(ContextCompat.getColor(this, R.color.white));

        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        //selectedlbl = new ArrayList<>();

        //ll_option = findViewById(R.id.ll_Label);

        tb_description = findViewById(R.id.tb_description);
        et_Source = (ExtendedEditText) findViewById(R.id.et_Complaint_Staff);
        tfb_Source = (TextFieldBoxes) findViewById(R.id.tfb_Source);
        et_Remarks = (ExtendedEditText) findViewById(R.id.et_Complaints_Description);
        btn_addPhoto = (ImageView) findViewById(R.id.btn_complaint_add_photo);
        btn_Submit = (Button) findViewById(R.id.btn_Complaint_Submit);
        ll_mediaPlayer = (LinearLayout) findViewById(R.id.ll_media_player);
        rl_record_audio = (RelativeLayout) findViewById(R.id.rl_record_audio);

        et_Complaint_Topic = findViewById(R.id.et_Complaint_Cat);
        tf_Complaint_Topic = findViewById(R.id.tf_Complaint_Topic);

        topicArrayList = new ArrayList<>();
        mainTopicArrayList = new ArrayList<>();
        categoryAdapter = new CategoryAda(getBaseContext(), android.R.layout.simple_dropdown_item_1line, topicArrayList);
        et_Complaint_Topic.setAdapter(categoryAdapter);
        //et_Complaint_Topic.setThreshold(0);
        et_Complaint_Topic.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                et_Complaint_Topic.setText(topicArrayList.get(position).getNm());
                et_Complaint_Topic.setTag(topicArrayList.get(position).getTopicId());
                pos = position;
                selectedTopics = topicArrayList.get(position);
            }
        });

        staffListAdapter = new AutoCompleteComplaintTextAdapter(getBaseContext(), android.R.layout.simple_dropdown_item_1line, staffList);
        et_Source.setAdapter(staffListAdapter);
        //et_Source.setThreshold(0);

        et_Source.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                Base selectedStaff = staffList.get(position);

                String staffNm = selectedStaff.getNm();
                if (selectedStaff.getRmNo() != null) {
                    staffNm = getResources().getString(R.string.comp_source_room_name, selectedStaff.getRmNo(), selectedStaff.getNm());
                } else if (selectedStaff.getAptNo() != null) {
                    staffNm = getResources().getString(R.string.comp_source_apt_name, selectedStaff.getNm(), selectedStaff.getAptNo());
                }

                et_Source.setText(staffNm);
                et_Source.setTag(staffList.get(position).getId());
                selectedSource = staffList.get(position);
            }
        });


        rv_complaintImages = (RecyclerView) findViewById(R.id.rv_complaint_images);
        rv_complaintImages.setLayoutManager(new GridLayoutManager(getApplicationContext(), 3));
        imageAdapter = new ImageAdapter(AddComplaintActivity.this, imgList, complaint.getKey());
        rv_complaintImages.setAdapter(imageAdapter);

        if (getIntent().getSerializableExtra("Complaint") != null) {
            complaint = (Complaint) getIntent().getSerializableExtra("Complaint");
            setValues(complaint);
        } else if (getIntent().getBooleanExtra("notification", false) &&
                getIntent().getStringExtra("id") != null) {
            getValues(getIntent().getStringExtra("id"));
        }


        tfb_Source.setSimpleTextChangeWatcher(new SimpleTextChangedWatcher() {
            @Override
            public void onTextChanged(String s, boolean b) {
                selectedSource = null;
            }
        });


        tf_Complaint_Topic.setSimpleTextChangeWatcher(new SimpleTextChangedWatcher() {
            @Override
            public void onTextChanged(String s, boolean b) {
                selectedTopics = null;
            }
        });


        btn_addPhoto.setOnClickListener(this);
        btn_Submit.setOnClickListener(this);


//        photoReceiver = new PhotoReceiver();


        rv_complaint_label_list = findViewById(R.id.rv_complaint_label);
        complaintLabels = new ArrayList<>();
        complaintLabelAdapter = new ComplaintLabelAdapter(complaintLabels);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);

        rv_complaint_label_list.setLayoutManager(linearLayoutManager);
        rv_complaint_label_list.setHasFixedSize(true);

        rv_complaint_label_list.setAdapter(complaintLabelAdapter);

        LinearLayout ll_img = findViewById(R.id.ll_comp_img);
        CardView cv_aud = findViewById(R.id.cv_comp_aud);

        ll_img.setVisibility(loginSp.getBoolean(Global.COMP_IMG, false) ? View.VISIBLE : View.GONE);
        cv_aud.setVisibility(loginSp.getBoolean(Global.COMP_AUDIO, false) ? View.VISIBLE : View.GONE);
    }

    private void getStatusList() {
        qrStatusList = new ArrayList<>();
        stList = new ArrayList<>();
        Base base1 = new Base();
        base1.setId("st");
        base1.setNm("Status");
        stList.add(0, base1);
        final DatabaseReference fieldRef = PersistentDatabase.getSecondaryDatabase().getReference("complaint/setting/" + bizKey + "/" + grpKey + "/st/");
        fieldRef.keepSynced(true);
        fieldRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()) {
                    for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                        System.out.println(snapshot);
                        if (!snapshot.getKey().equals("O")) {
//                            qrStatusList.add(snapshot.child("nm").getValue(String.class));
                            Base base = new Base();
                            base.setId(snapshot.getKey());
                            base.setNm(snapshot.child("nm").getValue(String.class));
                            stList.add(base);
                        }
                    }
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }


    private void getQuickComment() {
        qrCmtList = new ArrayList<>();
//        getSecondaryDatabase()
        final DatabaseReference qcRef = PersistentDatabase.getSecondaryDatabase().getReference("complaint/setting/" + bizKey
                + "/" + grpKey + "/qc");
        qcRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()) {
                    for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                        qrCmtList.add(snapshot.child("nm").getValue(String.class));
                    }
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });


    }

    /* private void setLabels(final ArrayList<FieldInfo> labelArrayList) {
         if (ll_option != null) {
             ll_option.removeAllViews();
         }
         checkBox = new CheckBox[labelArrayList.size()];
         for (int i = 0; i < labelArrayList.size(); i++) {
             ll1 = new LinearLayout(getApplicationContext());
             LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(
                     LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
             layoutParams.setMargins(15, 10, 0, 0);
             checkBox[i] = new CheckBox(getApplicationContext());
             checkBox[i].setText(labelArrayList.get(i).getNm());
             checkBox[i].setTextSize(14);
             checkBox[i].setId(i);
             checkBox[i].setTextColor(Color.parseColor("#404040"));
             if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                 checkBox[i].setButtonTintList(ColorStateList.valueOf(ContextCompat.getColor(this, R.color.colorPrimary)));
             } else {
                 checkBox[i].setHighlightColor(getResources().getColor(R.color.colorPrimary));
             }
             checkBox[i].setTag(labelArrayList.get(i).getId());
             ll1.addView(checkBox[i], layoutParams);
             ll_option.addView(ll1);
             checkBox[i].setOnCheckedChangeListener(this);
         }


         if (selectedlbl != null && selectedlbl.size() != 0) {
             for (int i = 0; i < labelArrayList.size(); i++) {
                 for (int j = 0; j < selectedlbl.size(); j++) {
                     if (labelArrayList.get(i).getId().equals(selectedlbl.get(j).getId())) {
                         checkBox[i].setChecked(true);
                         continue;
                     }
                 }
             }
         }


     }
 */
    private void getLabelSetting() {
        //labelsArrayList = new ArrayList<>();
//        getSecondaryDatabase()
        final DatabaseReference fieldRef = PersistentDatabase.getSecondaryDatabase().getReference("complaint/setting/" + bizKey + "/" + grpKey + "/label/");
        fieldRef.keepSynced(true);
        fieldRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                //labelsArrayList.clear();
                for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                   /* FieldInfo fieldInfo1 = snapshot.getValue(FieldInfo.class);
                    if (!fieldInfo1.isHide()) {
                        fieldInfo1.setId(snapshot.getKey());
                        labelsArrayList.add(fieldInfo1);
                    }*/

                    ComplaintLabel complaintLabel = new ComplaintLabel(snapshot.getKey(), snapshot.child("nm").getValue().toString());

                    if (complaint != null && complaint.getLbl() != null) {
                        for (Map.Entry<String, Object> map : complaint.getLbl().entrySet()) {
                            if (map.getKey().equalsIgnoreCase(complaintLabel.getKey())) {
                                complaintLabel.setSelected(true);
                            }
                        }
                    }

                    complaintLabels.add(complaintLabel);
                }

                complaintLabelAdapter.notifyDataSetChanged();

                // setLabels(labelsArrayList);


            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    private void requestPermissions() {

        if (ContextCompat.checkSelfPermission(AddComplaintActivity.this, android.Manifest.permission.RECORD_AUDIO) != PackageManager.PERMISSION_GRANTED
                || ContextCompat.checkSelfPermission(AddComplaintActivity.this, android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                requestPermissions(new String[]{android.Manifest.permission.RECORD_AUDIO, android.Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1234);
            }
        }
    }


    private void getValues(String id) {
        DatabaseReference complaintRef = PersistentDatabase.getSecondaryDatabase().
                getReference("complaint/log/" + bizKey + "/" + grpKey + "/open/" + id);
        complaintRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                if (snapshot.exists()) {
                    complaint = Complaint.getComplaint(snapshot);
                    setValues(complaint);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });
    }

    private void setValues(Complaint complaint) {

        if (complaint.getSt() != null && complaint.getSt().equals("C")) {
            btn_Submit.setVisibility(View.GONE);
        }
        if (complaint.getRmks() != null && !complaint.getRmks().equals("")) {
            et_Remarks.setText(complaint.getRmks());
        }
        if (complaint.getSrc() != null) {
            ArrayList<Base> srcList = complaint.getSrc();
            selectedSource = srcList.get(0);
            if (selectedSource.getId() != null) {
                et_Source.setTag(selectedSource.getId());
            }
            if (selectedSource.getNm() != null) {
                et_Source.setText(selectedSource.getNm());
            } else if (selectedSource.getRmNo() != null) {
                et_Source.setText(selectedSource.getRmNo());
            }
        }
        ArrayList<Topic> topicList = complaint.getTpc();
        if (topicList != null && topicList.size() > 0) {
            et_Complaint_Topic.setText(topicList.get(0).getNm());
            selectedTopics = topicList.get(0);

        } else if (complaint.getOthr() != null && !complaint.getOthr().equals("")) {
            et_Complaint_Topic.setText(complaint.getOthr());
        }

        imageAdapter = new ImageAdapter(AddComplaintActivity.this, imgList, complaint.getKey());
        rv_complaintImages.setAdapter(imageAdapter);

      /*  HashMap<String, Object> lblMap = complaint.getLbl();
        if (lblMap != null && lblMap.size() != 0) {
            for (Map.Entry<String, Object> map : lblMap.entrySet()) {
                System.out.print(map);
                FieldInfo fieldInfo = new FieldInfo();
                fieldInfo.setId(map.getKey());
                fieldInfo.setVal(map.getValue().toString());
                selectedlbl.add(fieldInfo);
            }
        }*/


        if (complaint.getAudio() != null) {
            initAudioPlayer(Uri.parse(complaint.getAudio()));
        } else {
            if (complaint.getKey() != null) {
                FirebaseImagePath localDatabase = new FirebaseImagePath(getApplicationContext());
                localDatabase.open();
                Audio audio = localDatabase.getAudioByHisKey(complaint.getKey());
                localDatabase.close();
                if (audio.getFile_path() != null) {
                    initAudioPlayer(Uri.parse(audio.getFile_path()));
                }
            }
        }

        if (complaint.getImg().size() > 0) {
            imgList.addAll(complaint.getImg());
            imageAdapter.notifyDataSetChanged();
        } else {
            FirebaseImagePath firebaseImagePath = new FirebaseImagePath(this);
            firebaseImagePath.open();
            imgList.addAll(firebaseImagePath.getAllImage(complaint.getKey()));
            imageAdapter.notifyDataSetChanged();
            firebaseImagePath.close();
        }
    }

    private void initRecorderView() {
        final CardView cv_audio = (CardView) findViewById(R.id.cv_comp_aud);
        final RecordView recordView = (RecordView) findViewById(R.id.record_view);
        final RecordButton recordButton = (RecordButton) findViewById(R.id.record_button);
        final LinearLayout linearLayout = (LinearLayout) findViewById(R.id.ll_media_player);

        //IMPORTANT
        recordButton.setRecordView(recordView);


        recordButton.setOnRecordClickListener(new OnRecordClickListener() {
            @Override
            public void onClick(View v) {

            }
        });


        //Cancel Bounds is when the Slide To Cancel text gets before the timer . default is 130
        recordView.setCancelBounds(50);

        recordView.setSoundEnabled(false);


        recordView.setSmallMicColor(Color.parseColor("#c2185b"));

        //prevent recording under one Second
        recordView.setLessThanSecondAllowed(false);


        recordView.setSlideToCancelText("Slide To Cancel");

        recordView.setCustomSounds(R.raw.record_start, R.raw.record_finished, 0);

        timerHandler = new Handler();
        timerRunnable = new Runnable() {
            @Override
            public void run() {
                recordView.cancelRecordView(recordButton);
            }
        };


        recordView.setOnRecordListener(new OnRecordListener() {
            @Override
            public void onStart() {
                Log.d("RecordView", "onStart");
                mediaRecorderPath = CommonMethods.createMediaFile(getApplicationContext());
                initializeMediaRecord();
                startMediaRecord();
                /*Stopping the record after 10 seconds*/
                timerHandler.postDelayed(timerRunnable, 11000);

            }

            @Override
            public void onCancel() {
                Log.d("RecordView", "onCancel");
                stopMediaRecord();
                timerHandler.removeCallbacks(timerRunnable);
                CommonMethods.deleteImages(mediaRecorderPath);
            }

            @Override
            public void onFinish(long recordTime) {

                stopMediaRecord();

                linearLayout.removeAllViews();

                timerHandler.removeCallbacks(timerRunnable);

                if (recordTime >= 2000) {
                    if (cv_audio.getVisibility() == View.VISIBLE) {
                        initAudioPlayer(Uri.fromFile(new File(mediaRecorderPath)));
                    }
                } else {
                    CommonMethods.deleteImages(mediaRecorderPath);
                }
            }

            @Override
            public void onLessThanSecond() {
                Toast.makeText(getApplicationContext(), "OnLessThanSecond", Toast.LENGTH_SHORT).show();
                Log.d("RecordView", "onLessThanSecond");

                CommonMethods.deleteImages(mediaRecorderPath);

                timerHandler.removeCallbacks(timerRunnable);
            }
        });

    }

    public void deleteRecordAudio(String path) {
        File file = new File(path);
        if (file.exists() || file.getAbsoluteFile().exists()) {
            file.delete();
        }
      /*  if(path.startsWith("https")){
            StorageReference imageRef  = FirebaseStorage.getInstance().getReferenceFromUrl(path);
            imageRef.delete().addOnSuccessListener(new OnSuccessListener<Void>() {
                @Override
                public void onSuccess(Void aVoid) {
                    System.out.println("Audio File Deleted....");
                    HashMap<String,Object> refMap = new HashMap<>();
                    refMap.put("appt/"+loginSP.getString(Global.BIZ_KEY,"")
                            +"/"+loginSP.getString(Global.GROUP_KEY,"")+"/"+appointment.getKey()+"/audio/",null);
                    refMap.put("visitor/"+loginSP.getString(Global.BIZ_KEY,"")
                            +"/"+loginSP.getString(Global.GROUP_KEY,"")
                            +"/"+appointment.getMob().substring(0,4)+"/"+appointment.getMob()+"/appt/"+Common.getOnlyDate(Long.valueOf
                            (appointment.getAppt().get("frtm").toString()))+"/audio/",null);

                    DatabaseUtil.getDatabase().getReference().updateChildren(refMap);
                }
            });
        }*/
    }

    private void initAudioPlayer(Uri uri) {
        View playerUi = getLayoutInflater().inflate(R.layout.aw_player, ll_mediaPlayer);
        View playView = playerUi.findViewById(R.id.play);
        View pauseView = playerUi.findViewById(R.id.pause);
        View deleteView = playerUi.findViewById(R.id.aud_delete);
        SeekBar seekBar = (SeekBar) playerUi.findViewById(R.id.media_seekbar);
        TextView playbackTime = (TextView) playerUi.findViewById(R.id.playback_time);


        deleteView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new androidx.appcompat.app.AlertDialog.Builder(AddComplaintActivity.this).setMessage("Are you sure ?").setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        ll_mediaPlayer.removeAllViews();
                        deleteRecordAudio(mediaRecorderPath);
                    }
                }).setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                }).create().show();
            }
        });

        AudioWife.getInstance().init(getApplicationContext(), uri)
                .setPlayView(playView)
                .setPauseView(pauseView)
                .setSeekBar(seekBar)
                .setPlaytime(playbackTime);

    }

    private void initializeMediaRecord() {
        if (mediaRecorder == null) {
            mediaRecorder = new MediaRecorder();
            mediaRecorder.setAudioSource(MediaRecorder.AudioSource.MIC);
            mediaRecorder.setOutputFormat(MediaRecorder.OutputFormat.MPEG_4);
            mediaRecorder.setAudioEncodingBitRate(16);
            mediaRecorder.setAudioSamplingRate(44100);
            mediaRecorder.setAudioEncoder(MediaRecorder.AudioEncoder.AAC);
            mediaRecorder.setOutputFile(mediaRecorderPath);
        }
    }

    private void startMediaRecord() {
        if (!mStartRecording) {
           /* try {
                mediaRecorder.prepare();
                mediaRecorder.start();
                mStartRecording=true;
            } catch (IOException e) {
                e.printStackTrace();
            }*/

            try {
                mediaRecorder.prepare();
                mediaRecorder.start();
                mStartRecording = true;
            } catch (IllegalStateException e) {
                e.printStackTrace();
//                Crashlytics.log(e.toString());
            } catch (IOException e) {
                e.printStackTrace();
//                Crashlytics.log(e.toString());
            }
        }

    }

    private void stopMediaRecord() {

        try {
            if (mediaRecorder != null) {
                mediaRecorder.stop();
                mediaRecorder.release();
                mediaRecorder = null;
                mStartRecording = false;
            }
        } catch (RuntimeException stopException) {

        }

    }

    private void getTopicList() {

//        getSecondaryDatabase()
        DatabaseReference dbref = PersistentDatabase.getSecondaryDatabase().getReference("complaint/setting/" +
                loginSp.getString(Global.BIZ_ID, "") + "/" + loginSp.getString(Global.GROUP_ID, "") + "/topic/");
        dbref.keepSynced(true);
        dbref.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull final DataSnapshot dataSnapshot) {

/*
                for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                    Topic topic1 = snapshot.getValue(Topic.class);
                    topic1.setTopicId(snapshot.getKey());
                    System.out.print(topic1);
                    topicArrayList.add(topic1);
                }

                Collections.sort(topicArrayList, new Comparator<Topic>() {
                    @Override
                    public int compare(Topic o1, Topic o2) {
                        return o1.getNm().compareTo(o2.getNm());
                    }
                });

                categoryAdapter.customdatasetChanged();*/

                AsyncTask.execute(new Runnable() {
                    @Override
                    public void run() {
                        for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                            Topic topic1 = snapshot.getValue(Topic.class);
                            topic1.setTopicId(snapshot.getKey());
                            topicArrayList.add(topic1);
                            mainTopicArrayList.add(topic1);
                        }

                        System.out.println("topicArrayList =" + topicArrayList.size());
                        Collections.sort(topicArrayList, new Comparator<Topic>() {
                            @Override
                            public int compare(Topic o1, Topic o2) {
                                return o1.getNm().compareTo(o2.getNm());
                            }
                        });

                        categoryAdapter.customdatasetChanged();
                    }
                });

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {

            case R.id.btn_complaint_add_photo:

                dispatchTakePictureIntent(100); // taking picture from camera

                break;

            case R.id.btn_Complaint_Submit:
                if (et_Source.getText().toString().trim().equals("")) {
                    tfb_Source.setError("Can't be empty", true);
                    return;
                }

                if (et_Complaint_Topic.getText().toString().trim().equals("")) {
                    tf_Complaint_Topic.setError("Can't be empty", true);
                    return;
                }


                showDialogBox();

                try {
                    InputMethodManager inputManager = (InputMethodManager)
                            getSystemService(Context.INPUT_METHOD_SERVICE);

                    inputManager.hideSoftInputFromWindow(btn_Submit.getWindowToken(),
                            InputMethodManager.HIDE_NOT_ALWAYS);
                } catch (Exception e) {

                }
                break;


        }

    }

    private void showDialogBox() {
        AlertDialog.Builder dialog = new AlertDialog.Builder(AddComplaintActivity.this);
        dialog.setTitle("Are you sure?");
        dialog.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                saveComplaint(complaint);
                et_Source.setEnabled(true);


            }
        });
        dialog.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        dialog.show();

    }

    private void showAckDialogBox() {
        AlertDialog.Builder dialog = new AlertDialog.Builder(AddComplaintActivity.this);
        dialog.setTitle("Are you sure");
        dialog.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                action_ack.setVisible(false);
                HashMap<String, Object> map = new HashMap<>();
                HashMap<String, Object> ackMap = new HashMap<>();
                map.put("id", loginSp.getString(Global.STAFF_KEY, ""));
                map.put("nm", loginSp.getString(Global.STAFF_NAME, ""));
                ackMap.put("ack", map);
                DatabaseReference complaintRef = PersistentDatabase.getSecondaryDatabase().
                        getReference("complaint/log/" + bizKey + "/" + grpKey + "/open/" + complaint.getKey());
                complaintRef.updateChildren(ackMap);

            }
        });
        dialog.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        dialog.show();

    }

    @Override
    public void onCheckedChanged(CompoundButton compoundButton, boolean b) {

    }

    public class PhotoReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent data) {
            final int requestCode = data.getIntExtra("requestCode", 0);
            if (requestCode == 100) {
                if (data.getStringExtra("path") != null) {
                    photoPath = data.getStringExtra("path");
                    final File file = new File(data.getStringExtra("path"));
                    if (file.exists()) {
                        imgList.add(new ImagePath(System.currentTimeMillis() + "", photoPath.toString()));
                        imageAdapter.notifyDataSetChanged();
                        imageIsPresent = true;
//                        Glide.with(getContext()).load(file).asBitmap().override(200,200).fitCenter().diskCacheStrategy(DiskCacheStrategy.RESULT).into(new BitmapImageViewTarget(imageView_courier_Photo) {
//                            @Override
//                            protected void setResource(Bitmap resource) {
//                                RoundedBitmapDrawable circularBitmapDrawable =
//                                        RoundedBitmapDrawableFactory.create(getContext().getResources(), resource);
//                                circularBitmapDrawable.setCircular(true);
//                                imageView_courier_Photo.setImageDrawable(circularBitmapDrawable);
//                            }
//                        });

                    } else {
                        Toast.makeText(getApplicationContext(), "Something went wrong! try again", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        }
    }

    private void getStaffList() {

        final DatabaseReference fieldRef = PersistentDatabase.getSecondaryDatabase()
                .getReference("complaint/setting/" + bizKey + "/" + grpKey + "/src/");
//        System.out.println("fieldRef "+fieldRef.getPath());
        fieldRef.keepSynced(true);
        fieldRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()) {
                    for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                        if (snapshot.getKey().equals("access") && snapshot.child("val").getValue().equals(true)) {
                            final DatabaseReference staffRef = PersistentDatabase.getPrimaryDatabase()
                                    .getReference("access")
                                    .child(bizKey).child(grpKey);
                            staffRef.keepSynced(true);
                            staffRef.addListenerForSingleValueEvent(new ValueEventListener() {
                                @Override
                                public void onDataChange(DataSnapshot dataSnapshot) {
                                    if (dataSnapshot.exists()) {
                                        for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                                            if (snapshot.hasChild("act") && snapshot.child("act").getValue().equals(true)) {
//                                                System.out.println("test=="+snapshot.getKey());
                                                Base complaint = snapshot.getValue(Base.class);
                                                System.out.println("fieldRef " + snapshot.getKey());
                                                if (snapshot.hasChild("f") && snapshot.child("f").hasChild("nm")) {
                                                    complaint.setNm(snapshot.child("f").child("nm").child("val").getValue(String.class));
                                                }
                                                if (snapshot.hasChild("f") && snapshot.child("f").hasChild("mob")) {
                                                    System.out.println("CrashPoint :" + snapshot.getKey());
                                                    complaint.setMob(snapshot.child("f").child("mob").child("val").getValue(String.class));
                                                }
                                                if (snapshot.hasChild("cmp") && snapshot.child("cmp").hasChild("ntfy")) {
                                                    complaint.setSrcNtfy((HashMap<String, Object>) snapshot.child("cmp").child("ntfy").getValue());
                                                }
                                                complaint.setId(snapshot.getKey());
                                                complaint.setVal("access");
                                                staffList.add(complaint);
                                            }
                                        }
                                        Collections.sort(staffList, new Comparator<Base>() {
                                            @Override
                                            public int compare(Base o1, Base o2) {
                                                return o1.getNm().compareTo(o2.getNm());
                                            }
                                        });
                                        staffListAdapter.customdatasetChanged();
                                    }
                                }

                                @Override
                                public void onCancelled(DatabaseError databaseError) {
                                }
                            });

                        } else if (snapshot.getKey().equals("room") && snapshot.child("val").getValue().equals(true)) {
                            final DatabaseReference roomRef = PersistentDatabase.getSecondaryDatabase()
                                    .getReference("room")
                                    .child(bizKey).child(grpKey);
                            roomRef.keepSynced(true);
                            roomRef.addListenerForSingleValueEvent(new ValueEventListener() {
                                @Override
                                public void onDataChange(DataSnapshot dataSnapshot) {
                                    if (dataSnapshot.exists()) {
                                        for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                                            Base complaint = new Base();
                                            if (snapshot.hasChild("gNm")) {
                                                complaint.setNm(snapshot.child("gNm").getValue(String.class));
                                            } else {
                                                complaint.setNm("");
                                            }
//                                            complaint.setNm(snapshot.child("gNm").getValue(String.class));
                                            complaint.setMob(snapshot.child("mob").getValue(String.class));
                                            complaint.setRmNo(snapshot.child("no").getValue(String.class));
                                            complaint.setVal("room");
                                            if (snapshot.hasChild("cmp") && snapshot.child("cmp").hasChild("ntfy")) {
                                                complaint.setSrcNtfy((HashMap<String, Object>) snapshot.child("cmp").child("ntfy").getValue());
                                            }
                                            complaint.setId(snapshot.getKey());
                                            staffList.add(complaint);
                                        }
                                       /* Collections.sort(staffList, new Comparator<Base>() {
                                            @Override
                                            public int compare(Base o1, Base o2) {
                                                System.out.println(" Valet "+ o1.getNm()+"=" +o2.getNm()+" = " +o2.getId());
                                                return o1.getNm().compareTo(o2.getNm());
                                            }
                                        });*/
                                        staffListAdapter.customdatasetChanged();
                                    }
                                }

                                @Override
                                public void onCancelled(DatabaseError databaseError) {

                                }
                            });
                        } else if (snapshot.getKey().equals("resident") && snapshot.child("val").getValue().equals(true)) {
                            final DatabaseReference residentRef = PersistentDatabase.getPrimaryDatabase()
                                    .getReference("resident")
                                    .child(bizKey).child(grpKey);
                            residentRef.keepSynced(true);
                            residentRef.addListenerForSingleValueEvent(new ValueEventListener() {
                                @Override
                                public void onDataChange(DataSnapshot dataSnapshot) {
                                    if (dataSnapshot.exists()) {
                                        for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                                            Base complaint = new Base();
                                            if (snapshot.hasChild("f")) {
                                                complaint.setNm(snapshot.child("f").child("nm").child("val").getValue(String.class));
                                                complaint.setAptNo(snapshot.child("f").child("aptNo").child("val").getValue(String.class));
                                                complaint.setMob(snapshot.child("f").child("mob").child("val").getValue(String.class));
                                                if (snapshot.hasChild("cmp") && snapshot.child("cmp").hasChild("ntfy")) {
                                                    complaint.setSrcNtfy((HashMap<String, Object>) snapshot.child("cmp").child("ntfy").getValue());
                                                }
                                                complaint.setId(snapshot.getKey());
                                                complaint.setVal("resident");
                                                staffList.add(complaint);
                                            }
                                        }
                                        Collections.sort(staffList, new Comparator<Base>() {
                                            @Override
                                            public int compare(Base o1, Base o2) {
                                                return o1.getNm().compareTo(o2.getNm());
                                            }
                                        });
                                        staffListAdapter.customdatasetChanged();
                                    }
                                }

                                @Override
                                public void onCancelled(DatabaseError databaseError) {
                                }
                            });
                        } else {
                            final DatabaseReference fieldRef = PersistentDatabase.getSecondaryDatabase()
                                    .getReference("complaint/setting/" + bizKey + "/" + grpKey + "/srcList/" + snapshot.getKey());
                            fieldRef.keepSynced(true);
                            fieldRef.addListenerForSingleValueEvent(new ValueEventListener() {
                                @Override
                                public void onDataChange(DataSnapshot dataSnapshot) {
                                    if (dataSnapshot.exists()) {
                                        for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                                            Base complaint = new Base();
                                            if (snapshot.hasChild("nm")) {
                                                complaint.setNm(snapshot.child("nm").getValue(String.class));
                                            }
                                            if (snapshot.hasChild("cmp") && snapshot.child("cmp").hasChild("ntfy")) {
                                                complaint.setSrcNtfy((HashMap<String, Object>) snapshot.child("cmp").child("ntfy").getValue());
                                            }
                                            complaint.setId(snapshot.getKey());
                                            complaint.setVal(dataSnapshot.getKey());
                                            staffList.add(complaint);
                                        }
                                      /*  Collections.sort(staffList, new Comparator<Base>() {
                                            @Override
                                            public int compare(Base o1, Base o2) {
                                                return o1.getNm().compareTo(o2.getNm());
                                            }
                                        });*/
                                        staffListAdapter.customdatasetChanged();
                                    }
                                }

                                @Override
                                public void onCancelled(DatabaseError databaseError) {
                                }
                            });

                        }
                    }
                }

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.status_menu, menu);

        MenuItem menu_close = menu.findItem(R.id.action_status);
        action_ack = menu.findItem(R.id.action_ack);


        final FirebaseImagePath firebaseImagePath = new FirebaseImagePath(getApplicationContext());
        firebaseImagePath.open();
        if (getIntent().getSerializableExtra("Complaint") == null) {
            menu_close.setVisible(false);
            action_ack.setVisible(false);

        } else {

            complaint = (Complaint) getIntent().getSerializableExtra("Complaint");
            if (complaint.getDt() != 0 &&
                    firebaseImagePath.getRowData(complaint.getKey()).size() == 0) {
                menu_close.setVisible(true);
                firebaseImagePath.close();
            } else {
                menu_close.setVisible(false);
                firebaseImagePath.close();
            }

            if (complaint.getAck() != null) {
                action_ack.setVisible(false);
            }

        }

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;

            case R.id.action_status:
                updateStatus(complaint);
                break;

            case R.id.action_ack:
                showAckDialogBox();
                break;


        }
        return super.onOptionsItemSelected(item);
    }

/*
    private void showTimePickerDialog(long time, final String typ) {

        final Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(time);
        final int hour = calendar.get(Calendar.HOUR_OF_DAY);
        int minute = calendar.get(Calendar.MINUTE);

     */
/*   if (typ.equalsIgnoreCase("frTm")) {
            if (!frTmDialogDirty) {
                minute = 0;
            }
        } else {
            if (!toTmDialogDirty) {
                minute = 0;
            }
        }
*//*

        TimePickerDialog.OnTimeSetListener onTimeSetListener = new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker view, int hourOfDay, int minute) {

                Calendar calendar1 = Calendar.getInstance();
                calendar1.set(Calendar.HOUR_OF_DAY, hourOfDay);
                calendar1.set(Calendar.MINUTE, minute);
                calendar1.set(Calendar.SECOND, 0);

                if (typ.equalsIgnoreCase("frTm")) {
                    et_Tm.setText(CommonMethods.formatTime(calendar1.getTimeInMillis()));
                    frDt.set(Calendar.HOUR_OF_DAY, hourOfDay);
                    frDt.set(Calendar.MINUTE, minute);
                }
            }
        };

        TimePickerDialog timePickerDialog = new TimePickerDialog(this, onTimeSetListener, hour, minute, false);

        timePickerDialog.show();
    }
*/

/*
    private void showDatePickerDialog(long time, final String type) {

        final Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(time);
        int year = calendar.get(Calendar.YEAR);
        int month = calendar.get(Calendar.MONTH);
        int day = calendar.get(Calendar.DAY_OF_MONTH);

        DatePickerDialog.OnDateSetListener onDateSetListener = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {

                Calendar calendar = Calendar.getInstance();
                calendar.set(year, month, dayOfMonth);

                if (type.equalsIgnoreCase("vtd")) {
                    et_date.setText(Common.formatDate(calendar.getTimeInMillis()));
//                    appointment.setVtd(calendar.getTimeInMillis());
                } else if (type.equalsIgnoreCase("frDt")) {
                    et_Dt.setText(Common.formatDate(calendar.getTimeInMillis()));
                    frDt.set(year, month, dayOfMonth);
                }
            }
        };

        DatePickerDialog datepickerDialog = new DatePickerDialog(this, onDateSetListener, year, month, day);

        datepickerDialog.getDatePicker().setMinDate(Calendar.getInstance().getTimeInMillis() - 60000);

        datepickerDialog.show();
    }
*/

    private void updateStatus(Complaint complaint) {
        final Calendar frDt;
        final Calendar calendar = Calendar.getInstance();
        frDt = Calendar.getInstance();
        final Dialog dialog = new Dialog(AddComplaintActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.complaint_status);
        dialog.setCancelable(true);

        // set the custom dialog components - text, image and button
        final String[] status_selected = new String[1];
        final String[] stId = {""};
//        final String[] status_ls=getResources().getStringArray(R.array.status);
        ArrayAdapter spinnerAdapter = new ArrayAdapter(getApplicationContext(), android.R.layout.simple_list_item_1, stList);
        spinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        ArrayAdapter cmtAdapter = new ArrayAdapter(AddComplaintActivity.this, android.R.layout.simple_dropdown_item_1line, qrCmtList);

        final Spinner spinner = (Spinner) dialog.findViewById(R.id.status_list);
        final ExtendedEditText et_comment = (ExtendedEditText) dialog.findViewById(R.id.et_comment);
        final LinearLayout ll_Dt = dialog.findViewById(R.id.ll_Dt);
        final EditText et_Dt = dialog.findViewById(R.id.et_Dt);
        final EditText et_Tm = dialog.findViewById(R.id.et_Tm);

       /* if (complaint.getSt()!=null&&!complaint.getSt().equals("")){
            for (int i=0;i<stList.size();i++){
                if (stList.get(i).getId().equals(complaint.getSt())){
                    String  s=stList.get(i).getNm();
                    spinner.setSelection(stList.indexOf(i));
//                    spinner.setSelection(i);
                    break;
                }
            }
        }*/

        et_Dt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                showDatePickerDialog(frDt.getTimeInMillis(), "frDt");

                final Calendar c = Calendar.getInstance();
                mYear = c.get(Calendar.YEAR);
                mMonth = c.get(Calendar.MONTH);
                mDay = c.get(Calendar.DAY_OF_MONTH);


                DatePickerDialog datePickerDialog = new DatePickerDialog(AddComplaintActivity.this,
                        new DatePickerDialog.OnDateSetListener() {

                            @Override
                            public void onDateSet(DatePicker view, int year,
                                                  int monthOfYear, int dayOfMonth) {
                                calendar.set(year, monthOfYear, dayOfMonth, 0, 0, 0);
                                et_Dt.setText(dayOfMonth + "-" + (monthOfYear + 1) + "-" + year);
//                                frDt.setTimeInMillis(calendar.getTimeInMillis());
                            }
                        }, mYear, mMonth, mDay);
                datePickerDialog.show();

            }
        });
        et_Tm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final Calendar c = Calendar.getInstance();
                mHour = c.get(Calendar.HOUR_OF_DAY);
                mMinute = c.get(Calendar.MINUTE);

                // Launch Time Picker Dialog
                TimePickerDialog timePickerDialog = new TimePickerDialog(AddComplaintActivity.this,
                        new TimePickerDialog.OnTimeSetListener() {

                            @Override
                            public void onTimeSet(TimePicker view, int hourOfDay,
                                                  int minute) {
                                calendar.set(mYear, mMonth, mDay, mHour, mMinute, 0);

                                frDt.setTimeInMillis(calendar.getTimeInMillis());
                                if (minute < 10) {
                                    String m = "0" + minute;
                                    et_Tm.setText(hourOfDay + ":" + m);
                                } else {
                                    et_Tm.setText(hourOfDay + ":" + minute);
                                }
                                frDt.set(Calendar.HOUR_OF_DAY, hourOfDay);
                            }
                        }, mHour, mMinute, false);
                timePickerDialog.show();
            }
        });

        et_comment.setAdapter(cmtAdapter);
        et_comment.setThreshold(0);
        et_comment.showDropDown();

        final Button btn_Save = (Button) dialog.findViewById(R.id.btn_Save);
        spinner.setAdapter(spinnerAdapter);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                // TODO Auto-generated method stub
                status_selected[0] = stList.get(position).getNm();
                stId[0] = stList.get(position).getId();
                if (status_selected[0].equals("Close")) {
                    btn_Save.setText("Close");
                } else {
                    btn_Save.setText("Save");
                }

                if (stId[0].equals("W")) {
                    ll_Dt.setVisibility(View.VISIBLE);
                } else {
                    ll_Dt.setVisibility(View.GONE);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                // TODO Auto-generated method stub

            }
        });

        btn_Save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                getSecondaryDatabase()
                final DatabaseReference cmtRef = PersistentDatabase.getSecondaryDatabase().getReference("complaint/cmt/" +
                        bizKey + "/" + grpKey + "/" + AddComplaintActivity.this.complaint.getHisKey());
                final String cmtKey = cmtRef.push().getKey();
                final DatabaseReference complaintRef = PersistentDatabase.getSecondaryDatabase().
                        getReference("complaint/log/" + bizKey + "/" + grpKey);

                if (status_selected[0].equals("Status")) {
                    Toast.makeText(getApplicationContext(), "please select status", Toast.LENGTH_SHORT).show();
                    return;
                }

                final HashMap<String, Object> stMap = new HashMap<>();
                stMap.put("dt", System.currentTimeMillis());
                if (!et_comment.getText().toString().trim().equals("")) {
                    String comment = et_comment.getText().toString().trim();
                    stMap.put("cmt", comment);
                }
                stMap.put("st", status_selected[0]);
                HashMap<String, Object> map = new HashMap<>();
                map.put("st", stId[0]);
                Log.i("value", stId[0]);
                Log.i("value", status_selected[0]);
                if (!et_Dt.getText().toString().trim().equals("") && !et_Tm.getText().toString().trim().equals("")
                        && frDt.getTimeInMillis() != 0 && stId[0].equals("W")) {
                    map.put("pDt", frDt.getTimeInMillis());
                }

                complaintRef.child("open").child(AddComplaintActivity.this.complaint.getHisKey()).updateChildren(map);
                cmtRef.child(cmtKey).updateChildren(stMap);
                dialog.dismiss();
                /*if (status_selected[0].equals("Close")){
                    complaintRef.child("open").child(complaint.getHisKey()).child("st").setValue("C");
                    cmtRef.child(cmtKey).updateChildren(stMap);
                    dialog.dismiss();

                }else{
                    complaintRef.child("open").child(complaint.getHisKey()).child("st").setValue("W");
                    cmtRef.child(cmtKey).updateChildren(stMap);
                    dialog.dismiss();
                }*/
                finish();
            }
        });
        dialog.show();
    }

    private void dispatchTakePictureIntent(int i) {
       /* IntentFilter intentFilter = new IntentFilter("versionx.complaintentry.PHOTO_TAKEN");
        registerReceiver(photoReceiver, intentFilter);
        Intent intent = new Intent(getApplicationContext(), CameraActivity.class);
        if (i == 100) {
            photoPath = createFileFolder(new Date().getTime() + ".jpg");
            complaintImageFile = new File(photoPath);
            intent.putExtra("path", photoPath);
        } else {
            tempFile = createFileFolder(new Date().getTime() + ".jpg");
            intent.putExtra("path", tempFile);
        }
        intent.putExtra("requestCode", i);
        startActivityForResult(intent, i);*/

        Intent cameraIntent1 = new Intent(AddComplaintActivity.this, CameraXActivity.class);

        startActivityForResult(cameraIntent1, i);

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {

            if (requestCode == 100) {
                if (data.getStringExtra("path") != null) {
                    photoPath = data.getStringExtra("path");
                    final File file = new File(data.getStringExtra("path"));
                    if (file.exists()) {
                        imgList.add(new ImagePath(System.currentTimeMillis() + "", photoPath.toString()));
                        imageAdapter.notifyDataSetChanged();
                        imageIsPresent = true;
//                        Glide.with(getContext()).load(file).asBitmap().override(200,200).fitCenter().diskCacheStrategy(DiskCacheStrategy.RESULT).into(new BitmapImageViewTarget(imageView_courier_Photo) {
//                            @Override
//                            protected void setResource(Bitmap resource) {
//                                RoundedBitmapDrawable circularBitmapDrawable =
//                                        RoundedBitmapDrawableFactory.create(getContext().getResources(), resource);
//                                circularBitmapDrawable.setCircular(true);
//                                imageView_courier_Photo.setImageDrawable(circularBitmapDrawable);
//                            }
//                        });

                    } else {
                        Toast.makeText(getApplicationContext(), "Something went wrong! try again", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    private String createFileFolder(String filename) {

//        File ComplaintDirectory = new File(Environment.getExternalStorageDirectory() +
//                File.separator + "ComplaintEntry" + File.separator + "Images");
        File ComplaintDirectory = new File(getApplicationContext().getFilesDir(), filename);

        if (!ComplaintDirectory.exists()) {
            try {
                ComplaintDirectory.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        File outputFile = new File(ComplaintDirectory, filename);
        if (outputFile.exists()) outputFile.delete();
        try {
            outputFile.createNewFile();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return ComplaintDirectory.getAbsolutePath();
    }

    private void saveComplaint(Complaint complaint) {
        long timesMills = System.currentTimeMillis();
        final long monthMills = CommonMethods.getOnlyMonth(timesMills);

//        getSecondaryDatabase()
        DatabaseReference dbref = PersistentDatabase.getSecondaryDatabase().getReference("complaint/log/" +
                loginSp.getString(Global.BIZ_ID, "") + "/" + loginSp.getString(Global.GROUP_ID, ""));

        String incRef = "complaint/log/" + loginSp.getString(Global.BIZ_ID, "") + "/" + loginSp.getString(Global.GROUP_ID, "");

        HashMap<String, Object> topMap = new HashMap<>();
        HashMap<String, Object> openRefMap = new HashMap<String, Object>();
        HashMap<String, Object> pMap = new HashMap<String, Object>();
        HashMap<String, Object> ntfyMap = new HashMap<String, Object>();
        pMap.put("id", loginSp.getString(Global.STAFF_KEY, ""));
        pMap.put("nm", loginSp.getString(Global.STAFF_NAME, ""));
        pMap.put("mob", loginSp.getString(Global.STAFF_MOB, ""));


     /*   if (selectedTopics != null) {
            if (pos != -1 && topicArrayList.get(0).getTopicId().equals(selectedTopics.getTopicId())) {
                String topicId = selectedTopics.getTopicId();
                selectedTopics.setTopicId(null);
                topMap.put(topicId, selectedTopics);
                openRefMap.put("tpc", topMap);
                openRefMap.put("othr", null);
            } else {
                openRefMap.put("othr", et_Complaint_Topic.getText().toString().trim());
                openRefMap.put("tpc", null);
            }
        } else {
            openRefMap.put("othr", et_Complaint_Topic.getText().toString().trim());
        }*/

        if (selectedTopics != null) {
            String topicId = selectedTopics.getTopicId();
            String catId = (String) selectedTopics.getCat().get("id");
            openRefMap.put("catId", catId);
            selectedTopics.setTopicId(null);
            HashMap<String, Object> objectEntryCat = (HashMap<String, Object>) selectedTopics.getCat().get("ntfy");

            if (selectedTopics.getNtfy() != null && selectedTopics.getNtfy().entrySet().iterator().next().getKey() != null) {
                ntfyMap.put(selectedTopics.getNtfy().entrySet().iterator().next().getKey(), true);
            }
            if (objectEntryCat != null && objectEntryCat.entrySet().iterator().next().getKey() != null) {
                ntfyMap.put(objectEntryCat.entrySet().iterator().next().getKey(), true);
            }
            topMap.put(topicId, selectedTopics);
            //            openRefMap.put("othr", null);
        } else {
            for (Topic topic : mainTopicArrayList) {
                if (topic.getTopicId().equalsIgnoreCase("othr")) {
                    selectedTopics = topic;
                }
            }

            String topicId = selectedTopics.getTopicId();
            String catId = (String) selectedTopics.getCat().get("id");
            openRefMap.put("catId", catId);
            selectedTopics.setTopicId(null);
            selectedTopics.setNm(et_Complaint_Topic.getText().toString().trim());
            HashMap<String, Object> objectEntryCat = (HashMap<String, Object>) selectedTopics.getCat().get("ntfy");

            if (selectedTopics.getNtfy() != null && selectedTopics.getNtfy().entrySet().iterator().next().getKey() != null) {
                ntfyMap.put(selectedTopics.getNtfy().entrySet().iterator().next().getKey(), true);
            }
            if (objectEntryCat != null && objectEntryCat.entrySet().iterator().next().getKey() != null) {
                ntfyMap.put(objectEntryCat.entrySet().iterator().next().getKey(), true);
            }
            topMap.put(topicId, selectedTopics);
            //            openRefMap.put("othr", et_Complaint_Topic.getText().toString().trim());
        }
        openRefMap.put("tpc", topMap);

        String key = null;
        if (complaint != null && complaint.getKey() != null) {
            key = complaint.getKey();
            openRefMap.put("mDt", System.currentTimeMillis());
        } else {
            openRefMap.put("dt", System.currentTimeMillis());
            key = dbref.push().getKey();
        }
        openRefMap.put("i", typ);

        openRefMap.put("rmks", et_Remarks.getText().toString().trim());
        openRefMap.put("devId", loginSp.getString(Global.DEVICE_ID, ""));
        openRefMap.put("st", "O");
        openRefMap.put("typ", "CMP");
        openRefMap.put("p", pMap);
        HashMap<String, Object> labelMap = new HashMap<>();

       /* for (int i = 0; i < labelsArrayList.size(); i++) {
            if (checkBox[i].isChecked()) {
                labelMap.put(labelsArrayList.get(i).getId(), labelsArrayList.get(i).getNm());
            }
        }
*/

        for (int i = 0; i < complaintLabels.size(); i++) {
            if (complaintLabels.get(i).isSelected()) {
                labelMap.put(complaintLabels.get(i).getKey(), complaintLabels.get(i).getNm());
            }
        }

        openRefMap.put("lbl", labelMap);


        /* if (!et_Source.getText().toString().trim().equals("")) {
         *//* stfMap.put("nm", et_Source.getText().toString());
            stfMap.put("id",et_Source.getTag().toString());
            if (selectedSource!=null&&selectedSource.getRmNo().equals("")){
                stfMap.put("rmNo",selectedSource.getRmNo());

            }*//*
            if (selectedSource != null) {
                openRefMap.put("src", selectedSource);
            }
        } else {
            openRefMap.put("src", null);
        }*/

        if (selectedSource != null) {
            HashMap<String, Object> map = selectedSource.getSrcNtfy();
            if (selectedSource.getSrcNtfy() != null) {
                ntfyMap.put(map.entrySet().iterator().next().getKey(), true);
                selectedSource.setSrcNtfy(null);
            }
            HashMap<String, Object> map1 = new HashMap<>();
            if (map != null && map.entrySet().iterator().next().getKey() != null) {
                map1.put(map.entrySet().iterator().next().getKey(), true);
            }
            HashMap<String, Object> selectedSourcMap = new HashMap<>();
            selectedSourcMap.put("ntfy", map1);
            selectedSourcMap.put("id", selectedSource.getId());
            if (selectedSource.getNm() != null) {
                selectedSourcMap.put("nm", selectedSource.getNm());
            }
            selectedSourcMap.put("val", selectedSource.getVal());
            selectedSourcMap.put("aptNo", selectedSource.getAptNo());
            selectedSourcMap.put("mob", selectedSource.getMob());
            selectedSourcMap.put("no", selectedSource.getRmNo());

            openRefMap.put("src", selectedSourcMap);
//            openRefMap.put("src/ntfy",map.entrySet().iterator().next().getKey());
        } else {
            openRefMap.put("src", new HashMap<String, Object>() {{
                put("nm", et_Source.getText().toString().trim());
            }});
        }


        openRefMap.put("ntfy", ntfyMap);


        String storagePath = loginSp.getString(Global.BIZ_ID, "")
                + "/" + loginSp.getString(Global.GROUP_ID, "")
                + "/complaint/" + key + "/";

        final FirebaseImagePath firebaseImagePath = new FirebaseImagePath(getApplicationContext());
        firebaseImagePath.open();

        if (imgList.size() != 0) {
            for (int i = 0; i < imgList.size(); i++) {
                if (imgList.get(i).getVal().startsWith("http")) {
                    continue;
                }
                ImagePath imagePath = new ImagePath();
                imagePath.setStoragePath(storagePath);
                imagePath.setHisKey(key);
                imagePath.setKey(imgList.get(i).getKey());
                imagePath.setLocalPath(imgList.get(i).getVal());
                saveDataToSQLiteDatabase(imagePath, firebaseImagePath, monthMills);
            }
        }

        if (mediaRecorderPath != null && (new File(mediaRecorderPath).exists() || new File(mediaRecorderPath).getAbsoluteFile().exists())) {

            StorageReference audioRef = FirebaseStorage.getInstance().getReference(loginSp.getString(Global.BIZ_ID, "")
                    + "/" + loginSp.getString(Global.GROUP_ID, "") + "/complaint/"
                    + key + "/" + new File(mediaRecorderPath).getName());
            String sRef = loginSp.getString(Global.BIZ_ID, "")
                    + "/" + loginSp.getString(Global.GROUP_ID, "") + "/complaint/"
                    + key + "/" + new File(mediaRecorderPath).getName();

            String comRef = incRef + "/" + monthMills + "/" + key + "/audio";
            String openAudioRef = incRef + "/open/" + key + "/audio";

            List<String> dbAudioRef = new ArrayList<>();
            dbAudioRef.add(comRef);
            dbAudioRef.add(openAudioRef);

            //Added by Ankit
            Audio audioPath = new Audio();
            audioPath.setHis_key(key);
            audioPath.setFile_path(mediaRecorderPath);
            audioPath.setStorage_path(sRef);
            audioPath.setDatabase_ref(dbAudioRef);
            firebaseImagePath.insertAudio(audioPath);
            firebaseImagePath.close();
            CommonMethods.uploadAudio(getApplicationContext());
        }

        firebaseImagePath.close();
        CommonMethods.uploadImages(getApplicationContext());

      /*  HashMap<String, Object> openRefMap = new HashMap<String, Object>();
        openRefMap.put(key, hashMap);*/

        HashMap<String, Object> monthMap = new HashMap<>();
        monthMap.put("dt", System.currentTimeMillis());
        monthMap.put("st", "O");

        dbref.child("open").child(key).updateChildren(openRefMap);
        dbref.child(monthMills + "").child(key).updateChildren(monthMap);
        CommonMethods.showToast(getApplicationContext(), "Saved Successfully", Toast.LENGTH_SHORT).show();
        clearData();
    }

    private HashMap<String, Object> getCateGoryList(String trim) {
        System.out.print(trim);
        HashMap<String, Object> catMap = new HashMap<>();
        HashMap<String, HashMap<String, Object>> keyMap = new HashMap<>();
        String[] cat = trim.split("\\,");
        System.out.print(cat);
        for (int i = 0; i < cat.length; i++) {
            for (int j = 0; j < sp_List.size(); j++) {
                if (cat[i].trim().equals(sp_List.get(j).getTitle())) {
                    HashMap<String, Object> map = new HashMap<>();
                    map.put("nm", sp_List.get(j).getTitle());
                    keyMap.put(sp_List.get(j).getKey(), map);
                    break;
                }
            }
        }
        catMap.put("cat", keyMap);
        return catMap;

    }

    private void saveDataToSQLiteDatabase(ImagePath imagePath, FirebaseImagePath firebaseImagePath, long monthMills) {
        boolean didItWork = true;
        try {


            final String complaintRef1 = "complaint/log/" + bizKey + "/" + grpKey + "/open/" + imagePath.getHisKey()
                    + "/img/" + imagePath.getKey() + "/";

            String complaintRef2 = "complaint/log/" + bizKey + "/" + grpKey + "/" + monthMills + "/" + imagePath.getHisKey()
                    + "/img/" + imagePath.getKey() + "/";

            List<RefTable> databaseRef = new ArrayList<>();
            databaseRef.add(new RefTable(complaintRef1, false));
            databaseRef.add(new RefTable(complaintRef2, true));

            /**
             * ******..............SAVING FIREBASE STORAGE PATH INTO SQLite DATABASE............******
             * */

            long rowId = firebaseImagePath.createEntry(imagePath);

            System.out.println("Inserted" + rowId);

            if (rowId > 0) {

                ImagePath imagePath1 = new ImagePath();
                imagePath1.setRowId(rowId + "");
                imagePath1.setDatabasePath(databaseRef);

                /**
                 * ******..............SAVING FIREBASE DATABASE PATH INTO SQLite DATABASE............******
                 * */

                firebaseImagePath.createEntryRef(databaseRef, rowId);

            }
        } catch (Exception e) {
            didItWork = false;
            String error = e.toString();
            System.out.println("AddComplaintActivity  " + error);
        }
    }

    private void clearData() {
        pos = -1;
        et_Remarks.setText("");
        et_Source.setText("");
        imgList.clear();
        staffId = null;
        typ = 2;
        imageAdapter.notifyDataSetChanged();
        finish();

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        AudioWife.getInstance().release();
        timerHandler.removeCallbacks(null);

    }

    @Override
    public void onBackPressed() {
        finish();
        super.onBackPressed();
    }

}
