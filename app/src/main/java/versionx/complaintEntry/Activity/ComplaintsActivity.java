package versionx.complaintEntry.Activity;

import static android.os.Build.VERSION.SDK_INT;

import android.Manifest;
import android.app.AlertDialog;
import android.app.SearchManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.IntentSenderRequest;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SearchView;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.view.MenuItemCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.google.android.gms.tasks.Task;
import com.google.android.material.navigation.NavigationView;
import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.google.android.play.core.appupdate.AppUpdateInfo;
import com.google.android.play.core.appupdate.AppUpdateManager;
import com.google.android.play.core.appupdate.AppUpdateManagerFactory;
import com.google.android.play.core.appupdate.AppUpdateOptions;
import com.google.android.play.core.install.InstallStateUpdatedListener;
import com.google.android.play.core.install.model.AppUpdateType;
import com.google.android.play.core.install.model.InstallStatus;
import com.google.android.play.core.install.model.UpdateAvailability;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;

import versionx.complaintEntry.Adapter.ComplaintListAdapter;
import versionx.complaintEntry.Firebase.PersistentDatabase;
import versionx.complaintEntry.GetterSetter.Complaint;
import versionx.complaintEntry.R;
import versionx.complaintEntry.Utils.CommonMethods;
import versionx.complaintEntry.Utils.Global;
import versionx.complaintEntry.Utils.SimpleDividerItemDecoration;


public class ComplaintsActivity extends AppCompatActivity implements View.OnClickListener {
    SharedPreferences loginSP;
    //    NFCReaderWriter nfcReaderWriter;
    ArrayList<Complaint> complaintArrayList;
    RecyclerView complaintsView;
    ComplaintListAdapter complaintListAdapter;
    DatabaseReference complaintRef;
    private String bizKey, grpKey;
    private ChildEventListener complaintsListener;

    private TextView tv_businessName, tv_groupName, tv_deviceName;
    private ImageView iv_businessLogo;
    private TextView tv_ver_Dtl;
    private DrawerLayout drawerLayout;
    androidx.appcompat.widget.Toolbar toolbar;
    private NavigationView navigationView;
    private ActionBarDrawerToggle actionBarDrawerToggle;

    private ValueEventListener bizImageListner;
    private ValueEventListener deviceNameListner;
    private ValueEventListener deviceVerRefListner, compConfigListener;
    private DatabaseReference deviceDeregister, compConfigRef;
    private ValueEventListener deviceDeregisterListner;


    private ValueEventListener adminDbListener;
    //  private LinearLayout ll_app_expired;
    public static int POST_NOTIFICATIONS = 10;
    ArrayList<String> catKeyList;
    AppUpdateManager appUpdateManager;

    InstallStateUpdatedListener listener = installState -> {
        if (installState.installStatus() == InstallStatus.DOWNLOADED) {
            popUp();
        }
    };
    ActivityResultLauncher<IntentSenderRequest> activityResultLauncher = registerForActivityResult(new ActivityResultContracts.StartIntentSenderForResult(),
            result -> {
                // handle callback
                int resultCode = result.getResultCode();
                if (resultCode == RESULT_OK) {
                    Log.v("MyActivity", "Update flow completed!");
                    // If the update is cancelled or fails,
                    // you can request to start the update again.
                } else if (resultCode == RESULT_CANCELED) {
                    Log.v("MyActivity", "User cancelled Update flow!");
                } else {
                    Log.v("MyActivity", "Update flow failed with resultCode:$resultCode");
                }
            });

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_complaints);
        loginSP = getSharedPreferences(Global.LOGIN_SP, MODE_PRIVATE);
        bizKey = loginSP.getString(Global.BIZ_ID, "");
        grpKey = loginSP.getString(Global.GROUP_ID, "");
        isStoragePermissionGranted();
        initGUI();
        updateLoginTime();
        inAppUpdate();
        // checkVersion();
        catKeyList = new ArrayList<>();
//        getComplaints();
        getSrc();//Source type example:Room , Staff, Resident
        getCmpValue();
        CommonMethods.uploadAudio(ComplaintsActivity.this);
        CommonMethods.uploadImages(ComplaintsActivity.this);
    }

    private void getCmpValue() {
        DatabaseReference accessRef = PersistentDatabase.getPrimaryDatabase().getReference("access")
                .child(loginSP.getString(Global.BIZ_ID, "")).child(loginSP.getString(Global.GROUP_ID, ""))
                .child(loginSP.getString(Global.STAFF_KEY, ""));
        accessRef.keepSynced(true);

        accessRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()) {
                    loginSP.edit().putBoolean(Global.STAFF_LOGGED_IN, true).apply();
                    loginSP.edit().putString(Global.STAFF_KEY, dataSnapshot.getKey()).apply();
                    if (dataSnapshot.hasChild("f") && dataSnapshot.child("f").hasChild("nm")) {
                        loginSP.edit().putString(Global.STAFF_NAME, dataSnapshot.child("f").child("nm").child("val").getValue().toString()).apply();

                    }
                    if (dataSnapshot.hasChild("f") && dataSnapshot.child("f").hasChild("mob")) {
                        loginSP.edit().putString(Global.STAFF_MOB, dataSnapshot.child("f").child("mob").child("val").getValue().toString()).apply();
                    }
                    if (dataSnapshot.hasChild("cmp")) {
                        boolean isOthrExist = false;
                        for (DataSnapshot snapshot : dataSnapshot.child("cmp").child("cat").getChildren()) {
                            if (snapshot.getKey().equalsIgnoreCase("othr")) {
                                isOthrExist = true;
                            }
                            getComplaints(snapshot.getKey(), false);
                        }
                        if (!isOthrExist) {
                            getComplaints(dataSnapshot.getKey(), true);
                        }
                        System.out.println(catKeyList);
                    }
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });


    }

    private void inAppUpdate() {
        appUpdateManager = AppUpdateManagerFactory.create(this);
        // Returns an intent  object that you use to check an update.
        Task<AppUpdateInfo> appUpdateInfoTask = appUpdateManager.getAppUpdateInfo();

        // checks that the platform will allow the specified type of update.
        appUpdateInfoTask.addOnSuccessListener(appUpdateInfo -> {

            if (appUpdateInfo.updateAvailability() == UpdateAvailability.UPDATE_AVAILABLE && appUpdateInfo.isUpdateTypeAllowed(AppUpdateType.FLEXIBLE)) {

                try {
                    appUpdateManager.startUpdateFlowForResult(appUpdateInfo, activityResultLauncher, AppUpdateOptions.newBuilder(AppUpdateType.FLEXIBLE).build());
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        appUpdateManager.registerListener(listener);
    }

    private void popUp() {

        Snackbar snackbar = Snackbar.make(findViewById(android.R.id.content), "An update has just been downloaded.", Snackbar.LENGTH_INDEFINITE);
        snackbar.setAction("Reload", v -> {
            appUpdateManager.completeUpdate();
        });
        snackbar.setActionTextColor(
                getResources().getColor(R.color.white));
        snackbar.show();
    }


    private void checkVersion() {
        DatabaseReference adminDbRef = PersistentDatabase.getPrimaryDatabase().getReference("admin/app/complaintEntry");

        if (adminDbListener != null) {
            adminDbRef.removeEventListener(adminDbListener);
        }

        adminDbListener = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                try {
                    PackageInfo pInfo = null;
                    try {
                        pInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
                    } catch (PackageManager.NameNotFoundException e) {
//                        Crashlytics.log(e.getMessage());
                    }
                    String version = pInfo.versionName;

                    loginSP.edit().putString(Global.VERSION_NUMBER, version).apply();

                    if (!dataSnapshot.child("ver").getValue().toString().equalsIgnoreCase(version)) {

                        //ll_app_expired.setVisibility(View.VISIBLE);
                        if (System.currentTimeMillis() >= dataSnapshot.child("dt").getValue(Long.class)) {
                            complaintsView.setVisibility(View.GONE);
                        }
                    } else {
                        // ll_app_expired.setVisibility(View.GONE);
                        complaintsView.setVisibility(View.VISIBLE);
                    }
                } catch (Exception e) {
//                    Crashlytics.log(e.getMessage());
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        };

        adminDbRef.addValueEventListener(adminDbListener);
    }


    private void updateLoginTime() {

        DatabaseReference deviceRef = PersistentDatabase.getPrimaryDatabase().getReference("device")
                .child(loginSP.getString(Global.BIZ_ID, "")).child(Global.APP_NAME).child(loginSP.getString(Global.DEVICE_ID, ""));

        deviceRef.child("dt").setValue(System.currentTimeMillis());
    }

    private void getSrc() {
//        getSecondaryDatabase()
        final DatabaseReference fieldRef = PersistentDatabase.getSecondaryDatabase()
                .getReference("complaint/setting/" + bizKey + "/" + grpKey + "/src/");
        fieldRef.keepSynced(true);
        fieldRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()) {
                    System.out.print(dataSnapshot);
                    if (dataSnapshot.hasChild("access")) {
                        loginSP.edit().putBoolean(Global.IS_SRC_STAFF, dataSnapshot.child("access").child("val").getValue(Boolean.class)).apply();
                    }
                    if (dataSnapshot.hasChild("resident")) {
                        loginSP.edit().putBoolean(Global.IS_SRC_RESIDENT, dataSnapshot.child("resident").child("val").getValue(Boolean.class)).apply();
                    }
                    if (dataSnapshot.hasChild("room")) {
                        loginSP.edit().putBoolean(Global.IS_SRC_ROOM, dataSnapshot.child("room").child("val").getValue(Boolean.class)).apply();
                    }
                }

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    private void isStoragePermissionGranted() {
        if (SDK_INT >= Build.VERSION_CODES.TIRAMISU) {
            if (checkSelfPermission(Manifest.permission.CAMERA)
                    == PackageManager.PERMISSION_GRANTED && checkSelfPermission(Manifest.permission.POST_NOTIFICATIONS) == PackageManager.PERMISSION_GRANTED
            ) {


            } else {
                ActivityCompat.requestPermissions(this, new String[]{
                        "android.permission.CAMERA", "android.permission.POST_NOTIFICATIONS"}, POST_NOTIFICATIONS);

            }

        } else if (SDK_INT >= 23) {
            if (checkSelfPermission(Manifest.permission.CAMERA)
                    == PackageManager.PERMISSION_GRANTED
            ) {


            } else {
                ActivityCompat.requestPermissions(this, new String[]{
                        "android.permission.CAMERA"}, 1);

            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if (requestCode == POST_NOTIFICATIONS) {
            for (int i = 0; i < grantResults.length; i++) {
                if (grantResults[i] == PackageManager.PERMISSION_DENIED) {
                    if (permissions[i].equals("android.permission.POST_NOTIFICATIONS")) {
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU) {
                            if (shouldShowRequestPermissionRationale(Manifest.permission.POST_NOTIFICATIONS)) {
                                showRationaleDialog(getString(R.string.rationale_title),
                                        getString(R.string.rationale_desc),
                                        android.Manifest.permission.READ_CONTACTS,
                                        POST_NOTIFICATIONS);
                            }
                        }
                    } else {
                        if (permissions[i].equals("android.permission.POST_NOTIFICATIONS")) {
                            // Toast.makeText(this, "Permission granted", Toast.LENGTH_SHORT).show();
                        }
                    }
                }
            }
        }
    }

    private void initNavigationView() {

        drawerLayout = (DrawerLayout) findViewById(R.id.drawer);
        navigationView = (NavigationView) findViewById(R.id.navigation_view);
        toolbar = findViewById(R.id.toolbar);
        toolbar.setTitle("Complaints");
        toolbar.setSubtitle(loginSP.getString(Global.STAFF_NAME, ""));
        toolbar.setTitleTextColor(ContextCompat.getColor(this, R.color.white));

        setSupportActionBar(toolbar);
        actionBarDrawerToggle = new ActionBarDrawerToggle(this, drawerLayout, toolbar,
                R.string.navigation_drawer_open,
                R.string.navigation_drawer_close) {
            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
            }

            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
            }
        };
        drawerLayout.addDrawerListener(actionBarDrawerToggle);
        actionBarDrawerToggle.syncState();


        View headerView = navigationView.getHeaderView(0);
        tv_ver_Dtl = (TextView) findViewById(R.id.tv_VersionNo);
        tv_businessName = (TextView) headerView.findViewById(R.id.nav_header_businessName);
        tv_groupName = (TextView) headerView.findViewById(R.id.nav__header_groupName);
        iv_businessLogo = (ImageView) headerView.findViewById(R.id.nav_header_image);
        tv_deviceName = (TextView) headerView.findViewById(R.id.nav__header_deviceName);

        tv_businessName.setText(loginSP.getString(Global.BIZ_COMPANY_NAME, ""));
        tv_groupName.setText(loginSP.getString(Global.GROUP_NAME, ""));
        tv_deviceName.setText(loginSP.getString(Global.DEVICE_NAME, ""));

        // changing the device name on long click......
        tv_deviceName.setOnLongClickListener(v -> {
            androidx.appcompat.app.AlertDialog.Builder builder = new androidx.appcompat.app.AlertDialog.Builder(ComplaintsActivity.this);
            View dialogView = getLayoutInflater().inflate(R.layout.layout_device_name_dialog, null);
            builder.setView(dialogView);
            builder.setCancelable(true);
            Button btnSave = dialogView.findViewById(R.id.dialog_btn_save);
            Button btnCancel = dialogView.findViewById(R.id.dialog_btn_cancel);

            TextInputLayout textInputLayout = dialogView.findViewById(R.id.tl_device_name);
            TextInputEditText textInputEditText = dialogView.findViewById(R.id.et_device);

            builder.setMessage("Would you like to change the device name?");
            androidx.appcompat.app.AlertDialog alertDialog = builder.create();
            alertDialog.show();

            btnSave.setOnClickListener(v0 -> {
                String deviceName = textInputEditText.getText().toString().trim();
                if (deviceName.equals("")) {
                    textInputLayout.setError("Please enter the device name");
                } else {
                    final DatabaseReference deviceRef = PersistentDatabase.getPrimaryDatabase().
                            getReference("device/" + loginSP.getString(Global.BIZ_ID, "")
                                    + "/" + Global.APP_NAME + "/" +
                                    loginSP.getString(Global.DEVICE_ID, ""));
                    deviceRef.child("nm").setValue(deviceName);
                    alertDialog.dismiss();
                }
            });

            btnCancel.setOnClickListener(v1 -> {
                alertDialog.dismiss();
            });
            return true;
        });

        PackageManager manager = getApplication().getPackageManager();
        PackageInfo info = null;
        try {
            info = manager.getPackageInfo(
                    getApplication().getPackageName(), 0);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        String version = info.versionName;

        tv_ver_Dtl.setText(version);

        if (!loginSP.getString(Global.BIZ_COMPANY_IMG, "").isEmpty()) {
            Glide.with(this).load(loginSP.getString(Global.BIZ_COMPANY_IMG, "")).into(iv_businessLogo);
        } else {
            iv_businessLogo.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.app_icon));
        }
    }

    private void getFirebaseData() {


        DatabaseReference bizImgRef = PersistentDatabase.getPrimaryDatabase().
                getReference("biz/" + loginSP.getString(Global.BIZ_ID, "")
                );

        if (bizImageListner != null)
            bizImgRef.removeEventListener(bizImageListner);

        bizImageListner = bizImgRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                if (dataSnapshot.hasChild("img")) {
                    loginSP.edit().putString(Global.BIZ_COMPANY_IMG, dataSnapshot.child("img").getValue(String.class)).apply();

                    Glide.with(getApplicationContext()).load(loginSP.getString(Global.BIZ_COMPANY_IMG, "")).fitCenter().override(200, 200)
                            .diskCacheStrategy(DiskCacheStrategy.RESOURCE).into(iv_businessLogo);


                } else if (dataSnapshot.hasChild("nm")) {
                    loginSP.edit().putString(Global.BIZ_COMPANY_NAME, dataSnapshot.child("nm").getValue(String.class)).apply();
                    tv_businessName.setText(dataSnapshot.child("nm").getValue(String.class));
                } else if (dataSnapshot.hasChild("grp")) {
                    String groupName = dataSnapshot.child(loginSP.getString(Global.GROUP_ID, ""))
                            .child("nm").getValue(String.class);
                    loginSP.edit().putString(Global.GROUP_NAME,
                            groupName).apply();

                    tv_groupName.setText(groupName);
                }

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });


        //////////////////////////////getting device name////////////////////////////////


        String uuid = Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID);

        loginSP.edit().putString(uuid, uuid).apply();
        final DatabaseReference deviceRef = PersistentDatabase.getPrimaryDatabase().getReference
                ("device/" + loginSP.getString(Global.BIZ_ID, "")
                        + "/" + Global.APP_NAME
                        + "/" + loginSP.getString(Global.DEVICE_ID, uuid)
                );
        if (deviceNameListner != null)
            deviceRef.removeEventListener(deviceNameListner);

        deviceNameListner = deviceRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                try {
                    if (dataSnapshot.hasChild("nm") && !dataSnapshot.child("nm").getValue(String.class).trim().isEmpty()) {
                        tv_deviceName.setText(dataSnapshot.child("nm").
                                getValue().toString());
                    } else {
                        String dtTime = CommonMethods.getCurrentTime("hh:mm a");
                        deviceRef.child("nm").setValue(dtTime);
                        tv_deviceName.setText(dtTime);
                        loginSP.edit().putString(Global.DEVICE_NAME, dtTime).apply();
                    }

                    if (dataSnapshot.hasChild("img") && dataSnapshot.child("img").getValue().toString().equalsIgnoreCase("del")) {
//                        CommonMethods.deleteToolsFolder(MainActivity.this, deviceRef);
                    }

                } catch (Exception e) {
                    tv_deviceName.setText(loginSP.getString(Global.DEVICE_NAME, ""));
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });


        final DatabaseReference deviceVerRef = PersistentDatabase.getPrimaryDatabase().
                getReference("device/" + loginSP.getString(Global.BIZ_ID, "") + "/"
                        + Global.APP_NAME
                        + "/" + loginSP.getString(Global.DEVICE_ID, "") + "/l");

        if (deviceVerRefListner != null) {
            deviceVerRef.removeEventListener(deviceVerRefListner);
        }


        deviceVerRefListner = deviceVerRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {


                try {

                    if (loginSP.getBoolean(Global.IS_LOGGED_IN, false)
                            && dataSnapshot != null && dataSnapshot.getValue() != null
                            && !dataSnapshot.getValue().toString().equalsIgnoreCase("")) {

                        String version = CommonMethods.getVerisonNumber(ComplaintsActivity.this);
                        DatabaseReference verRef = PersistentDatabase.getPrimaryDatabase()
                                .getReference("device/" + loginSP.getString(Global.BIZ_ID, "")
                                        + "/" + Global.APP_NAME
                                        + "/" + loginSP.getString(Global.DEVICE_ID, ""));

                        HashMap<String, Object> hashMap = new HashMap<String, Object>();
                        if (dataSnapshot.getValue().toString().equalsIgnoreCase("ver"))
                            hashMap.put(dataSnapshot.getValue().toString(), version);
                        if (dataSnapshot.getValue().toString().equalsIgnoreCase("now"))
                            hashMap.put("dt", Calendar.getInstance().getTimeInMillis());
                        verRef.updateChildren(hashMap);

                        deviceVerRef.setValue("");

                    }

                } catch (Exception e) {
//                    Crashlytics.log(e.getMessage());
                }


            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });


        ////////////////////////////// logout from server /////////////////////////////////////////
        deviceDeregister = PersistentDatabase.getPrimaryDatabase().getReference("device/" +
                loginSP.getString(Global.BIZ_ID, "")
                + "/" + Global.APP_NAME
                + "/" + loginSP.getString(Global.DEVICE_ID, "") +
                "/status");

        deviceDeregisterListner = deviceDeregister.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                try {


                    if (loginSP.getBoolean(Global.IS_LOGGED_IN, false)
                            && dataSnapshot != null &&
                            !Boolean.parseBoolean(dataSnapshot.getValue().toString())) {

                        new CommonMethods().logout(ComplaintsActivity.this);
                        Intent in = new Intent(ComplaintsActivity.this, VerificationActivity.class);
                        in.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        finish();
                        startActivity(in);
                    }
                } catch (Exception e) {
//                    Crashlytics.log(e.getMessage());
                }


            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });


        ////keep synced node for material syncing

//        To get the details of Mifare Sector Number...
//        DatabaseReference mifareCardRef = PersistentDatabase.getPrimaryDatabase().getReference("masterData")
//                .child(loginSP.getString(Global.BIZ_ID, ""))
//                .child(loginSP.getString(Global.GROUP_ID, "")).child("nfc").child("mifare").child("id");
//
//        mifareCardRef.keepSynced(true);
//
//        mifareCardRef.addListenerForSingleValueEvent(new ValueEventListener() {
//            @Override
//            public void onDataChange(DataSnapshot dataSnapshot) {
//                if (dataSnapshot.hasChild("sec")) {
//                    loginSP.edit().putInt(Global.MIFARE_SEC_NUM,
//                            Integer.valueOf(dataSnapshot.child("sec").getValue().toString())).apply();
//                }
//            }
//
//            @Override
//            public void onCancelled(DatabaseError databaseError) {
//
//            }
//        });

//        getSecondaryDatabase()
        compConfigRef = PersistentDatabase.getSecondaryDatabase().getReference("complaint/setting")
                .child(loginSP.getString(Global.BIZ_ID, ""))
                .child(loginSP.getString(Global.GROUP_ID, "")).child("config");

        if (compConfigListener != null) {
            compConfigRef.removeEventListener(compConfigListener);
        }

        compConfigListener = compConfigRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()) {
                    if (dataSnapshot.hasChild("aud")) {
                        loginSP.edit().putBoolean(Global.COMP_AUDIO, dataSnapshot.child("aud").getValue(Boolean.class)).apply();
                    }
                    if (dataSnapshot.hasChild("img")) {
                        loginSP.edit().putBoolean(Global.COMP_IMG, dataSnapshot.child("img").getValue(Boolean.class)).apply();
                    }
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    private void initGUI() {


        initNavigationView();
        getFirebaseData();

        complaintArrayList = new ArrayList<>();
        complaintsView = (RecyclerView) findViewById(R.id.rv_Complaint_History);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getApplicationContext());
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        complaintsView.setLayoutManager(linearLayoutManager);
        complaintListAdapter = new ComplaintListAdapter(getApplicationContext(), complaintArrayList);
        complaintsView.setAdapter(complaintListAdapter);
        //    complaintListAdapter.customdatasetChanged();

        //ll_app_expired = (LinearLayout) findViewById(R.id.layout_app_expired);

//        findViewById(R.id.btn_ver_update).setOnClickListener(this);

        complaintsView.addItemDecoration(new SimpleDividerItemDecoration(this));

//        nfcReaderWriter = new NFCReaderWriter(ComplaintsActivity.this,loginSP);

    }

    // Show alert dialog to the user saying a separate permission is needed for sending the notifications....
    // Launch the settings activity for the permission.....
    private void showRationaleDialog(String title, String message, String permission, Integer requestCode) {
        android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(this);
        builder.setTitle(title)
                .setMessage(message)
                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        Intent intent = new Intent(Settings.ACTION_APP_NOTIFICATION_SETTINGS)
                                .putExtra(Settings.EXTRA_APP_PACKAGE, ComplaintsActivity.this.getPackageName());
                        startActivity(intent);

                    }
                });
        android.app.AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }

    private void getComplaints(String key, final boolean isStaffId) {
        Query query;
        complaintRef = PersistentDatabase.getSecondaryDatabase().getReference("complaint/log/" + bizKey + "/" + grpKey + "/open");
        if (!isStaffId) {
            query = complaintRef.orderByChild("catId").equalTo(key);
        } else {
            query = complaintRef.orderByChild("p/id").equalTo(key);
        }
        complaintsListener = query.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                if (dataSnapshot.exists()) {

                    Complaint complaint = Complaint.getComplaint(dataSnapshot);
                    if (isStaffId && dataSnapshot.hasChild("catId") && !
                            dataSnapshot.child("catId").getValue().toString().equalsIgnoreCase("othr")) {
                        return;
                    }
                    complaintArrayList.add(0, complaint);
                    long minDiff = new CommonMethods().getMinDiff(complaint.getDt(), System.currentTimeMillis());

                    if (complaint.getSt() != null && (complaint.getSt().equalsIgnoreCase("O") ||
                            complaint.getSt().equalsIgnoreCase("W"))) {
                        if (minDiff > 15) {
                            complaint.setActionTimeExpired(true);
                        } else {
                            complaint.setActionTimeExpired(false);
                            /*creating handler to execute after time...*/
                            long time = (15 - minDiff) * 60 * 1000;
                            createTimer(complaint, time);
                        }
                    }

                    complaintListAdapter.customdatasetChanged();
                }
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {

                if (dataSnapshot.exists()) {
                    Complaint complaint = Complaint.getComplaint(dataSnapshot);
                    complaint.setHisKey(dataSnapshot.getKey());
                    for (int i = 0; i < complaintArrayList.size(); i++) {
                        if (complaintArrayList.get(i).getHisKey().equals(complaint.getKey())) {

                            long minDiff = new CommonMethods().getMinDiff(complaint.getDt(), System.currentTimeMillis());

                            if (complaint.getSt() != null && (complaint.getSt().equalsIgnoreCase("O")
                                    || complaint.getSt().equalsIgnoreCase("W"))) {
                                if (minDiff > 15) {
                                    complaint.setActionTimeExpired(true);
                                    complaintArrayList.set(i, complaint);
                                    complaintListAdapter.customdatasetChanged();
                                } else {
                                    complaint.setActionTimeExpired(false);
                                    complaintArrayList.set(i, complaint);
                                    complaintListAdapter.customdatasetChanged();
                                    /*creating handler to execute after time...*/
                                    long time = (15 - minDiff) * 60 * 1000;
                                    createTimer(complaint, time);
                                }
                            }
                            break;
                        }
                    }
                }
            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {
                for (int i = 0; i < complaintArrayList.size(); i++) {
                    if (complaintArrayList.get(i).getHisKey().equals(dataSnapshot.getKey())) {
                        complaintArrayList.remove(i);
                        complaintListAdapter.customdatasetChanged();
                    }
                }
            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    private void createTimer(final Complaint complaint, long time) {

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                for (int i = 0; i < complaintArrayList.size(); i++) {
                    if (complaintArrayList.get(i).getKey().equalsIgnoreCase(complaint.getKey())) {
                        complaintArrayList.get(i).setActionTimeExpired(true);
                        complaintListAdapter.customdatasetChanged();
                        break;
                    }
                }
            }
        }, time);
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        try {
            IntentResult scanningResult = IntentIntegrator.parseActivityResult(requestCode, resultCode, data);
            if (scanningResult != null) {
                if (scanningResult.getContents() != null) {
                    /*String scanContent = scanningResult.getContents();
                    int type = Global.PASS_SCAN_QR_ENTRY;
                    frag.upDate(scanContent, type);*/
//                    readNFCData(scanningResult.getContents());
                } else {
                    Toast toast = Toast.makeText(getApplicationContext(),
                            "No data received!", Toast.LENGTH_SHORT);
                    toast.show();
                }
            } else {
                super.onActivityResult(requestCode, resultCode, data);
            }
        } catch (Exception e) {
//            Crashlytics.log(e.getMessage());
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
       /* if (nfcReaderWriter.isNFCAvailable()) {
            nfcReaderWriter.stopForegroundDispatch(ComplaintsActivity.this, nfcReaderWriter.getNfcAdapter());
        }*/

    }

    @Override
    protected void onResume() {
        super.onResume();
        /*if (nfcReaderWriter.isNFCAvailable())
            nfcReaderWriter.setupForegroundDispatch(this);*/
        appUpdateManager.getAppUpdateInfo().addOnSuccessListener(appUpdateInfo -> {
            if (appUpdateInfo.installStatus() == InstallStatus.DOWNLOADED) {
                popUp();
            }
        });

    }

  /*  @Override
    public void readNFCData(String tag) {

        if (tag != null && tag.startsWith("#" + Global.TYP_STAFF + "|")) {
            int type = Global.PASS_SCAN_NFC_ENTRY;
//            frag.upDate(tag, type);
        }else if(tag !=null && !tag.startsWith("#"+Global.TYP_STAFF+"|")){
            String decryptedData = CommonMethods.decryptData(tag);
             *//* if(decryptedData!=null && decryptedData.startsWith("#" + Global.TYP_STAFF + "|")){
                  int type = Global.PASS_SCAN_NFC_ENTRY;
                  frag.upDate(decryptedData, type);
              }else {
                  CommonMethods.showToast(this, "Invalid Record!", Toast.LENGTH_SHORT).show();
              }*//*
            if(decryptedData!=null){
                readNFCData(decryptedData);
            }
        } else {
            Toast.makeText(this, "Invalid Record!", Toast.LENGTH_SHORT).show();
        }
    }*/

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);

     /*   if (nfcReaderWriter.isNFCAvailable()) {
            nfcReaderWriter.readNFC(intent, ComplaintsActivity.this);
        }*/
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (actionBarDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }
        switch (item.getItemId()) {
           /* case android.R.id.home:
                finish();
                return true;*/
            case R.id.action_complaint_add:
                Intent intent = new Intent(getApplicationContext(), AddComplaintActivity.class);
                startActivity(intent);
                break;
            case R.id.menu_logout:
                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setMessage("Are you sure?")
                        .setCancelable(false)
                        .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                loginSP.edit().putBoolean(Global.STAFF_LOGGED_IN, false).apply();
                                loginSP.edit().putString(Global.STAFF_NAME, "").apply();
                                loginSP.edit().putString(Global.STAFF_KEY, "").apply();
                                loginSP.edit().putString(Global.STAFF_MOB, "").apply();
                                String uuid = Settings.Secure.getString(getContentResolver(),
                                        Settings.Secure.ANDROID_ID);

                                DatabaseReference deviceRef = PersistentDatabase.getPrimaryDatabase().getReference("device/" +
                                        loginSP.getString(Global.BIZ_ID, "") + "/complaintEntry/" + uuid);
                                deviceRef.child("opr").removeValue();
                                Intent intent = new Intent(ComplaintsActivity.this, StaffLogInActivity.class);
                                startActivity(intent);
                                finish();
                            }
                        })
                        .setNegativeButton("No", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });

                AlertDialog alert = builder.create();
                alert.show();

                return true;
            default:
                return super.onOptionsItemSelected(item);

        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.incedent_menu, menu);
        MenuItem searchItem = menu.findItem(R.id.action_complaint_search);
        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        SearchView searchView = (SearchView) MenuItemCompat.getActionView(searchItem);
        searchView.setMaxWidth(600);
        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
        searchView.setIconifiedByDefault(true);
        EditText searchEditText = (EditText) searchView.findViewById(androidx.appcompat.R.id.search_src_text);
        searchEditText.setTextColor(getResources().getColor(R.color.white));
        searchEditText.setHintTextColor(getResources().getColor(R.color.white));

        SearchView.OnQueryTextListener textChangeListener = new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                String search = query.trim();
                complaintListAdapter.getFilter().filter(search);
                return true;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                String searchText = newText.trim();
                complaintListAdapter.getFilter().filter(searchText);
                return true;
            }
        };
        searchView.setOnQueryTextListener(textChangeListener);

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (complaintsListener != null) {
            complaintRef.removeEventListener(complaintsListener);
        }
        if (compConfigListener != null) {
            compConfigRef.removeEventListener(compConfigListener);
        }
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_ver_update:

                break;
        }
    }
}
