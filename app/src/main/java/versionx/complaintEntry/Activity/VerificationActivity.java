package versionx.complaintEntry.Activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ScrollView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.messaging.FirebaseMessaging;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;

import versionx.complaintEntry.BuildConfig;
import versionx.complaintEntry.Firebase.PersistentDatabase;
import versionx.complaintEntry.GetterSetter.Groups;
import versionx.complaintEntry.R;
import versionx.complaintEntry.Utils.CommonMethods;
import versionx.complaintEntry.Utils.Global;


public class VerificationActivity extends AppCompatActivity implements View.OnClickListener, TextWatcher {
    ScrollView scrollView;
    Button btn_verify;
    private TextInputEditText ed_verificationCode, et_Password;

    private TextInputLayout tl_login_code, tl_login_password;

    SharedPreferences sp;
    private FirebaseAuth mAuth;
    ProgressDialog dialog;
    SharedPreferences loginSp;

    DatabaseReference adminDb;
    ValueEventListener adminListener;
    AlertDialog.Builder alertdialog;
    AlertDialog alert;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_verification);
        mAuth = FirebaseAuth.getInstance();
        inti();
    }

    private void inti() {

        scrollView = (ScrollView) findViewById(R.id.scrollView);
        scrollView.setFocusableInTouchMode(true);
        scrollView.setDescendantFocusability(ViewGroup.FOCUS_BEFORE_DESCENDANTS);

        btn_verify = (Button) findViewById(R.id.btn_login);

        ed_verificationCode = findViewById(R.id.et_login_code);


        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);
        ed_verificationCode.setSelection(0);
        ed_verificationCode.requestFocus();

        scrollView.fullScroll(ScrollView.FOCUS_DOWN);

        et_Password = findViewById(R.id.et_login_password);

        tl_login_code = findViewById(R.id.tl_login_code);
        tl_login_password = findViewById(R.id.tl_login_password);


        ed_verificationCode.addTextChangedListener(this);
        et_Password.addTextChangedListener(this);


        mAuth = FirebaseAuth.getInstance();
        btn_verify.setOnClickListener(this);
        loginSp = getSharedPreferences(Global.LOGIN_SP, MODE_PRIVATE);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_login:
                if (validate()) {
                    return;
                }

                final String email = ed_verificationCode.getText().toString().trim() + "@versionx.in";
                String password = et_Password.getText().toString().trim();
                et_Password.setHintTextColor(ContextCompat.getColor(VerificationActivity.this, R.color.textColor));
                ed_verificationCode.setHintTextColor(ContextCompat.getColor(VerificationActivity.this, R.color.textColor));
                if (CommonMethods.isInternetWorking(VerificationActivity.this)) {
                    dialog = new ProgressDialog(VerificationActivity.this);
                    dialog.show();
                    dialog.setMessage("Verifying...");
                    mAuth.signInWithEmailAndPassword(email, password)
                            .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                                @Override
                                public void onComplete(@NonNull Task<AuthResult> task) {

                                    // If sign in fails, display a message to the user. If sign in succeeds
                                    // the auth state listener will be notified and logic to handle the
                                    // signed in user can be handled in the listener.
                                    if (!task.isSuccessful()) {
                                        Toast.makeText(VerificationActivity.this, "Authentication failed..", Toast.LENGTH_SHORT).show();
                                        dialog.dismiss();
                                    } else {

                                        isUserRegistered(getApplicationContext());
                                    }

                                    // ...
                                }
                            });


                } else {
                    Toast.makeText(this, Global.NO_INTERNET_MSG, Toast.LENGTH_SHORT).show();
                }
                break;
        }

       /* if (ed_verificationCode.getText().toString().equals("")){
            ed_verificationCode.setError("Enter code");
            Toast.makeText(getApplicationContext(), "Code can't be blank!", Toast.LENGTH_SHORT).show();
            ed_verificationCode.setHintTextColor(Color.RED);
        }else if (et_Password.getText().toString().equals("")){
            Toast.makeText(getApplicationContext(), "Password can't be blank!", Toast.LENGTH_SHORT).show();
            et_Password.setHintTextColor(Color.RED);
        }else if(!Validations.isNumberValid(et_Password.getText().toString().trim(), 10)){
//            Toast.makeText(getApplicationContext(), "Invalid Code!", Toast.LENGTH_SHORT).show();
            et_Password.setHintTextColor(Color.RED);
        }else {
            final String email = ed_verificationCode.getText().toString().trim()+"@versionx.in";
            String password = et_Password.getText().toString().trim();
            et_Password.setHintTextColor(ContextCompat.getColor(VerificationActivity.this, R.color.textColor));
            if (CommonMethods.isInternetWorking(VerificationActivity.this)) {
                dialog = new ProgressDialog(VerificationActivity.this);
                dialog.show();
                dialog.setMessage("Verifying...");
                mAuth.signInWithEmailAndPassword(email,password).
                        addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                            @Override
                            public void onComplete(@NonNull Task<AuthResult> task) {
                                if (!task.isSuccessful()) {
                                    dialog.dismiss();
                                    ed_verificationCode.setTextColor(Color.RED);
                                    et_Password.setTextColor(Color.RED);
                                    Toast.makeText(getApplicationContext(), "Please check email id or password", Toast.LENGTH_SHORT).show();                                } else {
                                    isUserRegistered(getApplicationContext());
                                }
                            }
                        });
            }else {
                Toast.makeText(this, Global.NO_INTERNET_MSG, Toast.LENGTH_SHORT).show();
            }
        }*/
    }

    private boolean validate() {

        List<Boolean> result = new ArrayList<>();
        if (ed_verificationCode.getText().toString().trim().isEmpty()) {
            tl_login_code.setError("Enter code..");
            tl_login_code.setErrorEnabled(true);
            result.add(false);
        } else {
            result.add(true);
            tl_login_code.setError(null);
            tl_login_code.setErrorEnabled(false);
        }

        if (et_Password.getText().toString().trim().isEmpty()) {
            tl_login_password.setError("Enter password..");
            tl_login_password.setErrorEnabled(true);
            result.add(false);
        } else {
            result.add(true);
            tl_login_password.setError(null);
            tl_login_password.setErrorEnabled(false);
        }

        return result.contains(false);
    }


    private void isUserRegistered(final Context context) {
        final ArrayList<Groups> groupList = new ArrayList<>();
        sp = getSharedPreferences(Global.LOGIN_SP, MODE_PRIVATE);
        int versionCode = 0;
        try {
            versionCode = getPackageManager().getPackageInfo(getPackageName(), 0).versionCode;
            sp.edit().putString(Global.VERSION_NUMBER, "" + versionCode).apply();

        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        DatabaseReference dRef = PersistentDatabase.getPrimaryDatabase().getReference("biz/");
        dRef.keepSynced(true);
        dRef.orderByChild("code").equalTo(ed_verificationCode.getText().toString().trim()).
                addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot mainSnapshot) {
                        Log.v("Verification", mainSnapshot.toString());
                        if (mainSnapshot != null && mainSnapshot.getValue() != null) {
                            for (DataSnapshot dataSnapshot : mainSnapshot.getChildren()) {
                                if (dataSnapshot != null) {
                                    loginSp.edit().putString(Global.BIZ_ID, dataSnapshot.getKey()).apply();
                                    loginSp.edit().putString(Global.BIZ_COMPANY_NAME, dataSnapshot.child("nm").getValue(String.class)).apply();
                                    loginSp.edit().putString(Global.BIZ_TYPE, dataSnapshot.child("typ").getValue(String.class)).apply();

                                    for (DataSnapshot grpChild : dataSnapshot.child("grp").getChildren()) {

                                        Groups groups = new Groups();
                                        groups.setGroupId(grpChild.getKey());
                                        groups.setGroupName(grpChild.child("nm").getValue().toString());
                                        groupList.add(groups);
                                        dialog.dismiss();


                                    }
                                    if (groupList.size() > 1) {
                                        openDialogForGroupSelection(groupList, dataSnapshot, context);

                                    } else {
                                        SharedPreferences.Editor editor = loginSp.edit();
                                        editor.putString(Global.GROUP_ID, groupList.get(0).getGroupId().toString());
                                        editor.putString(Global.GROUP_NAME, groupList.get(0).getGroupName().toString());
                                        if (dataSnapshot.hasChild("expDt"))
                                            editor.putString(Global.BIZ_EXPIRY_DATE, dataSnapshot.child("expDt").getValue().toString());

                                        if (dataSnapshot.hasChild("img"))
                                            editor.putString(Global.BIZ_COMPANY_IMG, dataSnapshot.child("img").getValue().toString());
                                        editor.apply();
                                        String uuid = Settings.Secure.getString(getContentResolver(),
                                                Settings.Secure.ANDROID_ID);

                                        loginSp.edit().putString(Global.DEVICE_ID, uuid).apply();
                                        loginSp.edit().putInt(Global.LOGGED_IN_VERSION_CODE, BuildConfig.VERSION_CODE).apply();

                                        updateDevice(true, CommonMethods.getCurrentTime("hh:mm a"), false,
                                                CommonMethods.getVerisonNumber(context), Calendar.getInstance().getTimeInMillis(), "complaintEntry", groupList.get(0).getGroupId().toString());


                                        dialog.dismiss();
                                        Intent in = new Intent(getApplicationContext(), StaffLogInActivity.class);
                                        in.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                        in.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                        in.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                        loginSp.edit().putBoolean(Global.IS_LOGGED_IN, true).apply();
                                        finish();
                                        startActivity(in);
                                    }
                                } else {
                                    dialog.dismiss();
                                    Toast toast = Toast.makeText(VerificationActivity.this, "Invalid code!", Toast.LENGTH_SHORT);
                                    toast.setGravity(Gravity.CENTER, 0, 0);
                                    toast.show();
                                }
                            }
                        } else {
                            dialog.dismiss();
                            Toast toast = Toast.makeText(VerificationActivity.this, "Invalid code!", Toast.LENGTH_SHORT);
                            toast.setGravity(Gravity.CENTER, 0, 0);
                            toast.show();
                        }
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });

    }

    private void updateDevice(final boolean b, String currentTime, final boolean b1, final String verisonNumber,
                              final long timeInMillis, final String complaintEntry, final String s) {

        FirebaseMessaging.getInstance().getToken()
                .addOnCompleteListener(new OnCompleteListener<String>() {
                    @Override
                    public void onComplete(@NonNull Task<String> task) {
                        if (!task.isSuccessful()) {
                            Log.w("VerificationActivity", "Fetching FCM registration token failed", task.getException());
                            return;
                        }
                        // Get new FCM registration token
                        String token = task.getResult();
                        String uuid = Settings.Secure.getString(getContentResolver(),
                                Settings.Secure.ANDROID_ID);
                        try {
                            DatabaseReference deviceRef = PersistentDatabase.getPrimaryDatabase().getReference("device/" +
                                    loginSp.getString(Global.BIZ_ID, "") + "/complaintEntry/" + uuid);

                            HashMap<String, Object> map = new HashMap<String, Object>();

                            map.put("status", b);
                            map.put("del", b1);
                            map.put("token", token);
                            map.put("ver", verisonNumber);
                            map.put("dt", timeInMillis);
                            map.put("app", complaintEntry);
                            deviceRef.updateChildren(map);
                            deviceRef.child("gId").setValue(s);
                        } catch (Exception e) {
                            Toast.makeText(getApplicationContext(), Global.SERVER_ERROR_MSG, Toast.LENGTH_SHORT).show();
                        }
                    }
                });


    }

    public void openDialogForGroupSelection(final ArrayList<Groups> groupList, final DataSnapshot dataSnapshot, final Context context) {
        AlertDialog.Builder b = new AlertDialog.Builder(this);
        b.setTitle("Select Group");
        final String[] groupListName = new String[groupList.size()];
        for (int i = 0; i < groupList.size(); i++)
            groupListName[i] = groupList.get(i).getGroupName();

        b.setSingleChoiceItems(groupListName, 0, new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                try {
                    loginSp.edit().putString(Global.GROUP_ID, groupList.get(i).getGroupId().toString()).commit();
                    loginSp.edit().putString(Global.BIZ_PERSON_NAME, dataSnapshot.child("person").child("nm").getValue().toString()).commit();

                    SharedPreferences.Editor editor = loginSp.edit();

                    editor.putString(Global.GROUP_NAME, groupListName[i]).apply();
                    if (dataSnapshot.hasChild("expDt"))
                        editor.putString(Global.BIZ_EXPIRY_DATE, dataSnapshot.child("expDt").getValue().toString());
                    if (dataSnapshot.hasChild("img"))
                        editor.putString(Global.BIZ_COMPANY_IMG, dataSnapshot.child("img").getValue().toString());
                    editor.commit();

                    String uuid = Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID);
                    loginSp.edit().putString(Global.DEVICE_ID, uuid).apply();

                    loginSp.edit().putInt(Global.LOGGED_IN_VERSION_CODE, BuildConfig.VERSION_CODE).apply();


                    updateDevice(true, CommonMethods.getCurrentTime("hh:mm a"), false, CommonMethods.getVerisonNumber(context), Calendar.getInstance().getTimeInMillis(), "complaintEntry",
                            groupList.get(i).getGroupId().toString());


                } catch (Exception e) {
                    System.out.println("exception is " + e.toString());
                }

                //          getMenuItems(dialogInterface);


                Intent in = new Intent(getApplicationContext(), StaffLogInActivity.class);
                in.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                in.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                in.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                loginSp.edit().putBoolean(Global.IS_LOGGED_IN, true).apply();
                finish();
                startActivity(in);
                dialogInterface.dismiss();


            }
        });
        b.show();
    }


    @Override
    protected void onResume() {
        super.onResume();
        //checkVersion();
    }


    private void checkVersion() {
        adminDb = PersistentDatabase.getPrimaryDatabase().getReference("admin/app/" + Global.APP_NAME);
        adminListener = adminDb.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot child) {
                try {
                    PackageInfo pInfo = null;
                    try {
                        pInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
                    } catch (PackageManager.NameNotFoundException e) {
                        e.printStackTrace();
                    }
                    String version = pInfo.versionName;
                    SharedPreferences loginSp = getSharedPreferences(Global.LOGIN_SP, Context.MODE_PRIVATE);
                    loginSp.edit().putString(Global.VERSION_NUMBER, version).apply();


                    if (!child.child("ver").getValue().toString().equalsIgnoreCase(version)) {
                        //showAlert(child, version);
                    } else {
                        if (alert != null && alert.isShowing()) {

                            alert.dismiss();
                            alert.cancel();
                        }

                    }
                } catch (Exception e) {
//                    FirebaseCrash.report(e);
//                    Crashlytics.log(e.getMessage());

                }

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

    }

    private void showAlert(DataSnapshot child, String version) {
        if (alert != null && alert.isShowing()) {

            alert.dismiss();
            alert.cancel();
        }

        alertdialog = new AlertDialog.Builder(this);
        alertdialog.setTitle("warning!");
        alertdialog.setMessage(child.child("msg").getValue().toString());
        alertdialog.setCancelable(false);
        alertdialog.setNegativeButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

                //    finish();
                Intent intent = new Intent(Intent.ACTION_VIEW,
                        Uri.parse("market://details?id=" + CommonMethods.getPackageName(VerificationActivity.this)));
                startActivity(intent);

            }
        });
        alert = alertdialog.create();


        alert.show();
    }

    @Override
    protected void onPause() {
        super.onPause();

    }


    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {

    }

    @Override
    public void afterTextChanged(Editable s) {

        System.out.println(s);
        ed_verificationCode.setTextColor(Color.WHITE);
        et_Password.setTextColor(Color.WHITE);

    }


}
