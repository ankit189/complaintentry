package versionx.complaintEntry.Activity;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;

import versionx.complaintEntry.R;
import versionx.complaintEntry.Utils.Global;


public class SplashScreenActivity extends AppCompatActivity {
    private SharedPreferences sp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);
        initGUI();
        int splash_Time_Out = 3000;

        if (checkPlayServices()) {

            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {

                    if (sp.getBoolean(Global.IS_LOGGED_IN, false)) // this means user has already verified the mobile number
                    {
                        if (sp.getBoolean(Global.STAFF_LOGGED_IN, false)) {
                            Intent in = new Intent(getApplicationContext(), ComplaintsActivity.class);
                            in.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            in.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            finish();
                            startActivity(in);
                        } else {
                            Intent in = new Intent(getApplicationContext(), StaffLogInActivity.class);
                            in.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            in.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            finish();
                            startActivity(in);
                        }
                    } else {
                        Intent in = new Intent(getApplicationContext(), VerificationActivity.class);
                        in.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        in.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        finish();
                        startActivity(in);
                    }

                }
            }, splash_Time_Out);
        }
    }


    public void initGUI() {
        sp = getSharedPreferences(Global.LOGIN_SP, MODE_PRIVATE);
    }


    private boolean checkPlayServices() {
        GoogleApiAvailability gApi = GoogleApiAvailability.getInstance();
        int resultCode = gApi.isGooglePlayServicesAvailable(this);
        if (resultCode != ConnectionResult.SUCCESS) {
            if (gApi.isUserResolvableError(resultCode)) {
                Dialog errorDailog = gApi.getErrorDialog(this, resultCode, 12345);
                errorDailog.show();
                errorDailog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                    @Override
                    public void onDismiss(DialogInterface dialog) {
                        finish();
                    }
                });
            } else {
                Toast.makeText(this, "Google Play Service issue!", Toast.LENGTH_SHORT).show();
                finish();
            }
            return false;
        }
        return true;
    }
}
