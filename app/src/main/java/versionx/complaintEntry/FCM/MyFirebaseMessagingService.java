package versionx.complaintEntry.FCM;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.util.Log;
import android.widget.Toast;

import androidx.core.app.NotificationCompat;
import androidx.core.content.ContextCompat;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import java.util.Random;

import versionx.complaintEntry.Activity.AddComplaintActivity;
import versionx.complaintEntry.Activity.StaffLogInActivity;
import versionx.complaintEntry.R;
import versionx.complaintEntry.Utils.CommonMethods;
import versionx.complaintEntry.Utils.Global;


public class MyFirebaseMessagingService extends FirebaseMessagingService {

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);
        if (remoteMessage.getData().size() > 0) {
            Log.d("Message data payload: ", " " + remoteMessage.getData());
            System.out.println("FCMMessage Received" + System.currentTimeMillis());
            sendNotification(remoteMessage);

        }
        /*// Check if message contains a notification payload.
        if (remoteMessage.getNotification() != null) {
            Log.d("Message Notification", "" + remoteMessage.getNotification().getBody());
        }*/

    }


    // [START on_new_token]

    /**
     * Called if InstanceID token is updated. This may occur if the security of
     * the previous token had been compromised. Note that this is called when the InstanceID token
     * is initially generated so this is where you would retrieve the token.
     */
    @Override
    public void onNewToken(String token) {
        Log.d("New Token", "Refreshed token: " + token);

        // If you want to send messages to this application instance or
        // manage this apps subscriptions on the server side, send the
        // Instance ID token to your app server.
        sendRegistrationToServer(token);
    }
    // [END on_new_token]


    /**
     * Persist token to third-party servers.
     * <p>
     * Modify this method to associate the user's FCM InstanceID token with any server-side account
     * maintained by your application.
     *
     * @param token The new token.
     */
    private void sendRegistrationToServer(String token) {
        // TODO: Implement this method to send token to your app server.

        // Add custom implementation, as needed.
        SharedPreferences loginSP = getSharedPreferences(Global.LOGIN_SP, MODE_PRIVATE);

        loginSP.edit().putString(Global.FIREBASE_TOKEN, token).apply();

        CommonMethods.updateToken(getApplicationContext(), token);
    }

    /**
     * Create and show a simple notification containing the received FCM message.
     *
     * @param messageBody FCM message body received.
     */
    private void sendNotification(RemoteMessage messageBody) {

        SharedPreferences loginSp = getSharedPreferences(Global.LOGIN_SP, MODE_PRIVATE);
        Intent intent = new Intent(this, AddComplaintActivity.class);
        if (loginSp.getBoolean(Global.STAFF_LOGGED_IN, false)) {
            intent.putExtra("id", messageBody.getData().get("id"));
            intent.putExtra("notification", true);
        } else {
            Toast.makeText(this, "Staff not login", Toast.LENGTH_LONG).show();
            intent = new Intent(this, StaffLogInActivity.class);
        }


        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

        PendingIntent pendingIntent;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.S) {
            pendingIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_MUTABLE);
        } else {
            pendingIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
        }

        String channelId = getString(R.string.default_notification_channel_id);
        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder =
                new NotificationCompat.Builder(this, channelId)
                        .setSmallIcon(Build.VERSION.SDK_INT >= Build.VERSION_CODES.O ? R.drawable.app_icon : R.drawable.noti)
                        .setContentTitle("Complaint")
                        .setAutoCancel(true)
                        .setPriority(NotificationCompat.PRIORITY_HIGH)
                        .setSound(defaultSoundUri)
                        .setColor(ContextCompat.getColor(getApplicationContext(), R.color.app_yellow))
                        .setContentIntent(pendingIntent);


        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);


        // Since android Oreo notification channel is needed.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel channel = new NotificationChannel(channelId,
                    "Complaint",
                    NotificationManager.IMPORTANCE_HIGH);
            notificationManager.createNotificationChannel(channel);
            channel.setLockscreenVisibility(Notification.VISIBILITY_PUBLIC);
            notificationBuilder.setChannelId(channelId);
        }

        notificationManager.notify(getRandomNum() /* ID of notification */, notificationBuilder.build());


    }

    public int getRandomNum() {
        Random random = new Random();
        int m = random.nextInt(9999 - 1000) + 1000;
        return m;
    }
}
