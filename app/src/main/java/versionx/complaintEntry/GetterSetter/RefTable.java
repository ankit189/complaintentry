package versionx.complaintEntry.GetterSetter;

public class RefTable {

    private String row_id,database_ref;
    private boolean month_ref;

    public RefTable(String database_ref, boolean month_ref){
        this.database_ref = database_ref;
        this.month_ref = month_ref;
    }

    public String getRow_id() {
        return row_id;
    }

    public void setRow_id(String row_id) {
        this.row_id = row_id;
    }

    public String getDatabase_ref() {
        return database_ref;
    }

    public void setDatabase_ref(String database_ref) {
        this.database_ref = database_ref;
    }

    public boolean isMonth_ref() {
        return month_ref;
    }

    public void setMonth_ref(boolean month_ref) {
        this.month_ref = month_ref;
    }
}
