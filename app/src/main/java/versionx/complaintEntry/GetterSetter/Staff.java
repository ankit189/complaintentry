package versionx.complaintEntry.GetterSetter;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by alkaaswal on 3/16/17.
 */

public class Staff implements Serializable{

    String id,nm;

    public void setId(String id) {
        this.id = id;
    }

    public void setNm(String nm) {
        this.nm = nm;
    }

    public String getId() {
        return id;
    }

    public String getNm() {
        return nm;
    }

    @Override
    public boolean equals(Object object)
    {
        boolean sameSame = false;

        if (object != null && object instanceof Staff)
        {
            sameSame = this.id.equals(((Staff) object).getId());
        }

        return sameSame;
    }

    public String getStaffId(ArrayList<Base> staffArrayList, String staffNm){

        String locId = null;

        for(Base staff : staffArrayList){

            if(staff.getNm().equalsIgnoreCase(staffNm.trim())){
                locId = staff.getId();
                break;
            }
        }

        return locId;
    }
}
