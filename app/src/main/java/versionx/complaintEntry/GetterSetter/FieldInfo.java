package versionx.complaintEntry.GetterSetter;

import java.util.TreeMap;

/**
 * Created by developer on 24/5/17.
 */

public class FieldInfo extends Base /*implements Comparator<FieldInfo>*/{
    private boolean hide,mandatory,printable,dcml,edt,srch;
    private int order;
    private String typ,val;
    private TreeMap<String,String> options;

    //dcml Added by Ankit for get decimal point value

    public boolean isDcml() {
        return dcml;
    }

    public void setDcml(boolean dcml) {
        this.dcml = dcml;
    }

    //    Added by Ankit for drop down searchable and editable
    public boolean isEdt() {
        return edt;
    }

    public void setEdt(boolean edt) {
        this.edt = edt;
    }

    public boolean isSrch() {
        return srch;
    }

    public void setSrch(boolean srch) {
        this.srch = srch;
    }

    public boolean isMandatory() {
        return mandatory;
    }

    public void setMandatory(boolean mandatory) {
        this.mandatory = mandatory;
    }

    public boolean isPrintable() {
        return printable;
    }

    public void setPrintable(boolean printable) {
        this.printable = printable;
    }

    public int getOrder() {
        return order;
    }

    public void setOrder(int order) {
        this.order = order;
    }

    public String getTyp() {
        return typ;
    }

    public void setTyp(String typ) {
        this.typ = typ;
    }

    public String getVal() {
        return val;
    }

    public void setVal(String val) {
        this.val = val;
    }

    public TreeMap<String, String> getOptions() {
        return options;
    }

    public void setOptions(TreeMap<String, String> options) {
        this.options = options;
    }

    public FieldInfo() {
    }

    public boolean isHide() {
        return hide;
    }

    public void setHide(boolean hide) {
        this.hide = hide;
    }

   /* @Override
    public int compare(FieldInfo o1, FieldInfo o2) {
        *//*sorts in ascending order*//*
        return o1.getOrder() > o2.getOrder() ? 1 : (o1.getOrder() < o2.getOrder() ? -1 : 0);
    }*/
}
