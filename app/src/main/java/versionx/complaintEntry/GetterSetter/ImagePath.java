package versionx.complaintEntry.GetterSetter;

import java.io.Serializable;
import java.util.List;

/**
 * Created by Developer on 06/30/2017.
 */

public class ImagePath implements Serializable{

    String key;String val;
    String storagePath;
    List<RefTable> databasePath;
    String rowId;
    String year;
    String month;
    String sessionUri;
    String hisKey;

    boolean is_custom;

    public boolean isIs_custom() {
        return is_custom;
    }

    public void setIs_custom(boolean is_custom) {
        this.is_custom = is_custom;
    }

    public String getHisKey() {
        return hisKey;
    }

    public void setHisKey(String hisKey) {
        this.hisKey = hisKey;
    }

    public String getSessionUri() {
        return sessionUri;
    }

    public void setSessionUri(String sessionUri) {
        this.sessionUri = sessionUri;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getMonth() {
        return month;
    }

    public void setMonth(String month) {
        this.month = month;
    }

    public List<RefTable> getDatabasePath() {
        return databasePath;
    }

    public void setDatabasePath(List<RefTable> databasePath) {
        this.databasePath = databasePath;
    }

    public String getRowId() {
        return rowId;
    }

    public void setRowId(String rowId) {
        this.rowId = rowId;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    String type;

    public ImagePath() {

    }

    public String getLocalPath() {
        return localPath;
    }

    public void setLocalPath(String localPath) {
        this.localPath = localPath;
    }

    String localPath;

    public String getStoragePath() {
        return storagePath;
    }

    public void setStoragePath(String storagePath) {
        this.storagePath = storagePath;
    }

    public ImagePath(String key, String val, String storagePath, String locslPath, String type) {
        this.key = key;
        this.val = val;
        this.storagePath = storagePath;
        this.localPath = locslPath;
        this.type=type;

    }

    public ImagePath(String key, String val){
        this.key = key;
        this.val = val;
    }

    public String getVal() {
        return val;
    }

    public void setVal(String val) {
        this.val = val;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }
}
