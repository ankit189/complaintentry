package versionx.complaintEntry.GetterSetter;

public class ComplaintLabel {

    private String key,nm;
    private boolean selected;

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }

    public ComplaintLabel(String key,String nm){
        this.key = key;
        this.nm = nm;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getNm() {
        return nm;
    }

    public void setNm(String nm) {
        this.nm = nm;
    }
}
