package versionx.complaintEntry.GetterSetter;

import java.util.List;

/**
 * Created by ankit on 5/2/18.
 */

public class Audio {

    private int _id;

    private String his_key,storage_path,file_path;

    private List<String> database_ref;

    public Audio() {
    }

    public int get_id() {
        return _id;
    }

    public void set_id(int _id) {
        this._id = _id;
    }

    public String getHis_key() {
        return his_key;
    }

    public void setHis_key(String his_key) {
        this.his_key = his_key;
    }

    public String getStorage_path() {
        return storage_path;
    }

    public void setStorage_path(String storage_path) {
        this.storage_path = storage_path;
    }

    public String getFile_path() {
        return file_path;
    }

    public void setFile_path(String file_path) {
        this.file_path = file_path;
    }

    public List<String> getDatabase_ref() {
        return database_ref;
    }

    public void setDatabase_ref(List<String> database_ref) {
        this.database_ref = database_ref;
    }
}
