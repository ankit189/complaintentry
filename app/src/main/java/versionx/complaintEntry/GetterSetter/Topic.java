package versionx.complaintEntry.GetterSetter;

import java.io.Serializable;
import java.util.Map;

public class Topic implements Serializable {
    String topicId,nm;
    Map<String,Object> cat;
    Map<String,Object> ntfy;
    boolean pm;



    public Topic() {
    }

    public boolean isPm() {
        return pm;
    }

    public void setPm(boolean pm) {
        this.pm = pm;
    }

    public String getTopicId() {
        return topicId;
    }

    public void setTopicId(String topicId) {
        this.topicId = topicId;
    }

    public String getNm() {
        return nm;
    }

    public void setNm(String nm) {
        this.nm = nm;
    }

    public Map<String, Object> getCat() {
        return cat;
    }

    public void setCat(Map<String, Object> cat) {
        this.cat = cat;
    }

    public Map<String, Object> getNtfy() {
        return ntfy;
    }

    public void setNtfy(Map<String, Object> ntfy) {
        this.ntfy = ntfy;
    }
}
