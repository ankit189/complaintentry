package versionx.complaintEntry.GetterSetter;

import com.google.firebase.database.Exclude;

import java.io.Serializable;
import java.util.HashMap;

/**
 * Created by Developer on 01/28/2017.
 */

public class Base extends Staff implements Serializable{

    String id,nm,mob,rmNo,val,aptNo;

    HashMap<String,Object> srcNtfy;

    public HashMap<String, Object> getSrcNtfy() {
        return srcNtfy;
    }

    public void setSrcNtfy(HashMap<String, Object> srcNtfy) {
        this.srcNtfy = srcNtfy;
    }

    public Base(String id, String name){
        this.id = id;
        this.nm = name;
    }

    public Base(String key, String nm, String mob, String rmNo) {
        this.id = key;
        this.nm = nm;
        this.mob = mob;
        this.rmNo = rmNo;
    }


    public Base() {
    }

    public String getAptNo() {
        return aptNo;
    }

    public void setAptNo(String aptNo) {
        this.aptNo = aptNo;
    }

    public String getVal() {
        return val;
    }

    public void setVal(String val) {
        this.val = val;
    }

    public String getMob() {
        return mob;
    }

    public void setMob(String mob) {
        this.mob = mob;
    }

    public String getRmNo() {
        return rmNo;
    }

    public void setRmNo(String rmNo) {
        this.rmNo = rmNo;
    }

    @Override
    public String getId() {
        return id;
    }

    @Override
    public void setId(String id) {
        this.id = id;
    }

    public String getNm() {
        return nm;
    }

    public void setNm(String nm) {
        this.nm = nm;
    }

    @Override
    public String toString() {
        return nm;
    }

    @Exclude
    public HashMap<String,Object> toMap(){
        HashMap<String,Object> result  = new HashMap<>();
        result.put("nm",nm);
        result.put("id",id);
        result.put("no",rmNo==null ? aptNo : rmNo);
        result.put("val",val);
        return result;
    }
}
