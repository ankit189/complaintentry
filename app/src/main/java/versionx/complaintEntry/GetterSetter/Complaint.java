package versionx.complaintEntry.GetterSetter;




import com.google.firebase.database.DataSnapshot;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;


/**
 * Created by developer on 30/3/17.
 */

public class Complaint implements Serializable {

    String nm,mob,id,hisKey,key,audio,rmks,othr,st;
    long dt,mDt;
    boolean isAudioExists;

    ArrayList<String> ntfy;


    public ArrayList<String> getNtfy() {
        return ntfy;
    }

    public void setNtfy(ArrayList<String> ntfy) {
        this.ntfy = ntfy;
    }

    public boolean isActionTimeExpired() {
        return actionTimeExpired;
    }

    public void setActionTimeExpired(boolean actionTimeExpired) {
        this.actionTimeExpired = actionTimeExpired;
    }

    boolean actionTimeExpired;
    ArrayList<String> imgList;
    private ArrayList<ImagePath> img;
    private HashMap<String,Object> customFields;
    private HashMap<String,Object> ack;

    ArrayList<Base> cat;
    ArrayList<Topic> tpc;
    HashMap<String,Object> lbl;
    ArrayList<Base> src;


    public HashMap<String, Object> getAck() {
        return ack;
    }

    public void setAck(HashMap<String, Object> ack) {
        this.ack = ack;
    }

    public String getSt() {
        return st;
    }

    public void setSt(String st) {
        this.st = st;
    }

    public String getOthr() {
        return othr;
    }

    public void setOthr(String othr) {
        this.othr = othr;
    }

    public ArrayList<Base> getSrc() {
        return src;
    }

    public void setSrc(ArrayList<Base> src) {
        this.src = src;
    }

    public HashMap<String, Object> getLbl() {
        return lbl;
    }

    public void setLbl(HashMap<String, Object> lbl) {
        this.lbl = lbl;
    }

    public String getRmks() {
        return rmks;
    }

    public void setRmks(String rmks) {
        this.rmks = rmks;
    }

    public ArrayList<Topic> getTpc() {
        return tpc;
    }

    public void setTpc(ArrayList<Topic> tpc) {
        this.tpc = tpc;
    }

    public ArrayList<Base> getCat() {
        return cat;
    }

    public void setCat(ArrayList<Base> cat) {
        this.cat = cat;
    }


    public boolean isAudioExists() {
        return isAudioExists;
    }

    public void setAudioExists(boolean audioExists) {
        isAudioExists = audioExists;
    }


    public HashMap<String, Object> getCustomFields() {
        return customFields;
    }

    public void setCustomFields(HashMap<String, Object> customFields) {
        this.customFields = customFields;
    }

    public String getAudio() {
        return audio;
    }

    public void setAudio(String audio) {
        this.audio = audio;
    }

    public ArrayList<ImagePath> getImg() {
        return img;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public void setImg(ArrayList<ImagePath> img) {
        this.img = img;
    }

    public String getNm() {
        return nm;
    }

    public void setNm(String nm) {
        this.nm = nm;
    }


    public String getMob() {
        return mob;
    }

    public void setMob(String mob) {
        this.mob = mob;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getHisKey() {
        return hisKey;
    }

    public void setHisKey(String hisKey) {
        this.hisKey = hisKey;
    }

    public long getDt() {
        return dt;
    }

    public void setDt(long dt) {
        this.dt = dt;
    }


    public ArrayList<String> getImgList() {
        return imgList;
    }

    public void setImgList(ArrayList<String> imgList) {
        this.imgList = imgList;
    }


    public long getmDt() {
        return mDt;
    }

    public void setmDt(long mDt) {
        this.mDt = mDt;
    }

    public static Complaint getComplaint(DataSnapshot dataSnapshot){
        Complaint complaint = new Complaint();
        if (dataSnapshot.hasChild("dt")) {
            complaint.setDt(dataSnapshot.child("dt").getValue(Long.class));
        }

        if (dataSnapshot.hasChild("ack")) {
            HashMap<String,Object> map=new HashMap<>();
            map.put("id",dataSnapshot.child("ack").child("id").getValue());
            map.put("nm",dataSnapshot.child("ack").child("nm").getValue());
            complaint.setAck(map);
        }

        if (dataSnapshot.hasChild("st")) {
            complaint.setSt(dataSnapshot.child("st").getValue(String.class));
        }

        if(dataSnapshot.hasChild("mDt")){
            complaint.setmDt(dataSnapshot.child("mDt").getValue(Long.class));
        }
        if(dataSnapshot.hasChild("ntfy")){
            ArrayList<String> ntfyVal=new ArrayList<>();
            for (DataSnapshot snapshot:dataSnapshot.child("ntfy").getChildren()){
                ntfyVal.add(snapshot.getKey());
            }
            complaint.setNtfy(ntfyVal);
        }

        ArrayList<Base> srcList=new ArrayList<>();
        if (dataSnapshot.hasChild("src")){
            Base base=new Base();
            base.setNm(dataSnapshot.child("src").child("nm").getValue().toString());

            if (dataSnapshot.hasChild("src")&&dataSnapshot.child("src").hasChild("ntfy")) {
                base.setSrcNtfy(((HashMap<String, Object>) dataSnapshot.child("src").child("ntfy").getValue()));
            }

            if(dataSnapshot.hasChild("src")&&dataSnapshot.child("src").hasChild("id")) {
                base.setId(dataSnapshot.child("src").child("id").getValue().toString());
            }

            if (dataSnapshot.hasChild("src")&&dataSnapshot.child("src").hasChild("mob")){
                base.setMob(dataSnapshot.child("src").child("mob").getValue().toString());
            }
            if (dataSnapshot.hasChild("src")&&dataSnapshot.child("src").hasChild("no")){
                base.setRmNo(dataSnapshot.child("src").child("no").getValue().toString());
            }
            if (dataSnapshot.hasChild("src")&&dataSnapshot.child("src").hasChild("val")){
                base.setVal(dataSnapshot.child("src").child("val").getValue().toString());
            }
            srcList.add(base);
            complaint.setSrc(srcList);
        }
        if (dataSnapshot.hasChild("audio")) {
            complaint.setAudio(dataSnapshot.child("audio").getValue().toString());
        }else {
            complaint.setAudioExists(false);
        }
        ArrayList<Topic> topicArrayList = new ArrayList<>();
        if (dataSnapshot.hasChild("tpc")){
            for (DataSnapshot snapshot:dataSnapshot.child("tpc").getChildren()) {
                Topic topic = snapshot.getValue(Topic.class);
                topic.setTopicId(snapshot.getKey());
                topicArrayList.add(topic);
                complaint.setTpc(topicArrayList);
            }
        }else if (dataSnapshot.hasChild("othr")){
            complaint.setOthr(dataSnapshot.child("othr").getValue(String.class));
        }
        if (dataSnapshot.hasChild("rmks")) {
            complaint.setRmks(dataSnapshot.child("rmks").getValue().toString());
        }


        ArrayList<ImagePath> imgList = new ArrayList<ImagePath>();
        HashMap<String,Object> customFieldMap = new HashMap<String, Object>();
        if (dataSnapshot.hasChild("img")) {
            for (DataSnapshot imgSnap : dataSnapshot.child("img").getChildren()) {
                imgList.add(new ImagePath(imgSnap.getKey(), imgSnap.getValue().toString()));
            }
        }
        complaint.setImg(imgList);
        complaint.setKey(dataSnapshot.getKey());
        complaint.setHisKey(dataSnapshot.getKey());

        if (dataSnapshot.hasChild("lbl")){
            HashMap<String,Object> lblMap= (HashMap<String, Object>) dataSnapshot.child("lbl").getValue();
            complaint.setLbl(lblMap);
        }




      /*  for(DataSnapshot child : dataSnapshot.getChildren()){
            if(child.getKey().length()==20 || child.getKey().equalsIgnoreCase("p") ||
                    child.getKey().equalsIgnoreCase("st") ||
                    child.getKey().equalsIgnoreCase("rmks")
                    || child.getKey().equalsIgnoreCase("to")){
                customFieldMap.put(child.getKey(),child.getValue());
            }
        }
        complaint.setCustomFields(customFieldMap);*/
        return complaint;
    }
}
