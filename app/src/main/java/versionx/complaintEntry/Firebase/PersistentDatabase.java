package versionx.complaintEntry.Firebase;

import com.google.firebase.database.FirebaseDatabase;

import versionx.complaintEntry.BaseUrls;


public class PersistentDatabase {

    private static FirebaseDatabase mPrimaryDatabase,mSecondaryDatabase;

    public static FirebaseDatabase getSecondaryDatabase() {
        if (mSecondaryDatabase == null) {
            mSecondaryDatabase = FirebaseDatabase.getInstance(BaseUrls.SECONDARY_DATABASE_URL);
            mSecondaryDatabase.setPersistenceEnabled(true);
        }
        return mSecondaryDatabase;
    }

    public static FirebaseDatabase getPrimaryDatabase() {
        if (mPrimaryDatabase == null) {
            mPrimaryDatabase = FirebaseDatabase.getInstance(BaseUrls.PRIMARY_DATABASE_URL);
            mPrimaryDatabase.setPersistenceEnabled(true);
        }
        return mPrimaryDatabase;
    }

}
