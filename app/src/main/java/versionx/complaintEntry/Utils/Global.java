package versionx.complaintEntry.Utils;

public class Global {
    public static final String IS_SRC_STAFF = "is_src_staff";
    public static final String IS_SRC_RESIDENT = "is_src_resident";
    public static final String IS_SRC_ROOM = "is_src_room";
    public static final String MIFARE_SEC_NUM = "mifare_sec_num";

    public static final String STAFF_LOGGED_IN = "stf_log_in";
    public static final String STAFF_KEY = "stf_key";
    public static final String STAFF_NAME = "stf_nm";
    public static final String STAFF_MOB = "stf_mob_no";
    public static final String COMPLAINT_GROUP_TYPE = "cmp_typ";
    public static final String FIREBASE_TOKEN = "firebase_token";

    public static String IS_LOGGED_IN="IsLoggedIn";
    public static final String LOGIN_SP="LoginSp";


    public static String SERVER_ERROR_MSG = "Server error! please try after some time";
    public static String NO_INTERNET_MSG = "No Internet Connection! Try again";
    public static String CHECK_INTERNET_MSG = "Check your internet connection and try again!";

    public static final String BIZ_TYPE = "biz_type";
    public static final String LOGGED_IN_VERSION_CODE = "logged_in_version_code";



    public static final String VERSION_NUMBER="Version_No";
    public static final String BIZ_ID="BizId";
    public static final String GROUP_ID="GroupId";
    public static final String GROUP_NAME="GroupNm";
    public static final String BIZ_EXPIRY_DATE="ExpDt";
    public static final String BIZ_COMPANY_IMG="CompanyImg";
    public static final String DEVICE_ID="Device_Id";
    public static final String BIZ_PERSON_NAME="biz_per_nm";
    public static final String LOCATION_ID="locId";
    public static final String ITEM_WISE="itemWise";
    public static final String LOCATION_NM="locNm";
    public static final String APP_NAME="complaintEntry";
    public static final String MAT="mat";
    public static final String KM="km";
    public static final String VER="ver";
    public static final String MENU_COUNT="menuCount";
    public static final String BIZ_COMPANY_NAME="bizName";
    public static final String DEVICE_NAME="deviceName";
    public static final String REFRESH ="refresh";


    public static final String TO_MEET_TYPE="to_meet_type";
    public static final String TYP_STAFF ="ST";
    public static final String BUSINESS_TYPE = "businessType";


    public static final int PASS_SCAN_NFC_ENTRY=3;
    public static final int PASS_SCAN_QR_ENTRY=1;


    public static String INCIDENT_MODULE="cmp";
    public static final String COMP_IMG = "comp_img";
    public static final String COMP_AUDIO = "comp_audio";
    public static final String UUID = "uuid";
}
