package versionx.complaintEntry.Utils;

import com.google.android.material.textfield.TextInputLayout;
import android.text.TextUtils;

import java.util.regex.Pattern;


public class Validations {


    public static boolean isNumberValid(String mobile , int size)
    {
        boolean b = Pattern.matches("^[0-9]{"+size+"}$", mobile);
        return b;
    }

    public static boolean CheckEmail(final TextInputLayout textInputLayout, String email){

        if(TextUtils.isEmpty(email) && !isValidEmail(email)){
            textInputLayout.setError("Enter a valid e-mail address !");
            textInputLayout.setErrorEnabled(true);
            return false;
        }else{
            textInputLayout.setErrorEnabled(false);
        }
        return  true;
    }

    private static boolean isValidEmail(String email) {
        return !TextUtils.isEmpty(email) && android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }

    public static boolean isValidPassword(final TextInputLayout textInputLayout, String password){

       if(!TextUtils.isEmpty(password) && password.length() > 4)
       {
           textInputLayout.setErrorEnabled(false);
       }
        else
       {
           textInputLayout.setError("Enter a valid password!");
           textInputLayout.setErrorEnabled(true);
           return false;
       }

        return true;
    }

}
