package versionx.complaintEntry.Utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Environment;
import androidx.annotation.NonNull;

import android.provider.Settings;
import android.view.Gravity;
import android.widget.Toast;

import com.google.android.gms.tasks.Continuation;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

import versionx.complaintEntry.Firebase.PersistentDatabase;
import versionx.complaintEntry.GetterSetter.Audio;
import versionx.complaintEntry.GetterSetter.ImagePath;
import versionx.complaintEntry.GetterSetter.RefTable;
import versionx.complaintEntry.OfflineDataBase.FirebaseImagePath;

import static android.content.Context.MODE_PRIVATE;

public class CommonMethods {

    public CommonMethods() {

    }
    public static void updateToken(Context context, String token) {

        String uuid = Settings.Secure.getString(context.getContentResolver(), Settings.Secure.ANDROID_ID);

        final SharedPreferences loginSp = context.getSharedPreferences(Global.LOGIN_SP, MODE_PRIVATE);

        final SharedPreferences devSP = context.getSharedPreferences(Global.LOGIN_SP, MODE_PRIVATE);

        DatabaseReference deviceRef = PersistentDatabase.getPrimaryDatabase().getReference("device")
                .child(loginSp.getString(Global.BIZ_ID, ""))
                .child("complaintEntry").child(loginSp.getString(Global.DEVICE_ID,"")).child("token");
        deviceRef.setValue(token);


    }
    public void logout(Context context) {

        SharedPreferences loginSp = context.getSharedPreferences(Global.LOGIN_SP, Context.MODE_PRIVATE);
        String uuid = android.provider.Settings.Secure.getString(context.getContentResolver(), android.provider.Settings.Secure.ANDROID_ID);
        DatabaseReference codeRef = PersistentDatabase.getPrimaryDatabase().getReference("device/" +
                loginSp.getString(Global.BIZ_ID, "") +
                "/" + Global.APP_NAME + "/" + uuid);

        codeRef.getRef().child("del").setValue(true);
        codeRef.getRef().child("dt").setValue(Calendar.getInstance().getTimeInMillis());
        SharedPreferences sp = context.getSharedPreferences(Global.LOGIN_SP, Context.MODE_PRIVATE);
        SharedPreferences.Editor editorlogin = sp.edit();
        editorlogin.clear();
        editorlogin.commit();

        //FirebaseAuth.getInstance().signOut();
        File listFile = new File(
                android.os.Environment.getExternalStorageDirectory() + File.separator +
                        "ComplaintEntry" + File.separator + "Images");
        if (listFile.exists())
            DeleteRecursive(listFile);
        deleteCache(context);
    }

    public static String formatTime(long timestamp) {
        if (timestamp == 0) {
            return "";
        } else {
            return new SimpleDateFormat("h:mm aa", Locale.getDefault()).format(new Date(timestamp));
        }
    }

    public static void DeleteRecursive(File fileOrDirectory) {
        if (fileOrDirectory.isDirectory()) {
            for (File child : fileOrDirectory.listFiles()) {
                DeleteRecursive(child);
            }
        }

        fileOrDirectory.delete();
    }

    public void deleteCache(Context context) {
        try {
            File dir = context.getCacheDir();
            deleteDir(dir);
        } catch (Exception e) {
        }
    }

    public boolean deleteDir(File dir) {
        if (dir != null && dir.isDirectory()) {
            String[] children = dir.list();
            for (int i = 0; i < children.length; i++) {
                boolean success = deleteDir(new File(dir, children[i]));
                if (!success) {
                    return false;
                }
            }
            return dir.delete();
        } else if (dir != null && dir.isFile()) {
            return dir.delete();
        } else {
            return false;
        }
    }

    public static boolean isInternetWorking(Context context) {

        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        if (activeNetwork != null) { // connected to the internet
            if (activeNetwork.getType() == ConnectivityManager.TYPE_WIFI) {
                // connected to wifi
                return true;
            } else if (activeNetwork.getType() == ConnectivityManager.TYPE_MOBILE) {
                // connected to the mobile provider's data plan
                return true;
            }
        } else {
            // not connected to the internet
            return false;
        }

        return false;
    }

    public static String getCurrentTime(String format) {
        Calendar c = Calendar.getInstance();
        System.out.println("Current time => " + c.getTime());
        SimpleDateFormat df = new SimpleDateFormat(format);
        String formattedDate = df.format(c.getTime());

        return formattedDate;
    }

    public static String getVerisonNumber(Context context) {
        PackageInfo pInfo = null;
        try {
            pInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        return pInfo.versionName;
    }


    public static String getPackageName(Context context) {
        return context.getPackageName();
    }


    public static String getDate(long milliSeconds, String dateFormat) {

        SimpleDateFormat formatter = new SimpleDateFormat(dateFormat, Locale.getDefault());

        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(milliSeconds);
        return formatter.format(calendar.getTime());
    }


    public static String decryptData(String scanContent) {
        try {
            CryptLib cryptLib = new CryptLib();
            return cryptLib.decryptSimple(scanContent, "versionx", "123");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }


    public static String createMediaFile(Context context) {

//        File directory = new File(Environment.getExternalStorageDirectory()
//                + File.separator + "Recorded");

        File ComplaintDirectory = new File(context.getFilesDir(), System.currentTimeMillis() + ".3gpp");

        if (!ComplaintDirectory.exists()) {
            try {
                ComplaintDirectory.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

//        File mediaFile = new File(directory + File.separator + System.currentTimeMillis() + ".mp3");
//        File mediaFile = new File(ComplaintDirectory + File.separator + System.currentTimeMillis() + ".3gpp");


        return ComplaintDirectory.getAbsolutePath();
    }


    public static void deleteImages(String path) {
        File file = new File(path);
        if (file.exists() || file.getAbsoluteFile().exists()) {
            file.delete();
        }
    }

    public static long getOnlyDate(long timestamp) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(timestamp);


        calendar.set(Calendar.MILLISECOND, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.HOUR_OF_DAY, 0);

        return calendar.getTimeInMillis();
    }

    public static long getOnlyMonth(long timestamp) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(timestamp);

        calendar.set(Calendar.MILLISECOND, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.DATE, 1);

        return calendar.getTimeInMillis();
    }

    public static long getMeYesterday() {
        return new Long(getOnlyDate(System.currentTimeMillis()) - 24 * 60 * 60 * 1000);
    }


    public static void uploadAudio(final Context context) {

        final SharedPreferences loginSp = context.getSharedPreferences(Global.LOGIN_SP, Context.MODE_PRIVATE);

        final FirebaseImagePath firebaseImagePath = new FirebaseImagePath(context);
        if (!isInternetWorking(context)) {
            return;
        }
        try {

            firebaseImagePath.open();

            final Audio audio = firebaseImagePath.getLastAudio();

            firebaseImagePath.close();

            if (audio.get_id() != 0) {

                if (audio.getFile_path() == null) {

                    firebaseImagePath.open();

                    firebaseImagePath.deleteAudio(audio.get_id());

                    firebaseImagePath.close();

                    uploadAudio(context);

                    return;
                }

                if (new File(audio.getFile_path()).exists() ||
                        new File(audio.getFile_path()).getAbsoluteFile().exists()) {

//                    final DatabaseReference databaseReference = PersistentDatabase.getSecondaryDatabase().getReference();
                    final DatabaseReference databaseReference = PersistentDatabase.getSecondaryDatabase().getReference();

                    final StorageReference storageRef = FirebaseStorage.getInstance().getReference(audio.getStorage_path());
                    Uri file = Uri.fromFile(new File(audio.getFile_path()));
                    final UploadTask taskFinal = storageRef.putFile(file);

                    Task<Uri> uriTask = taskFinal.continueWithTask(new Continuation<UploadTask.TaskSnapshot, Task<Uri>>() {
                        @Override
                        public Task<Uri> then(@NonNull Task<UploadTask.TaskSnapshot> task) throws Exception {
                            if (!task.isSuccessful()) {
                                throw task.getException();
                            }
                            return storageRef.getDownloadUrl();
                        }
                    }).addOnCompleteListener(new OnCompleteListener<Uri>() {
                        @Override
                        public void onComplete(@NonNull Task<Uri> task) {
                            if (task.isSuccessful()) {
                                String url = task.getResult().toString();
                                firebaseImagePath.open();
                                List<String> databasePathList = firebaseImagePath.getAudioRefById(audio.get_id());
                                firebaseImagePath.close();

                                HashMap<String, Object> databaseMap = new HashMap<>();
                                for (String databasePath : databasePathList) {
                                    databaseMap.put(databasePath, url);
                                }

                                databaseReference.updateChildren(databaseMap)
                                        .addOnSuccessListener(new OnSuccessListener<Void>() {
                                            @Override
                                            public void onSuccess(Void aVoid) {

                                                firebaseImagePath.open();

                                                firebaseImagePath.deleteAudio(audio.get_id());


                                                if (!firebaseImagePath.checkPathExists(audio.getFile_path())) {
                                                    deleteAudioPath(audio.getFile_path());
                                                }

                                                firebaseImagePath.close();

                                                uploadAudio(context);

                                            }
                                        }).addOnFailureListener(new OnFailureListener() {
                                    @Override
                                    public void onFailure(@NonNull Exception e) {

                                       /* Crashlytics.log("UrlUpdateOnFailure\tBusiness" + loginSp.getString(Global.BIZ_ID, "") + "\t" +
                                                loginSp.getString(Global.GROUP_ID, ""));

                                        Crashlytics.logException(e);*/
                                    }
                                });
                            }
                        }
                    }).addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                          /*  Crashlytics.log("UploadAudioOnFailure\tBusiness" + loginSp.getString(Global.BIZ_ID, "") + "\t" +
                                    loginSp.getString(Global.GROUP_ID, ""));

                            Crashlytics.logException(e);*/
                        }
                    });
                } else {

                    firebaseImagePath.open();

                    firebaseImagePath.deleteAudio(audio.get_id());

                    firebaseImagePath.close();

                    uploadAudio(context);
                }
            }
        } catch (Exception e) {
           /* Crashlytics.log("UploadAudioBusiness" + loginSp.getString(Global.BIZ_ID, "") + "\t" +
                    loginSp.getString(Global.GROUP_ID, ""));
            Crashlytics.logException(e);*/
        }

    }

    public static void deleteAudioPath(String path) {
        File file = new File(path);
        if (file.exists() || file.getAbsoluteFile().exists()) {
            file.delete();
        }
    }


    public static void uploadImages(final Context context) {

        final SharedPreferences loginSp = context.getSharedPreferences(Global.LOGIN_SP, Context.MODE_PRIVATE);

        final FirebaseImagePath info = new FirebaseImagePath(context);

        if (!CommonMethods.isInternetWorking(context)) {
            return;
        }

        try {

            info.open();

            final ArrayList<ImagePath> data = info.getData();

            info.close();

            if (data.size() != 0) {

                if (data.get(0).getLocalPath() == null) {

                    HashMap<String, Object> noUploadMap = new HashMap<>();
                    noUploadMap.put("error", "image path not exists");

                    //saveNoUploadImages(data.get(0), loginSp, noUploadMap);

                    info.open();

                    info.deleteStoragePath(Long.parseLong(data.get(0).getRowId()));
                    info.deleteDatabasePath(Long.parseLong(data.get(0).getRowId()));

                    info.close();

                    uploadImages(context);

                    return;
                }
                if (new File(data.get(0).getLocalPath()).exists() ||
                        new File(data.get(0).getLocalPath()).getAbsoluteFile().exists()) {
                    StorageReference storageRef = FirebaseStorage.getInstance().getReference();
                    final StorageReference imageStorageRef = storageRef.child(data.get(0).getStoragePath() + "/" + data.get(0).getKey() + ".jpg");
                    Uri file = Uri.fromFile(new File(data.get(0).getLocalPath()));
                    UploadTask taskFinal = imageStorageRef.putFile(file);
                    taskFinal.continueWithTask(new Continuation<UploadTask.TaskSnapshot, Task<Uri>>() {
                        @Override
                        public Task<Uri> then(@NonNull Task<UploadTask.TaskSnapshot> task) throws Exception {
                            if (!task.isSuccessful()) {
                                throw task.getException();
                            }
                            // Continue with the task to get the download URL
                            return imageStorageRef.getDownloadUrl();
                        }
                    }).addOnCompleteListener(new OnCompleteListener<Uri>() {
                        @Override
                        public void onComplete(@NonNull Task<Uri> task) {
                            if (task.isSuccessful()) {
                                Uri downloadUri = task.getResult();
                                String url = downloadUri.toString();
                                info.open();
                                final List<RefTable> databasePathList = info.getDataRef(data.get(0).getRowId());
                                info.close();
                                updateRef(databasePathList, url, info, context, data, loginSp);
                            } else {
                               /* Crashlytics.log("UploadImageOnFailure\tBusiness" + loginSp.getString(Global.BIZ_ID, "") + "\t" +
                                        loginSp.getString(Global.GROUP_ID, ""));*/
                                if (task.getException() != null && task.getException().getMessage() != null) {
//                                    Crashlytics.logException(task.getException());
                                    HashMap<String, Object> noUploadMap = new HashMap<>();
                                    noUploadMap.put("error", "onFailureListener" + task.getException().getMessage());
                                    //saveNoUploadImages(data.get(0), loginSp, noUploadMap);
                                }
                            }
                        }
                    });
                } else {

                    HashMap<String, Object> noUploadMap = new HashMap<>();
                    noUploadMap.put("error", "file doesn't exists");

                    //saveNoUploadImages(data.get(0), loginSp, noUploadMap);

                    info.open();

                    info.deleteStoragePath(Long.parseLong(data.get(0).getRowId()));
                    info.deleteDatabasePath(Long.parseLong(data.get(0).getRowId()));

                    info.close();

                    uploadImages(context);

                }
            }
        } catch (Exception e) {
           /* Crashlytics.log("UploadImageBusiness" + loginSp.getString(Global.BIZ_ID, "") + "\t" +
                    loginSp.getString(Global.GROUP_ID, ""));
            Crashlytics.logException(e);*/
        }
    }

    private static void updateRef(final List<RefTable> databasePath, final String url, final FirebaseImagePath info, final Context context,
                                  final ArrayList<ImagePath> data, final SharedPreferences loginSp) {

        String monthRef = "", historyRef = "";

        for (RefTable refTable : databasePath) {
            if (refTable.isMonth_ref()) {
                monthRef = refTable.getDatabase_ref();
            } else {
                historyRef = refTable.getDatabase_ref();
            }
        }

        final HashMap<String, Object> urlMap = new HashMap<>();
        if (!monthRef.isEmpty()) urlMap.put(monthRef, url);
        if (!historyRef.isEmpty()) urlMap.put(historyRef, url);

        String historyPath = historyRef.split("/img")[0];
/*
        for custom image path
*/
        if (historyPath.contains("val")) {
            historyPath = historyPath.split("/val")[0];
        }

//        getSecondaryDatabase()
        DatabaseReference databaseReference = PersistentDatabase.getSecondaryDatabase().getReference(historyPath);

        databaseReference.keepSynced(true);

        final String finalHistoryRef = historyRef;
        final String finalMonthRef = monthRef;

        databaseReference.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()) {
//                    getSecondaryDatabase()
                    PersistentDatabase.getSecondaryDatabase().getReference().updateChildren(urlMap, new DatabaseReference.CompletionListener() {
                        @Override
                        public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {
                            if (databaseError == null) {
                                info.open();
                                info.deleteStoragePath(Long.parseLong(data.get(0).getRowId()));
                                info.deleteDatabasePath(Long.parseLong(data.get(0).getRowId()));
                                //added by Rajesh to delete images after uploading....
                                if (!info.checkPathExists(data.get(0).getLocalPath())) {
                                    CommonMethods.deleteImages(data.get(0).getLocalPath());
                                }
                                info.close();
                                uploadImages(context);
                            } else {
                               /* Crashlytics.log("UrlUpdateOnFailure\tBusiness" + loginSp.getString(Global.BIZ_ID, "") + "\t" +
                                        loginSp.getString(Global.GROUP_ID, ""));
                                Crashlytics.logException(databaseError.toException());*/
                                HashMap<String, Object> noUploadMap = new HashMap<>();
                                noUploadMap.put("error", "UrlUpdateonFailureListener" + databaseError.toString());
                                noUploadMap.put("url", url);
                                //saveNoUploadImages(data.get(0), loginSp, noUploadMap);
                            }
                        }
                    });

                } else {
                    HashMap<String, Object> noRefMap = new HashMap<>();
                    noRefMap.put("url", url);
                    noRefMap.put("dt", System.currentTimeMillis());
                    noRefMap.put("ref", finalHistoryRef);

//                    getSecondaryDatabase()
                    DatabaseReference noUploadRef = PersistentDatabase.getSecondaryDatabase().getReference()
                            .child("temp").child("noRefExists").child(loginSp.getString(Global.BIZ_ID, ""))
                            .child(loginSp.getString(Global.GROUP_ID, ""));

                    noUploadRef.push().updateChildren(noRefMap);

                    if (finalMonthRef.isEmpty()) {
                        return;
                    }


//                    getSecondaryDatabase()
                    PersistentDatabase.getSecondaryDatabase().getReference().child(finalMonthRef)
                            .setValue(url).addOnSuccessListener(new OnSuccessListener<Void>() {
                        @Override
                        public void onSuccess(Void aVoid) {
                            info.open();
                            info.deleteStoragePath(Long.parseLong(data.get(0).getRowId()));
                            info.deleteDatabasePath(Long.parseLong(data.get(0).getRowId()));
                            //added by Rajesh to delete images after uploading....
                            if (!info.checkPathExists(data.get(0).getLocalPath())) {
                                CommonMethods.deleteImages(data.get(0).getLocalPath());
                            }
                            info.close();
                            uploadImages(context);
                        }
                    }).addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                          /*  Crashlytics.log("UrlUpdateOnFailure\tBusiness" + loginSp.getString(Global.BIZ_ID, "") + "\t" +
                                    loginSp.getString(Global.GROUP_ID, ""));
                            Crashlytics.logException(e);*/
                            HashMap<String, Object> noUploadMap = new HashMap<>();
                            noUploadMap.put("error", "UrlUpdateonFailureListener" + e.toString());
                            noUploadMap.put("url", url);
                            //saveNoUploadImages(data.get(0), loginSp, noUploadMap);
                        }
                    });
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    private static void saveNoUploadImages(ImagePath imagePath, SharedPreferences loginSp, HashMap<String, Object> errorMap) {

//        getSecondaryDatabase()
        DatabaseReference noUploadRef = PersistentDatabase.getSecondaryDatabase().getReference()
                .child("temp").child("complaint").child(loginSp.getString(Global.BIZ_ID, ""))
                .child(loginSp.getString(Global.GROUP_ID, "")).child("noupload");

        HashMap<String, Object> noUploadMap = new HashMap<>();
        noUploadMap.put("dt", System.currentTimeMillis());
        noUploadMap.put("path", imagePath.getLocalPath());
        noUploadMap.put("imgRef", imagePath.getStoragePath());

        noUploadMap.putAll(errorMap);

        noUploadRef.push().updateChildren(noUploadMap);
    }


    public static Toast showToast(Context context, String msg, int time) {
        Toast toast = Toast.makeText(context, msg, time);
        toast.setGravity(Gravity.CENTER, 0, 0);
        return toast;
    }


    public static Bitmap decodeSampledBitmapFromFile(String path, int reqWidth, int reqHeight) { // BEST QUALITY MATCH

        //First decode with inJustDecodeBounds=true to check dimensions
        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inSampleSize = 1;
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(path, options);

        // Calculate inSampleSize, Raw height and width of image
        final int height = options.outHeight;
        final int width = options.outWidth;
        options.inPreferredConfig = Bitmap.Config.RGB_565;
        int inSampleSize = 1;

        if (height > reqHeight) {
            inSampleSize = Math.round((float) height / (float) reqHeight);
        }
        int expectedWidth = width / inSampleSize;

        if (expectedWidth > reqWidth) {
            //if(Math.round((float)width / (float)reqWidth) > inSampleSize) // If bigger SampSize..
            inSampleSize = Math.round((float) width / (float) reqWidth);
        }

        options.inSampleSize = inSampleSize;

        // Decode bitmap with inSampleSize set
        options.inJustDecodeBounds = false;

        return BitmapFactory.decodeFile(path, options);
    }

    public static Bitmap rotateBitmap(String sourcepath, Bitmap bitmap, String imageKey, Context context) {
        int rotate = 0;

        try {
            File imageFile = new File(sourcepath);
            ExifInterface exif = new ExifInterface(
                    imageFile.getAbsolutePath());
            int orientation = exif.getAttributeInt(
                    ExifInterface.TAG_ORIENTATION,
                    ExifInterface.ORIENTATION_NORMAL);

            switch (orientation) {
                case ExifInterface.ORIENTATION_ROTATE_270:
                    rotate = 270;
                    break;
                case ExifInterface.ORIENTATION_ROTATE_180:
                    rotate = 180;
                    break;
                case ExifInterface.ORIENTATION_ROTATE_90:
                    rotate = 90;
                    break;
            }
        } catch (Exception e) {
            //  e.printStackTrace();

           /* SharedPreferences imgPathSharedPref=
                    context.getSharedPreferences(Global.IMAGE_SP,Context.MODE_PRIVATE);
            if(imageKey!=null && imgPathSharedPref.contains(imageKey)) {
                try {
                    File WhoCameDirectory = new File(imgPathSharedPref.getString(imageKey, ""));

                    if (WhoCameDirectory.exists())
                        WhoCameDirectory.delete();
                }catch (Exception ex)
                {
                    CommonMethods.writeToLogFile(ex.toString());
                }
                finally {
                    imgPathSharedPref.edit().remove(imageKey).apply();
                }

            }*/
            CommonMethods.writeToLogFile(e.toString());

        }
        Matrix matrix = new Matrix();
        matrix.postRotate(rotate);
        bitmap = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, true);


        return bitmap;

    }


    public static void writeToLogFile(String log) {


        try {
            File root = new File(Environment.getExternalStorageDirectory(), "Complaint");
            if (!root.exists()) {
                root.mkdirs();
            }
            File gpxfile = new File(root, "log");
            FileWriter writer = new FileWriter(gpxfile, true);
            writer.append("dt: " + Calendar.getInstance().getTime() + "\n" + log + "\n");
            writer.flush();
            writer.close();

        } catch (IOException e) {
            e.printStackTrace();
        }

    }

 /*   public long getHourMinute(long time){
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(time);
        calendar.set(Calendar.SECOND,0);
        calendar.set(Calendar.MILLISECOND,0);
        return calendar.getTimeInMillis();
    }*/

    public long getMinDiff(long stTime, long endTime) {
        long diff = endTime - stTime;
        return TimeUnit.MILLISECONDS.toMinutes(diff);
    }

    public String getStatus(String statusCode) {
        switch (statusCode) {
            case "O":
                return "Open";
            case "W":
                return "Fixing";
            case "C":
                return "Close";
            default:
                return "Open";
        }
    }
}
