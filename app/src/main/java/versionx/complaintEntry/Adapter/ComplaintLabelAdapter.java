package versionx.complaintEntry.Adapter;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;

import java.util.List;

import versionx.complaintEntry.GetterSetter.ComplaintLabel;
import versionx.complaintEntry.R;

public class ComplaintLabelAdapter extends RecyclerView.Adapter<ComplaintLabelAdapter.ComplaintLabelViewHolder> {

    private List<ComplaintLabel> complaintLabelList;

    public ComplaintLabelAdapter(List<ComplaintLabel> complaintLabels){
        this.complaintLabelList = complaintLabels;
    }

    @NonNull
    @Override
    public ComplaintLabelViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_complaint_label_item,parent,false);
        return new ComplaintLabelViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull ComplaintLabelViewHolder holder, int position) {
         holder.cb_label.setText(complaintLabelList.get(position).getNm());
         holder.cb_label.setChecked(complaintLabelList.get(position).isSelected());
    }

    @Override
    public int getItemCount() {
        return complaintLabelList.size();
    }

    protected class ComplaintLabelViewHolder extends RecyclerView.ViewHolder{

        private CheckBox cb_label;

        public ComplaintLabelViewHolder(View itemView) {
            super(itemView);
            cb_label = itemView.findViewById(R.id.cb_complaint_label);

            cb_label.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    complaintLabelList.get(getAdapterPosition()).setSelected(isChecked);
                }
            });
        }
    }
}
