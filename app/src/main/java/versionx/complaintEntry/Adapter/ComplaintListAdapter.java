package versionx.complaintEntry.Adapter;


import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import androidx.core.content.ContextCompat;
import androidx.core.util.TimeUtils;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;

import versionx.complaintEntry.Activity.AddComplaintActivity;
import versionx.complaintEntry.GetterSetter.Complaint;
import versionx.complaintEntry.GetterSetter.Topic;
import versionx.complaintEntry.R;
import versionx.complaintEntry.Utils.CommonMethods;


/**
 * Created by developer on 30/3/17.
 */

public class ComplaintListAdapter extends RecyclerView.Adapter<ComplaintListAdapter.ComplaintsViewHolder> implements Filterable {


    private final String MY_DEBUG_TAG = "CustomerAdapter";
    Context context;

    private ArrayList<Complaint> complaintArrayList, tempComplaintList;
    private ArrayList<Complaint> filterData;

    private int resource;


    public ComplaintListAdapter(Context context, ArrayList<Complaint> complaints) {
        this.context = context;
        this.resource = resource;
        this.complaintArrayList = complaints;
        this.context = context;
        tempComplaintList = new ArrayList<>();
        filterData = new ArrayList<>();

    }

    @Override
    public ComplaintsViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.complaint_items, parent, false);
        return new ComplaintsViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ComplaintsViewHolder holder, final int position) {

        Complaint complaint = complaintArrayList.get(position);
        ArrayList<Topic> topicList = complaint.getTpc();

        long timeDifference=System.currentTimeMillis()-complaint.getDt();
        System.out.println(timeDifference);
        long diffSeconds = timeDifference / 1000 % 60;
        long diffMinutes = timeDifference / (60 * 1000) % 60;
        long diffHours = timeDifference / (60 * 60 * 1000) % 24;
        long diffDays = timeDifference / (24 * 60 * 60 * 1000);
        System.out.println(diffHours);

        if (topicList != null) {
            holder.tv_complaint.setText(topicList.get(0).getNm());
        } else if (complaint.getOthr() != null && !complaint.getOthr().equals("")) {
            holder.tv_complaint.setText(complaint.getOthr());
        }
        /*if (complaint.getSrc() != null) {
            holder.ll_Complaint_Source.setVisibility(View.VISIBLE);
            holder.tv_Complaint_Source.setText(complaint.getSrc().get(0).getNm());
        } else {
            holder.ll_Complaint_Source.setVisibility(View.GONE);
        }*/

        if (complaint.getSrc() != null) {
            holder.tv_Complaint_Source.setText(complaint.getSrc().get(0).getNm());
        }else {
            holder.tv_Complaint_Source.setText("");
        }


        if (complaint.getLbl() != null) {
            HashMap<String, Object> map = complaint.getLbl();
            for (Map.Entry<String, Object> entry : map.entrySet()) {
                String val = entry.getKey();
                if (val.equals("CRIT") || val.equals("ANG")) {
                    if (val.equals("CRIT")) {
                        holder.img_Angry.setVisibility(View.VISIBLE);
                        holder.img_Angry.setBackgroundResource(R.drawable.ic_critical);
                        break;
                    } else if (val.equals("ANG")) {
                        holder.img_Angry.setVisibility(View.VISIBLE);
                        holder.img_Angry.setBackgroundResource(R.drawable.ic_angry);
                    } else {
                        holder.img_Angry.setBackground(null);
                    }
                } else {
                    holder.img_Angry.setBackground(null);
                }
            }
        }else {
            holder.img_Angry.setBackground(null);
        }

        holder.tv_Date.setText(CommonMethods.getDate(complaintArrayList.get(position).getDt(), "d MMM @ h:mm a "));
        holder.tv_Date.setTypeface(null, Typeface.BOLD);

        if (complaint.getSt()!=null) {
//            holder.tv_comp_status.setText(CommonMethods.getStatus(complaintArrayList.get(position).getSt())+":"+diffHours+"hr");
//            holder.tv_comp_status.setText(context.getResources().getString(R.string.comp_status,
//                    new CommonMethods().getStatus(complaintArrayList.get(position).getSt())));
            String time;
            if (diffHours==0){
                time=diffMinutes+" Min";
            }else {
                time=diffHours+" Hr";
            }
            holder.tv_comp_status.setText(context.getResources().getString(R.string.comp_st,
                    new CommonMethods().getStatus(complaintArrayList.get(position).getSt()),time));

        }
        holder.tv_complaint.setTextColor(ContextCompat.getColor(context, complaintArrayList.get(position).isActionTimeExpired() ? R.color.light_red_color : R.color.black));
    }


    @Override
    public int getItemCount() {
        return complaintArrayList.size();
    }

    class ComplaintsViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView tv_complaint, tv_Date, tv_Complaint_Source,tv_comp_status;
        ImageView img_Angry;

        ComplaintsViewHolder(View itemView) {
            super(itemView);
            tv_complaint = (TextView) itemView.findViewById(R.id.tv_Complaint_History_Description);
            tv_Date = (TextView) itemView.findViewById(R.id.tv_Complaint_SinlgeRow_Date);
            tv_Complaint_Source = (TextView) itemView.findViewById(R.id.tv_Complaint_Source);
            tv_comp_status = itemView.findViewById(R.id.tv_Complaint_status);

            //ll_comp_item = itemView.findViewById(R.id.ll_comp_item);

            img_Angry = itemView.findViewById(R.id.img_Angry);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
     /*       switch (v.getId()) {
                case R.id.ll_Complaint_List:*/
            Intent intent = new Intent(context.getApplicationContext(), AddComplaintActivity.class);
            intent.putExtra("Complaint", complaintArrayList.get(getAdapterPosition()));
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            context.startActivity(intent);
        /*            break;


            }*/
        }
    }

    public void customdatasetChanged() {

        this.notifyDataSetChanged();
        this.tempComplaintList.clear();
        this.tempComplaintList.addAll(complaintArrayList);
        Collections.sort(complaintArrayList, new Comparator<Complaint>() {
            @Override
            public int compare(Complaint o1, Complaint o2) {
                return Long.compare(o2.getDt(), o1.getDt());
            }
        });
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                FilterResults filterResults = new FilterResults();
                filterData.clear();
                if (charSequence.length() == 0) {
                    filterData.addAll(tempComplaintList);
                } else {
                    for (Complaint complaint : tempComplaintList) {
                        if ((complaint.getOthr() != null && complaint.getOthr().toLowerCase().contains(charSequence.toString().toLowerCase()))
                                || (complaint.getTpc() != null && complaint.getTpc().get(0).getNm().toLowerCase().contains(charSequence.toString().toLowerCase()))) {
                            filterData.add(complaint);
                        }
                    }
                }
                filterResults.count = filterData.size();
                filterResults.values = filterData;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence constraint, FilterResults filterResults) {
                complaintArrayList.clear();
                if (filterResults.values != null) {
                    complaintArrayList.addAll((ArrayList<Complaint>) filterResults.values);
                    notifyDataSetChanged();
                }
            }
        };
    }
}
