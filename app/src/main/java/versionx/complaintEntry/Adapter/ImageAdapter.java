package versionx.complaintEntry.Adapter;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.transition.Transition;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.io.File;
import java.util.ArrayList;

import versionx.complaintEntry.Firebase.PersistentDatabase;
import versionx.complaintEntry.GetterSetter.ImagePath;
import versionx.complaintEntry.OfflineDataBase.FirebaseImagePath;
import versionx.complaintEntry.R;
import versionx.complaintEntry.Utils.CommonMethods;
import versionx.complaintEntry.Utils.Global;
import versionx.complaintEntry.Utils.TouchImageView;


/**
 * Created by developer on 2/12/16.
 */

public class ImageAdapter extends RecyclerView.Adapter<ImageAdapter.ImageViewHolder> {

    private Context context;
    private ArrayList<ImagePath> imgList;
    private ArrayList<Integer> pos;
    private SharedPreferences loginSp;
    private String compKey;

    public ImageAdapter(Context context, ArrayList<ImagePath> imgList, String compKey) {
        this.imgList = imgList;
        this.context = context;
        pos = new ArrayList<>();
        this.compKey = compKey;
        loginSp = this.context.getSharedPreferences(Global.LOGIN_SP, Context.MODE_PRIVATE);
    }

    @Override
    public ImageViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);

        // Inflate the custom layout
        View view = inflater.inflate(R.layout.image_row, parent, false);

        // Return a new holder instance

        return new ImageViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ImageViewHolder viewHolder, int position) {

        if (!imgList.get(position).getVal().contains("https:")) {
            try {
                viewHolder.in_image.setImageBitmap(CommonMethods.decodeSampledBitmapFromFile(imgList.get(position).getVal(), 90, 120));
            } catch (Exception e) {
                viewHolder.in_image.setImageResource(R.drawable.default_user_picture);
            }

        } else {
            Glide.with(context).load(imgList.get(position).getVal()).fitCenter().override(200, 200).diskCacheStrategy(DiskCacheStrategy.RESOURCE).into(viewHolder.in_image);

            // imageLoaderOriginal.DisplayImage(imgList.get(position).getVal(), viewHolder.in_image);
        }


     /*   viewHolder.in_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                zoomImg(imgList.get(position));

            }
        });*/


    }

    private void zoomImg(final String url, final String imgKey, final int position, Context context) {
        final Dialog dialog = new Dialog(context, android.R.style.Theme_Black_NoTitleBar_Fullscreen);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_img);
        dialog.setCancelable(true);

        final TouchImageView iv_idProof = (TouchImageView) dialog.findViewById(R.id.iv_dialog_idProof);
        final ImageView deleteImg = (ImageView) dialog.findViewById(R.id.deleteImg);

        if (!url.contains("https:")) {
            try {

                Glide.with(this.context).asBitmap().load(url).fitCenter().
                        override(500, 600).into(new SimpleTarget<Bitmap>() {
                            @Override
                            public void onResourceReady(@NonNull Bitmap resource, Transition<? super Bitmap> glideAnimation) {

                                iv_idProof.setImageBitmap(resource);
                                deleteImg.setVisibility(View.VISIBLE);

                            }
                        });


            } catch (Exception e) {
                deleteImg.setVisibility(View.GONE);
                iv_idProof.setImageResource(R.drawable.default_user_picture);
            }
        } else {


            Glide.with(this.context).asBitmap().load(url).fitCenter().
                    override(500, 600).into(new SimpleTarget<Bitmap>() {
                        @Override
                        public void onResourceReady(@NonNull Bitmap resource, Transition<? super Bitmap> glideAnimation) {
                            iv_idProof.setImageBitmap(resource);

                        }
                    });



          /*  deleteImg.setVisibility(View.GONE);
            imageLoaderOriginal.DisplayImage(url, iv_idProof);*/
        }

        dialog.show();

        deleteImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                showConfirmationDialog(ImageAdapter.this.context, url, imgKey, position, dialog);
            }
        });


    }


    private Context getContext() {
        return context;
    }

    @Override
    public int getItemCount() {
        return imgList.size();
    }

    private void showConfirmationDialog(Context context, final String imgUrl, final String imgKey, final int position, final Dialog dialogImg) {

        new AlertDialog.Builder(context)
                .setTitle("Delete Image")
                .setMessage("Are you sure?")
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int whichButton) {

                        imgList.remove(position);
                        notifyItemRemoved(position);

                        /*If it is a url image, then only deleting it....*/
                        if (compKey != null && imgUrl.startsWith("https")) {
//                            getSecondaryDatabase()
                            DatabaseReference imgRef = PersistentDatabase.getSecondaryDatabase().getReference("complaint/log/")
                                    .child(loginSp.getString(Global.BIZ_ID, ""))
                                    .child(loginSp.getString(Global.GROUP_ID, "")).child("open").child(compKey).child("img");

                            StorageReference imgStorageRef = FirebaseStorage.getInstance().getReferenceFromUrl(imgUrl);

                            /*Deleting from databaseRef*/
                            imgRef.child(imgKey).removeValue();

                            /*Deleting from storageRef*/
                            imgStorageRef.delete().addOnFailureListener(new OnFailureListener() {
                                @Override
                                public void onFailure(@NonNull Exception e) {
//                                    FirebaseCrash.report(e);
//                                    Crashlytics.log(e.getMessage());
                                }
                            });

                        } else {
                            /*If it is a local file, deleting it...*/
                            File file = new File(imgUrl);
                            if (file.exists()) {
                                file.delete();
                            }
                            FirebaseImagePath info = new FirebaseImagePath(getContext());
                            try {
                                info.open();
                                ArrayList<ImagePath> rowData = info.getRowDataByFilePath(imgUrl);

                                //Getting Row Id
                                if (rowData.size() != 0) {
                                    long rowId = Long.parseLong(rowData.get(0).getRowId());

                                    System.out.println("ImageAdapter" + rowData.get(0).getRowId());

                                    //Deleting Data By Row Id

                                    long storagePathTable = info.deleteStoragePath(rowId);
                                    long databasePathTable = info.deleteDatabasePath(rowId);
                                    System.out.println("DatabaseRowDeleted  " + String.valueOf(storagePathTable + "  =   " + databasePathTable));
                                    System.out.println();
                                    info.close();
                                }
                            } catch (Exception e) {
//                                FirebaseCrash.report(e);
//                                Crashlytics.log(e.getMessage());
//                                String error = e.toString();
//                                System.out.println("MaterialOutDetails Error  " + error);
                            }


                        }

                        dialog.dismiss();
                        dialogImg.dismiss();


                    }
                })
                .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        dialogImg.dismiss();

                    }
                }).show();
    }

    class ImageViewHolder extends RecyclerView.ViewHolder {

        ImageView in_image;

        ImageViewHolder(View itemView) {
            super(itemView);
            in_image = (ImageView) itemView.findViewById(R.id.in_image);

            in_image.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    zoomImg(imgList.get(getAdapterPosition()).getVal(), imgList.get(getAdapterPosition()).getKey(), getAdapterPosition(), context);

                }
            });
        }
    }

}
