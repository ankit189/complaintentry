package versionx.complaintEntry.Adapter;

import android.content.Context;
import androidx.annotation.NonNull;

import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import versionx.complaintEntry.GetterSetter.Topic;

public class CategoryAda extends ArrayAdapter<Topic> implements Filterable {
    private final String MY_DEBUG_TAG = "CustomerAdapter";
    Context context;
    private List<Topic> items;
    private List<Topic> tempItems;
    private List<Topic> suggestions;
    private int resource;

    public CategoryAda(Context context, int resource, List<Topic> items) {
        super(context, resource,0,items);

        this.context=context;
        this.items=items;
        this.resource = resource;
        this.tempItems =new ArrayList<>(this.items);
        this.suggestions = new ArrayList<>();

    }

    @NonNull
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view=convertView;
        if (convertView==null){
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(resource, parent, false);
        }

        Topic complaint=items.get(position);
        HashMap<String,Object> mainCategory= (HashMap<String, Object>) complaint.getCat();

        if (complaint!=null&& view instanceof TextView){
            ((TextView) view).setText(complaint.getNm()+" ("+mainCategory.get("nm")+") ");
            ((TextView) view).setTextColor(Color.BLACK);
        }

        return view;
    }
    public void customdatasetChanged(){
        this.notifyDataSetChanged();
        this.tempItems.clear();
        this.tempItems.addAll(items);
    }

    @Override
    public Filter getFilter()
    {
        return nameFilter;
    }

    Filter nameFilter = new Filter()
    {

        @Override
        protected FilterResults performFiltering(CharSequence constraint)
        {

            FilterResults filterResults = new FilterResults();
            suggestions.clear();
            if (constraint.length() == 0) {
                suggestions.addAll(tempItems);
            } else {
                for (Topic stOptions : tempItems) {
                    if (stOptions.getNm().toLowerCase().contains(constraint.toString().toLowerCase()) ) {
                        suggestions.add(stOptions);
                    }
                }
            }
            filterResults.count = suggestions.size();
            filterResults.values = suggestions;

            return filterResults;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results)
        {
            List<Topic> filterList = (ArrayList<Topic>) results.values;
            if (results != null && results.count > 0)
            {
                clear();
                items.addAll(filterList);
                notifyDataSetChanged();
            }
        }
    };
}

