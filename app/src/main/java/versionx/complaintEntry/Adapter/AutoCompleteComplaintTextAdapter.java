package versionx.complaintEntry.Adapter;

import android.content.Context;
import androidx.annotation.NonNull;

import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import versionx.complaintEntry.GetterSetter.Base;
import versionx.complaintEntry.R;

/**
 * Created by developer on 31/3/17.
 */

public class AutoCompleteComplaintTextAdapter extends ArrayAdapter<Base> implements Filterable {
    private final String MY_DEBUG_TAG = "CustomerAdapter";
    Context context;
    private List<Base> items;
    private List<Base> tempItems;
    private List<Base> suggestions;
    private int resource;

    public AutoCompleteComplaintTextAdapter(Context context, int resource, List<Base> items) {
        super(context, resource, 0, items);

        this.context = context;
        this.items = items;
        this.resource = resource;
        this.tempItems = new ArrayList<>(this.items);
        this.suggestions = new ArrayList<>();

    }

    @NonNull
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(resource, parent, false);
        }

        Base complaint = items.get(position);

        if (complaint != null && view instanceof TextView) {
            if (complaint.getRmNo() == null && complaint.getAptNo()==null) {
                ((TextView) view).setText(complaint.getNm());
                ((TextView) view).setTextColor(Color.BLACK);
            } else if (complaint.getAptNo() != null) {
                ((TextView) view).setText(context.getResources().getString(R.string.comp_source_apt_name, complaint.getNm(), complaint.getAptNo()));
                ((TextView) view).setTextColor(Color.BLACK);

            } else if (complaint.getRmNo() != null) {
                ((TextView) view).setText(context.getResources().getString(R.string.comp_source_room_name, complaint.getRmNo(), complaint.getNm()));
                ((TextView) view).setTextColor(Color.BLACK);
            } else {
                ((TextView) view).setText(complaint.getNm());
                ((TextView) view).setTextColor(Color.BLACK);
            }

        }

        return view;
    }

    public void customdatasetChanged() {
        this.notifyDataSetChanged();
        this.tempItems.clear();
        this.tempItems.addAll(items);
    }

    @Override
    public Filter getFilter() {
        return nameFilter;
    }

    Filter nameFilter = new Filter() {

        @Override
        protected FilterResults performFiltering(CharSequence constraint) {

            FilterResults filterResults = new FilterResults();
            suggestions.clear();
            if (constraint.length() == 0) {
                suggestions.addAll(tempItems);
            } else {
                for (Base stOptions : tempItems) {
                    /*if (stOptions.getNm().toLowerCase().contains(constraint.toString().toLowerCase()) ||
                            (stOptions.getRmNo() != null && stOptions.getRmNo().contains(constraint.toString().toLowerCase()))
                    ) {
                        suggestions.add(stOptions);
                    }*/

                    if (stOptions.getNm().toLowerCase().contains(constraint.toString().toLowerCase()) ||
                            (stOptions.getRmNo() != null && stOptions.getRmNo().contains(constraint.toString().toLowerCase()))
                            || (stOptions.getAptNo() != null && stOptions.getAptNo().contains(constraint.toString().toLowerCase()))
                    ) {
                        suggestions.add(stOptions);
                    }
                }
            }
            filterResults.count = suggestions.size();
            filterResults.values = suggestions;

            return filterResults;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            List<Base> filterList = (ArrayList<Base>) results.values;
            if (results != null && results.count > 0) {
                clear();
                items.addAll(filterList);
                notifyDataSetChanged();
            }
        }
    };
}

